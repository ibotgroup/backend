php artisan horizon:terminate #check that queue is empty


--deploy
cd /var/www/tradebot/bot-backend && su www-data -s /bin/sh
[go to supervisor and stop all.]
php artisan down
git fetch
git checkout {$number}
composer update --prefer-dist --no-dev --optimize-autoloader --classmap-authoritative
php artisan clear-compiled && php artisan config:clear && php artisan cache:clear && php artisan route:clear && php artisan view:clear && php artisan event:clear && php artisan config:cache && php artisan route:cache && php artisan event:cache && php artisan optimize
php artisan migrate
exit
systemctl restart php8.0-fpm.service
su www-data -s /bin/sh
php artisan up
exit
[go to supervisor and restart all.]
