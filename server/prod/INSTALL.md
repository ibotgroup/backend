apt-get nginx php8.0 php8.0-fpm mysql-server-8.0
mysql:
---
ALTER USER 'root'@'localhost' IDENTIFIED WITH caching_sha2_password BY 'rooT8pwd8*';
GRANT ALL ON *.* TO 'root'@'localhost';

CREATE USER 'tradebot'@'localhost' IDENTIFIED WITH caching_sha2_password BY 'rooT8pwd8*';
GRANT ALL ON *.* TO 'tradebot'@'localhost';
FLUSH PRIVILEGES;

-SELECT user FROM mysql.user;

CREATE DATABASE tradebot CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

---
composer install
php artisan horizon:install
sudo apt-get install supervisor

--test only, change port on prod.
sudo firewall-cmd --zone=public --add-port=9001/tcp
sudo firewall-cmd --zone=public --permanent --add-port=9001/tcp
--

sudo cp server/prod/etc/supervisor/conf.d/horizon.conf /etc/supervisor/conf.d/
sudo cp server/prod/etc/supervisor/conf.d/master.conf /etc/supervisor/conf.d/
sudo cp server/prod/etc/supervisor/conf.d/scheduler.conf /etc/supervisor/conf.d/

systemctl start supervisor
systemctl status supervisor

supervisor:
---
php artisan core:scheduler
php artisan master-process:tasks
---

