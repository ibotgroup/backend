<?php
declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Add field amount per order to algorithms grid.
 */
final class AddFieldAmountPerOrderToAlgorithmsGrid extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('algorithms_grid', function (Blueprint $table) {
            $table->float('initial_margin', 45, 8)
                ->nullable(true)
                ->change();

            $table->float('amount_per_order', 45, 8)
                ->nullable(true)
                ->after('initial_margin');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('algorithms_grid', function (Blueprint $table) {
            $table->float('initial_margin', 45, 8)
                ->nullable(false)
                ->change();

            $table->dropColumn('amount_per_order');
        });
    }
}
