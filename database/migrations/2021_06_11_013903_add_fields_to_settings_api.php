<?php
declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * This migration adds some fields to settings_api.
 */
final class AddFieldsToSettingsApi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('accesses', function (Blueprint $table) {
            $table->string('title')
                ->after('id');
            $table->unsignedBigInteger('users_id')
                ->after('id');
            $table->foreign('users_id', 'accesses_users_id_foreign')
                ->references('id')
                ->on('users')
                ->cascadeOnDelete();
            $table->unsignedInteger('max_active_bots')
                ->after('secret');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('accesses', function (Blueprint $table) {
            $table->dropColumn('title');
            $table->dropColumn('max_active_bots');
            $table->dropForeign('accesses_users_id_foreign');
            $table->dropColumn('users_id');
        });
    }
}
