<?php
declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Add field deals errors id to bot.
 */
final class AddFieldDealsErrorsIdToBot extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('bots', function (Blueprint $table) {
            $table->unsignedBigInteger('deals_errors_id')
                ->after('status')
                ->nullable(true);
            $table->foreign('deals_errors_id', 'bots_deals_errors_id_foreign')
                ->references('id')
                ->on('deals_errors')
                ->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('bots', function (Blueprint $table) {
            $table->dropForeign('bots_deals_errors_id_foreign');
            $table->dropColumn('deals_errors_id');
        });
    }
}
