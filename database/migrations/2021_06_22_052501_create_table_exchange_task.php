<?php
declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Create table "tasks".
 */
final class CreateTableExchangeTask extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->id();
            $table->integer('provider');
            $table->integer('weight');
            $table->text('payload');
            $table->dateTime('after_at')
                ->index('tasks_after_at_index')
                ->nullable(true);
            $table->integer('attempts')
                ->default(0)
                ->index('tasks_attempts_index');
            $table->boolean('is_processed')
                ->default(false)
                ->index('tasks_is_processed_index');
            $table->boolean('is_locked')
                ->default(false)
                ->index('tasks_is_locked_index');
            $table->timestamps();
            $table->index('created_at', 'tasks_created_at_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('tasks');
    }
}
