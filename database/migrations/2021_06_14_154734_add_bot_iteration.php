<?php
declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Add bots_iteration table for store bot updates data.
 */
final class AddBotIteration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('bots_iteration', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('bots_id');
            $table->foreign('bots_id', 'bots_iteration_bots_id_foreign')
                ->references('id')
                ->on('bots')
                ->cascadeOnDelete();
            $table->tinyInteger('status')
                ->index('bots_iteration_bots_id_index');
            $table->timestamps();
        });

        Schema::table('bots', function (Blueprint $table) {
            $table->unsignedBigInteger('bots_iteration_id')
                ->after('sid')
                ->nullable(true);
            $table->foreign('bots_iteration_id', 'bots_bots_iteration_id_foreign')
                ->references('id')
                ->on('bots_iteration')
                ->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('bots', function (Blueprint $table) {
            $table->dropForeign('bots_bots_iteration_id_foreign');
            $table->dropColumn('bots_iteration_id');
        });

        Schema::drop('bots_iteration');
    }
}
