<?php
declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Create symbols table.
 */
final class CreateSymbols extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('pairs', function (Blueprint $table) {
            $table->id();
            $table->char('pair', 30);
            $table->tinyInteger('provider');
            $table->unsignedSmallInteger('base_asset_precision');
            $table->unsignedSmallInteger('quote_asset_precision');
            $table->string('tick_size');
            $table->timestamps();
            $table->unique(['pair', 'provider'], 'pairs_pair_provider_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::drop('pairs');
    }
}
