<?php
declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Add field closing deals to bot_iteration.
 */
final class AddFieldClosingDealsToBotIteration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('bots_iteration', function (Blueprint $table) {
            $table->boolean('closing_deals')
                ->default(false)
                ->after('processed_after_monitoring');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('bots_iteration', function (Blueprint $table) {
            $table->dropColumn('closing_deals');
        });
    }
}
