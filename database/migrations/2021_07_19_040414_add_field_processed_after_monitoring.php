<?php
declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * This migration add field "processed_after_monitoring"
 * to "bots_iteration".
 */
final class AddFieldProcessedAfterMonitoring extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('bots_iteration', function(Blueprint $table) {
            $table->boolean('processed_after_monitoring')
                ->default(false)
                ->after('price_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('bots_iteration', function(Blueprint $table) {
            $table->dropColumn('processed_after_monitoring');
        });
    }
}
