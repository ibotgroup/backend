<?php
declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * This migration creates bots table.
 */
final class CreateBots extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('bots', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('sid')
                ->unique('bots_sid_unique');
            $table->unsignedBigInteger('users_id');
            $table->foreign('users_id', 'bot_users_id_foreign')
                ->references('id')
                ->on('users')
                ->cascadeOnDelete();
            $table->unsignedBigInteger('accesses_id');
            $table->foreign('accesses_id', 'bot_access_id_id_foreign')
                ->references('id')
                ->on('accesses')
                ->cascadeOnDelete();
            $table->unsignedBigInteger('algorithms_grid_id')
                ->nullable(true);
            $table->foreign('algorithms_grid_id', 'bot_algorithms_grid_id_foreign')
                ->references('id')
                ->on('algorithms_grid')
                ->cascadeOnDelete();
            $table->tinyInteger('status')
                ->index('bots_status_index');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('bots');
    }
}
