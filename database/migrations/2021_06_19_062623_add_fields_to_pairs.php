<?php
declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Add fields to pairs.
 */
final class AddFieldsToPairs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('pairs', function (Blueprint $table) {
            $table->boolean('is_spot_trading_allowed')
                ->after('last_price_at');
        });

        Schema::table('pairs', function (Blueprint $table) {
            $table->boolean('is_margin_trading_allowed')
                ->after('is_spot_trading_allowed');
        });

        Schema::table('pairs', function (Blueprint $table) {
            $table->float('filter_price_max', 45, 8)
                ->nullable(true)
                ->after('is_margin_trading_allowed');
            $table->float('filter_price_min', 45, 8)
                ->after('is_margin_trading_allowed');
        });

        Schema::table('pairs', function (Blueprint $table) {
            $table->float('filter_lot_max_qty', 45, 8)
                ->nullable(true)
                ->after('filter_price_max');

            $table->float('filter_lot_min_qty', 45, 8)
                ->nullable(true)
                ->after('filter_price_max');
        });

        Schema::table('pairs', function (Blueprint $table) {
            $table->float('filter_min_notional', 45, 8)
                ->nullable(true)
                ->after('filter_lot_max_qty');
        });

        Schema::table('pairs', function (Blueprint $table) {
            $table->float('filter_max_num_orders', 45, 8)
                ->nullable(true)
                ->after('filter_min_notional');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('pairs', function (Blueprint $table) {
            $table->dropColumn([
                'is_spot_trading_allowed',
                'is_margin_trading_allowed',
                'filter_price_max',
                'filter_price_min',
                'filter_lot_max_qty',
                'filter_lot_min_qty',
                'filter_min_notional',
                'filter_max_num_orders',
            ]);
        });
    }
}
