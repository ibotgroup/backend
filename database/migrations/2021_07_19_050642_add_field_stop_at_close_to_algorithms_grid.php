<?php
declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Add field stop at close to "algorithms_grid" table.
 */
final class AddFieldStopAtCloseToAlgorithmsGrid extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('algorithms_grid', function (Blueprint $table) {
            $table->boolean('stop_at_close')
                ->default(false)
                ->after('amount_per_order');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('algorithms_grid', function (Blueprint $table) {
            $table->dropColumn('stop_at_close');
        });
    }
}
