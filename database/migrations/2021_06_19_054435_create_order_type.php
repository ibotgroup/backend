<?php
declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Create order type table.
 */
final class CreateOrderType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('orders_types', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('pairs_id');
            $table->foreign('pairs_id', 'orders_types_pairs_id_foreign')
                ->references('id')
                ->on('pairs')
                ->cascadeOnDelete();
            $table->string('type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders_types');
    }
}
