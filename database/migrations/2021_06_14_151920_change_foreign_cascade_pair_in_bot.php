<?php
declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Change foreign cascade pair in bot.
 */
final class ChangeForeignCascadePairInBot extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('bots', function (Blueprint $table) {
            $table->dropForeign('bots_pairs_id_foreign');
            $table->foreign('pairs_id', 'bots_pairs_id_foreign')
                ->references('id')
                ->on('pairs')
                ->restrictOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('bots', function (Blueprint $table) {
            $table->dropForeign('bots_pairs_id_foreign');
            $table->foreign('pairs_id', 'bots_pairs_id_foreign')
                ->references('id')
                ->on('pairs')
                ->cascadeOnDelete();
        });
    }
}
