<?php
declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Create algorithm grid.
 */
final class CreateAlgorithmsGrid extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('algorithms_grid', function (Blueprint $table) {
            $table->id();
            $table->tinyInteger('type');
            $table->float('lower_limit', 45, 8);
            $table->float('upper_limit', 45, 8);
            $table->unsignedInteger('grids');
            $table->unsignedFloat('initial_margin', 45, 8);
            $table->unsignedFloat('activation_trigger', 45, 8)
                ->nullable(true);
            $table->unsignedFloat('low_stop_trigger', 45, 8)
                ->nullable(true);
            $table->unsignedFloat('high_stop_trigger', 45, 8)
                ->nullable(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('algorithms_grid');
    }
}
