<?php
declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Drop table tasks.
 */
final class DropTableTasks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('tasks');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('tasks', function(Blueprint $table) {
            $table->id();
            $table->string('task_id')
                ->nullable(true)
                ->index('tasks_task_id_index');
            $table->boolean('without_overlapping')
                ->default(false);
            $table->integer('provider')
                ->index('tasks_provider_index');
            $table->integer('weight');
            $table->boolean('cancel_overdue')
                ->default(false);
            $table->boolean('force')
                ->default(false);
            $table->text('payload');
            $table->dateTime('after_at')
                ->nullable(true)
                ->index('tasks_after_at_index');
            $table->integer('attempts')
                ->index('tasks_attempts_index');
            $table->boolean('is_cancelled')
                ->default(false);
            $table->integer('is_processed')
                ->index('tasks_is_processed_index')
                ->default(false);
            $table->integer('is_locked')
                ->index('tasks_is_locked_index')
                ->default(false)
                ->index('tasks_is_locked_index');
            $table->integer('left_weight')
                ->nullable(true);
            $table->dateTime('left_weight_at')
                ->nullable(true);
            $table->timestamps();
            $table->index('created_at', 'tasks_created_at_index');
        });
    }
}
