<?php
declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Add fields to deals.
 */
final class AddFieldsToDeals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('deals', function (Blueprint $table) {
            $table->unsignedBigInteger('binance_order_id')
                ->nullable(true)
                ->after('sid');
        });

        Schema::table('deals', function (Blueprint $table) {
            $table->string('binance_client_order_id')
                ->nullable(true)
                ->after('binance_order_id');
        });

        Schema::table('deals', function (Blueprint $table) {
            $table->dateTime('failed_at')
                ->nullable(true)
                ->after('cancelled_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('deals', function (Blueprint $table) {
            $table->dropColumn('binance_order_id');
            $table->dropColumn('binance_client_order_id');
            $table->dropColumn('failed_at');
        });
    }
}
