<?php
declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Create deals errors.
 */
final class CreateDealsErrors extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('deals_errors', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('deals_id');
            $table->foreign('deals_id', 'deals_errors_deals_id_foreign')
                ->references('id')
                ->on('deals')
                ->cascadeOnDelete();
            $table->string('code');
            $table->text('message')
                ->nullable(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::drop('deals_errors');
    }
}
