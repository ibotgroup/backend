<?php
declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Create tasks_used_weights.
 */
final class AddUsedWeightsToTasks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tasks', function (Blueprint $table) {
            $table->string('task_id')
                ->nullable(true)
                ->after('id')
                ->index('tasks_task_id_index');
        });

        Schema::table('tasks', function (Blueprint $table) {
            $table->integer('left_weight')
                ->nullable(true)
                ->after('is_locked');
        });

        Schema::table('tasks', function (Blueprint $table) {
            $table->dateTime('left_weight_at')
                ->nullable(true)
                ->after('left_weight');
        });

        Schema::table('tasks', function (Blueprint $table) {
            $table->boolean('is_cancelled')
                ->default(false)
                ->after('attempts');
        });

        Schema::table('tasks', function (Blueprint $table) {
            $table->boolean('cancel_overdue')
                ->default(false)
                ->after('weight');
        });

        Schema::table('tasks', function (Blueprint $table) {
            $table->index('provider', 'tasks_provider_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tasks', function (Blueprint $table) {
            $table->dropColumn(['task_id', 'left_weight', 'left_weight_at', 'is_cancelled', 'cancel_overdue']);
            $table->dropIndex('tasks_provider_index');
        });
    }
}
