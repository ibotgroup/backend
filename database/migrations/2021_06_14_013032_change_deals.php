<?php
declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * This migration change/add fields in deals.
 */
final class ChangeDeals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('deals', function (Blueprint $table) {
            $table->dropColumn(['activated_at', 'closed_at']);

            $table->dateTime('cancelled_at')
                ->nullable(true)
                ->after('status');
            $table->dateTime('triggered_at')
                ->nullable(true)
                ->after('status');
            $table->dateTime('set_at')
                ->nullable(true)
                ->after('status');
            $table->tinyInteger('side')
                ->after('status');
            $table->tinyInteger('type')
                ->after('status');
            $table->float('price', 45, 8)
                ->after('status');

            $table->index('status', 'deals_status_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('deals', function (Blueprint $table) {
            $table->dropColumn(['cancelled_at', 'triggered_at', 'set_at', 'side', 'type', 'price']);
            $table->dropIndex('deals_status_index');

            $table->dateTime('closed_at')
                ->nullable(true)
                ->after('status');
            $table->dateTime('activated_at')
                ->nullable(true)
                ->after('status');
        });
    }
}
