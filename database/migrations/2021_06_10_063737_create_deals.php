<?php
declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Create deals migration.
 */
final class CreateDeals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('deals', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('sid')
                ->unique('deals_sid_unique');
            $table->unsignedBigInteger('bots_id');
            $table->foreign('bots_id', 'deals_bots_id_foreign')
                ->references('id')
                ->on('bots')
                ->cascadeOnDelete();
            $table->float('volume', 45, 8);
            $table->tinyInteger('status');
            $table->dateTime('activated_at')
                ->nullable(true);
            $table->dateTime('closed_at')
                ->nullable(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('deals');
    }
}
