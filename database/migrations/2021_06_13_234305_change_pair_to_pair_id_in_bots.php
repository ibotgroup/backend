<?php
declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * This migration changes pair to pair id in bots table.
 */
final class ChangePairToPairIdInBots extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('bots', function (Blueprint $table) {
            $table->dropColumn('pair');

            $table->unsignedBigInteger('pairs_id')
                ->after('algorithms_grid_id');
            $table->foreign('pairs_id', 'bots_pairs_id_foreign')
                ->references('id')
                ->on('pairs')
                ->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('bots', function (Blueprint $table) {
            $table->dropForeign('bots_pairs_id_foreign');
            $table->dropColumn('pairs_id');

            $table->char('pair', 30)
                ->after('algorithms_grid_id');
        });
    }
}
