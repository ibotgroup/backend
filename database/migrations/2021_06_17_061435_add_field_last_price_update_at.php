<?php
declare(strict_types=1);

use App\Modules\Core\Facades\DB;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Add field last price update at.
 */
final class AddFieldLastPriceUpdateAt extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('pairs', function (Blueprint $table) {
            $table->dateTime('last_price_at')
                ->after('last_price')
                ->nullable(true);
        });

        DB::transaction(function () {
            foreach (DB::table('pairs')
                         ->get() as $pair) {
                DB::table('pairs')
                    ->where('id', $pair->id)
                    ->update([
                        'last_price_at' => new DateTime(),
                    ]);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('pairs', function (Blueprint $table) {
            $table->dropColumn('last_price_at');
        });
    }
}
