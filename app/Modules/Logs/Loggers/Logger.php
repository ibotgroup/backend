<?php
declare(strict_types=1);

namespace App\Modules\Logs\Loggers;

use App\Modules\Logs\Contracts\LogsModuleContract;
use JetBrains\PhpStorm\ArrayShape;
use JetBrains\PhpStorm\Deprecated;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\StreamHandler;
use Monolog\Logger as BaseLogger;
use Nette\PhpGenerator\Dumper;
use Symfony\Component\HttpFoundation\HeaderBag;
use Throwable;
use function sprintf;

/**
 * Logger.
 */
final class Logger extends BaseLogger implements LogsModuleContract
{
    /** @var Dumper Dumper php code. */
    private Dumper $dumper;

    /**
     * Constructor.
     *
     * @param Dumper $dumper
     */
    public function __construct(Dumper $dumper)
    {
        $name = 'logger';
        parent::__construct($name);

        $this->init('logs/app.log');

        $this->dumper = $dumper;
    }

    /**
     * Logging error with data.
     *
     * @param mixed $data
     */
    public function jsError(string $data): void
    {
        $this->error(
            "JS error log | " . $data
        );
    }

    /**
     * Инициализация обработчиков логов.
     *
     * @param string $path
     */
    public function init(string $path): void
    {
        $stream = new StreamHandler(storage_path($path), Logger::ERROR);
        $stream->setFormatter(
            new LineFormatter(null, 'Y-m-d H:i:s')
        );

        $this->pushHandler($stream);
        $this->pushHandler(new StreamHandler('php://stdout', Logger::INFO));
    }

    /**
     * {@inheritDoc}
     */
    public function debug($message, array $context = array()): void
    {
        parent::debug($message, $context);
    }

    /**
     * Rollback transaction logging.
     *
     * @param Throwable $e
     * @param mixed $data
     */
    public function rollbackTransaction(Throwable $e, mixed $data = null): void
    {
        $this->registerException($e, $data, 'The transaction was rollback');
    }

    /**
     * Зарегистрировать исключение.
     *
     * @param Throwable $e
     * @param mixed $data
     * @param string|null $message
     */
    public function registerException(Throwable $e, mixed $data = null, string $message = null): void
    {
        $this->error(
            sprintf(
                "[%s: %s] [exception: %s; %s] [previous: %s; %s]  [request: %s] [data: %s]",
                $e::class,
                $message ?? 'Registered exception.',
                $e->getMessage(),
                $e->getTraceAsString(),
                $e->getPrevious()
                    ?->getMessage(),
                $e->getPrevious()
                    ?->getTraceAsString(),
                $this->dumper->dump($this->getRequestData()),
                $this->dumper->dump($data),
            )
        );
    }

    /**
     * Данные запроса для логгирования.
     *
     * @return array
     */
    #[ArrayShape([
        'url' => "string",
        'request' => "array",
        'headers' => HeaderBag::class
    ])]
    private function getRequestData(): array
    {
        $request = request();

        return [
            'url' => $request->fullUrl(),
            'request' => $request->all(),
            'headers' => $request->headers,
        ];
    }
}
