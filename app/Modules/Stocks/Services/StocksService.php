<?php
declare(strict_types=1);

namespace App\Modules\Stocks\Services;

use App\Modules\Bots\Enum\Trades\TypeIntervalEnum;
use App\Modules\Core\Contracts\Services\ServiceContract;
use App\Modules\Core\Exceptions\Services\Network\RequestException;
use App\Modules\Exchanges\Services\BinanceService;
use App\Modules\Market\Models\Pair;
use App\Modules\Stocks\Contracts\StocksModuleContract;
use Illuminate\Support\Collection;

/**
 * Stock service.
 */
final class StocksService implements ServiceContract, StocksModuleContract
{
    /**
     * Get last thousand.
     *
     * @param Pair $pair
     * @param string $interval
     * @return Collection
     * @throws RequestException
     */
    public function getLastThousand(Pair $pair, string $interval): Collection
    {
        return collect(
            $this->getSystemBinance()
                ->getSystemBinance()
                ->getKLines(
                    $pair->pair,
                    $interval,
                    1000
                )
        );
    }

    /**
     * Get {@see BinanceService::class}.
     *
     * @return BinanceService
     */
    private function getSystemBinance(): BinanceService
    {
        return app(BinanceService::class);
    }
}
