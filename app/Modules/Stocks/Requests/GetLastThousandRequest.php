<?php
declare(strict_types=1);

namespace App\Modules\Stocks\Requests;

use App\Modules\Bots\Enum\Trades\TypeIntervalEnum;
use App\Modules\Market\Models\Pair;
use App\Modules\Stocks\Contracts\StocksModuleContract;
use Illuminate\Foundation\Http\FormRequest;
use JetBrains\PhpStorm\ArrayShape;

/**
 * Get last thousand minutes request.
 *
 * @property-read string|mixed $interval
 * @property-read Pair|mixed $pair
 */
final class GetLastThousandRequest extends FormRequest implements StocksModuleContract
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return in_array($this->interval, TypeIntervalEnum::INTERVALS) &&
            $this->pair instanceof Pair;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [];
    }

    /**
     * {@inheritDoc}
     */
    #[ArrayShape([
        'pair' => Pair::class,
        'interval' => "string"
    ])]
    public function validated(): array
    {
        return [
            'pair' => $this->pair,
            'interval' => $this->interval,
        ];
    }
}
