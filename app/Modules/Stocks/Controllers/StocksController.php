<?php
declare(strict_types=1);

namespace App\Modules\Stocks\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Core\Attributes\Controllers\CatchResponse;
use App\Modules\Core\Contracts\CoreModuleContract;
use App\Modules\Core\Dto\ResponseDto;
use App\Modules\Core\Exceptions\Services\Network\RequestException;
use App\Modules\Market\Models\Pair;
use App\Modules\Stocks\Requests\GetLastThousandRequest;
use App\Modules\Stocks\Services\StocksService;

/**
 * Stock controller.
 */
final class StocksController extends Controller implements CoreModuleContract
{
    /**
     * Get last thousand minutes.
     * @throws RequestException
     */
    #[CatchResponse]
    public function getLastThousand(GetLastThousandRequest $request): ResponseDto
    {
        return new ResponseDto(
            $this->getStockService()
                ->getLastThousand(
                    ...$request->validated()
                )
        );
    }

    /**
     * Get {@see StocksService::class}.
     *
     * @return StocksService
     */
    private function getStockService(): StocksService
    {
        return app(StocksService::class);
    }
}
