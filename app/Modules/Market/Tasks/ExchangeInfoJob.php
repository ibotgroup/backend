<?php
declare(strict_types=1);

namespace App\Modules\Market\Tasks;

use App\Modules\Core\Contracts\Jobs\WeightControlled;
use App\Modules\Core\Contracts\Services\DataUpdaterContract;
use App\Modules\Core\Exceptions\Services\Network\RequestException;
use App\Modules\Core\Jobs\AbstractJob;
use App\Modules\Core\Jobs\RateLimited;
use App\Modules\Core\Traits\Tasks\WeightControl;
use App\Modules\Exchanges\Jobs\BinanceWeightLimited;
use App\Modules\Market\Contracts\MarketModuleContract;
use App\Modules\Market\Services\Updaters\ExchangeInformationUpdaterService;
use DateTime;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Cache\LockTimeoutException;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\Middleware\WithoutOverlapping;
use Illuminate\Queue\SerializesModels;
use function app;

/**
 * ExchangeInfoJob.
 */
final class ExchangeInfoJob extends AbstractJob implements WeightControlled, MarketModuleContract, DataUpdaterContract, ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, WeightControl;

    /**
     * Update prices in database from system binance api.
     *
     * @throws RequestException
     * @throws LockTimeoutException
     */
    public function handle(): void
    {
        $this->info();

        $service = $this->getExchangeInformationUpdaterService();

        try {
            $service->update();
        } finally {
            $this->setLeftWeight(
                $service->leftWeight(),
                $service->leftWeightAt(),
            );
        }
    }

    /**
     * {@inheritDoc}
     */
    public function getId(): string
    {
        return 'market:exchange-info';
    }

    /**
     * {@inheritDoc}
     */
    public function getWeight(): int
    {
        return 10;
    }

    /**
     * Determine the time at which the job should timeout.
     *
     * @return DateTime
     */
    public function retryUntil(): DateTime
    {
        return now()->addMinutes(5);
    }

    /**
     * Get the middleware the job should pass through.
     *
     * @return array
     */
    public function middleware(): array
    {
        return [
            (new WithoutOverlapping($this->getId()))
                ->expireAfter(300)
                ->dontRelease(),
            new RateLimited($this->getId(), 3600, true),
            new BinanceWeightLimited($this->getWeight()),
        ];
    }

    /**
     * Get {@see ExchangeInformationUpdaterService::class}.
     *
     * @return ExchangeInformationUpdaterService
     */
    private function getExchangeInformationUpdaterService(): ExchangeInformationUpdaterService
    {
        return app(ExchangeInformationUpdaterService::class);
    }
}
