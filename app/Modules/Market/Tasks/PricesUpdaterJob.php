<?php
declare(strict_types=1);

namespace App\Modules\Market\Tasks;

use App\Modules\Core\Contracts\Jobs\WeightControlled;
use App\Modules\Core\Exceptions\Services\Network\RequestException;
use App\Modules\Core\Jobs\AbstractJob;
use App\Modules\Core\Jobs\RateLimited;
use App\Modules\Core\Locks\AtomicLocks;
use App\Modules\Core\Traits\Tasks\WeightControl;
use App\Modules\Exchanges\Exceptions\Providers\BinanceProvider\ErrorMsgReceivedRequestException;
use App\Modules\Exchanges\Exceptions\Providers\BinanceProvider\GeneralRequestException;
use App\Modules\Exchanges\Exceptions\Providers\BinanceProvider\RequestIssueRequestException;
use App\Modules\Exchanges\Exceptions\Providers\BinanceProvider\UnknownRequestException;
use App\Modules\Exchanges\Jobs\BinanceWeightLimited;
use App\Modules\Market\Contracts\MarketModuleContract;
use App\Modules\Market\Services\Updaters\PricesUpdaterService;
use DateTime;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Cache\LockTimeoutException;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\Middleware\WithoutOverlapping;
use Illuminate\Queue\SerializesModels;
use function app;

/**
 * PricesUpdaterJob.
 */
final class PricesUpdaterJob extends AbstractJob implements WeightControlled, MarketModuleContract, ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, WeightControl;

    /**
     * Update prices in database from system binance api.
     *
     * @throws RequestException
     * @throws ErrorMsgReceivedRequestException
     * @throws GeneralRequestException
     * @throws RequestIssueRequestException
     * @throws UnknownRequestException
     * @throws LockTimeoutException
     */
    public function handle(): void
    {
        $this->info();

        AtomicLocks::lockSkip(120, $this->getId(), function() {
            $service = $this->getPricesUpdaterService();

            try {
                $service->update();
            } finally {
                $this->setLeftWeight(
                    $service->leftWeight(),
                    $service->leftWeightAt(),
                );
            }
        });
    }

    /**
     * {@inheritDoc}
     */
    public function getId(): string
    {
        return 'market:price-updater';
    }

    /**
     * {@inheritDoc}
     */
    public function getWeight(): int
    {
        return 2;
    }

    /**
     * Determine the time at which the job should timeout.
     *
     * @return DateTime
     */
    public function retryUntil(): DateTime
    {
        return now()->addMinutes(5);
    }

    /**
     * Get the middleware the job should pass through.
     *
     * @return array
     */
    public function middleware(): array
    {
        return [
            (new WithoutOverlapping($this->getId()))
                ->expireAfter(300)
                ->dontRelease(),
            new RateLimited($this->getId(), 5, true),
            new BinanceWeightLimited($this->getWeight()),
        ];
    }

    /**
     * Get {@see PricesUpdaterService::class}.
     *
     * @return PricesUpdaterService
     */
    private function getPricesUpdaterService(): PricesUpdaterService
    {
        return app(PricesUpdaterService::class);
    }
}
