<?php
declare(strict_types=1);

namespace App\Modules\Market\Services;

use App\Modules\Core\Contracts\Services\ServiceContract;
use App\Modules\Market\Contracts\MarketModuleContract;
use App\Modules\Market\Models\Pair;
use App\Modules\Market\Repositories\PairsRepository;
use Illuminate\Support\Collection;

/**
 * Market data service.
 */
final class MarketDataService implements ServiceContract, MarketModuleContract
{
    /**
     * Get pairs.
     *
     * @param int $provider
     * @return Collection
     */
    public function getTradedPairs(
        int $provider
    ): Collection
    {
        return $this->getPairsRepository()
            ->getTradedListByProvider($provider);
    }

    /**
     * Find pair by pair code.
     *
     * @param string $pair
     * @param int $provider
     * @return Pair|null
     */
    public function findByPair(
        string $pair,
        int $provider,
    ): ?Pair
    {
        return $this->getPairsRepository()
            ->findByPair($pair, $provider);
    }

    /**
     * Get {@see PairsRepository::class}.
     *
     * @return PairsRepository
     */
    private function getPairsRepository(): PairsRepository
    {
        return app(PairsRepository::class);
    }
}
