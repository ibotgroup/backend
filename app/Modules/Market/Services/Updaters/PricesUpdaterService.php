<?php
declare(strict_types=1);

namespace App\Modules\Market\Services\Updaters;


use App\Modules\Core\Contracts\Services\DataUpdaterContract;
use App\Modules\Core\Contracts\Jobs\WeightControlled;
use App\Modules\Core\Exceptions\Services\Network\RequestException;
use App\Modules\Core\Traits\Tasks\WeightControl;
use App\Modules\Exchanges\Enum\ProviderEnum;
use App\Modules\Exchanges\Exceptions\Providers\BinanceProvider\ErrorMsgReceivedRequestException;
use App\Modules\Exchanges\Exceptions\Providers\BinanceProvider\GeneralRequestException;
use App\Modules\Exchanges\Exceptions\Providers\BinanceProvider\RequestIssueRequestException;
use App\Modules\Exchanges\Exceptions\Providers\BinanceProvider\UnknownRequestException;
use App\Modules\Exchanges\Services\BinanceService;
use App\Modules\Market\Contracts\MarketModuleContract;
use App\Modules\Market\Models\Pair;
use App\Modules\Market\Repositories\PairsRepository;
use DateTime;
use Illuminate\Contracts\Cache\LockTimeoutException;
use stdClass;

/**
 * PricesUpdaterService.
 */
final class PricesUpdaterService implements DataUpdaterContract, MarketModuleContract, WeightControlled
{
    use WeightControl;

    /**
     * Update.
     *
     * @throws RequestException
     * @throws ErrorMsgReceivedRequestException
     * @throws GeneralRequestException
     * @throws RequestIssueRequestException
     * @throws UnknownRequestException
     * @throws LockTimeoutException
     */
    public function update(): void
    {
        $binance = $this->getSystemBinance()
            ->getSystemBinance();

        try {
            $tickers = collect($binance->getTickerPrice());

            $pairs = $this->getPairsRepository()
                ->getListByProvider(ProviderEnum::BINANCE_SPOT);

            $pairs->each(static function (Pair $pair) use ($tickers) {
                $ticker = $tickers->filter(fn(stdClass $ticker) => $ticker->symbol === $pair->pair)
                    ->first();

                if ($ticker) {
                    $pair->last_price = (float)$ticker->price;
                    $pair->last_price_at = new DateTime();
                    $pair->save();
                }
            });
        } finally {
            $this->setLeftWeight(
                $binance->leftWeight(),
                $binance->leftWeightAt(),
            );
        }
    }


    /**
     * Get {@see PairsRepository::class}.
     *
     * @return PairsRepository
     */
    private function getPairsRepository(): PairsRepository
    {
        return app(PairsRepository::class);
    }

    /**
     * Get {@see BinanceService::class}.
     *
     * @return BinanceService
     */
    private function getSystemBinance(): BinanceService
    {
        return app(BinanceService::class);
    }
}
