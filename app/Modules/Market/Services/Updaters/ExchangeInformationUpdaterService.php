<?php
declare(strict_types=1);

namespace App\Modules\Market\Services\Updaters;

use App\Modules\Core\Contracts\Services\DataUpdaterContract;
use App\Modules\Core\Contracts\Jobs\WeightControlled;
use App\Modules\Core\Exceptions\Services\Network\RequestException;
use App\Modules\Core\Traits\Tasks\WeightControl;
use App\Modules\Exchanges\Enum\ProviderEnum;
use App\Modules\Exchanges\Services\BinanceService;
use App\Modules\Market\Contracts\MarketModuleContract;
use App\Modules\Market\Models\OrderType;
use App\Modules\Market\Models\Pair;
use App\Modules\Market\Repositories\PairsRepository;
use Illuminate\Support\Collection;
use stdClass;

/**
 * ExchangeInformationUpdaterService.
 */
final class ExchangeInformationUpdaterService implements DataUpdaterContract, MarketModuleContract, WeightControlled
{
    use WeightControl;

    /**
     * Update symbols in database from system binance api.
     *
     * @throws RequestException
     */
    public function update(): void
    {
        $binance = $this->getSystemBinance()
            ->getSystemBinance();

        try {
            $exchangeInfo = $binance->getExchangeInfo();

            $symbolsCollection = collect($exchangeInfo->symbols);
            $symbols = $symbolsCollection
                ->map(fn(stdClass $symbol) => $symbol->symbol);

            $exists = $this->getPairsRepository()
                ->getListByProvider(ProviderEnum::BINANCE_SPOT);

            $existsPairs = $exists->pluck('pair');

            $this->addNewPairs($symbols->diff($existsPairs), $symbolsCollection);
            $this->removePairs($existsPairs->diff($symbols));
            $this->updatePairs($symbolsCollection, $exists);
        } finally {
            $this->setLeftWeight($binance->leftWeight(), $binance->leftWeightAt());
        }
    }

    /**
     * Add new pairs.
     *
     * @param Collection $newPairs
     * @param Collection $symbolsCollection
     */
    private function addNewPairs(Collection $newPairs, Collection $symbolsCollection): void
    {
        $newPairs->each(function (string $newPair) use ($symbolsCollection) {
            $symbol = $symbolsCollection
                ->filter(fn(stdClass $symbol) => $symbol->symbol === $newPair)
                ->first();

            if (!$symbol) {
                return;
            }

            $pair = new Pair();
            $pair->pair = $newPair;
            $pair->provider = ProviderEnum::BINANCE_SPOT;
            $this->setPairData($pair, $symbol);

            $pair->save();
        });
    }

    /**
     * Update pairs.
     *
     * @param Collection $symbolsCollection
     * @param Collection $exists
     */
    private function updatePairs(Collection $symbolsCollection, Collection $exists): void
    {
        $exists->each(function (Pair $pair) use ($symbolsCollection) {
            $symbol = $symbolsCollection
                ->filter(fn(stdClass $symbol) => $symbol->symbol === $pair->pair)
                ->first();

            if (!$symbol) {
                return;
            }

            $this->setPairData($pair, $symbol);

            $pair->save();
        });
    }

    /**
     * Remove pairs.
     *
     * @param Collection $removedPairs
     */
    private function removePairs(Collection $removedPairs): void
    {
        $removedPairs->each(function (string $removedPair) {
            $this->getPairsRepository()
                ->findByPair($removedPair, ProviderEnum::BINANCE_SPOT)
                ?->delete();
        });
    }

    /**
     * Update pair data.
     *
     * @param Pair $pair
     * @param stdClass $symbol
     */
    private function setPairData(Pair $pair, stdClass $symbol): void
    {
        $filters = collect($symbol->filters)
            ->filter(fn(stdClass $filter) => $filter->filterType === 'PRICE_FILTER')
            ->first();

        $lots = collect($symbol->filters)
            ->filter(fn(stdClass $filter) => $filter->filterType === 'LOT_SIZE')
            ->first();

        $minNotional = collect($symbol->filters)
            ->filter(fn(stdClass $filter) => $filter->filterType === 'MIN_NOTIONAL')
            ->first();

        $maxNumOrders = collect($symbol->filters)
            ->filter(fn(stdClass $filter) => $filter->filterType === 'MAX_NUM_ORDERS')
            ->first();

        $pair->base_asset_precision = $symbol->baseAssetPrecision;
        $pair->quote_asset_precision = $symbol->quoteAssetPrecision;
        $pair->tick_size = $filters->tickSize;
        $pair->step_size = $lots->stepSize;
        $pair->is_spot_trading_allowed = $symbol->isSpotTradingAllowed;
        $pair->is_margin_trading_allowed = $symbol->isMarginTradingAllowed;
        $pair->filter_price_min = $filters->minPrice;
        $pair->filter_price_max = $filters->maxPrice;
        $pair->filter_lot_min_qty = $lots->minQty;
        $pair->filter_lot_max_qty = $lots->maxQty;
        $pair->filter_min_notional = $minNotional->minNotional;
        $pair->filter_max_num_orders = $maxNumOrders->maxNumOrders;

        $pair->save();

        $list = collect($symbol->orderTypes)
            ->diff(
                $pair->orderTypes
                    ->pluck('type')
            );

        $list->each(static function (string $type) use ($pair) {
            $orderType = new OrderType();
            $orderType->pair()
                ->associate($pair);
            $orderType->type = $type;
            $orderType->save();
        });
    }

    /**
     * Get {@see PairsRepository::class}.
     *
     * @return PairsRepository
     */
    private function getPairsRepository(): PairsRepository
    {
        return app(PairsRepository::class);
    }

    /**
     * Get {@see BinanceService::class}.
     *
     * @return BinanceService
     */
    private function getSystemBinance(): BinanceService
    {
        return app(BinanceService::class);
    }
}
