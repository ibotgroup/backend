<?php
declare(strict_types=1);

namespace App\Modules\Market\Dto\Api\Pair;

use App\Modules\Core\Dto\BaseDto;
use Carbon\Carbon;

/**
 * Pair dto.
 *
 * @property-read string $pair
 * @property-read int $provider
 * @property-read int $baseAssetPrecision
 * @property-read int $quoteAssetPrecision
 * @property-read float $tickSize
 * @property-read bool $isSpotTradingAllowed
 * @property-read bool $isMarginTradingAllowed
 * @property-read float $filterPriceMin
 * @property-read float|null $filterPriceMax
 * @property-read float|null $filterLotMinQty
 * @property-read float|null $filterLotMaxQty
 * @property-read float|null $filterMinNotional
 * @property-read float|null $filterMaxNumOrders
 * @property-read float|null $stepSize
 * @property-read float|null $lastPrice
 * @property-read Carbon|null $lastPriceAt
 */
final class PairDto extends BaseDto
{
    /**
     * Constructor.
     *
     * @param string $pair
     * @param int $provider
     * @param int $baseAssetPrecision
     * @param int $quoteAssetPrecision
     * @param float $tickSize
     * @param bool $isSpotTradingAllowed
     * @param bool $isMarginTradingAllowed
     * @param float $filterPriceMin
     * @param float|null $filterPriceMax
     * @param float|null $filterLotMinQty
     * @param float|null $filterLotMaxQty
     * @param float|null $filterMinNotional
     * @param float|null $filterMaxNumOrders
     * @param float|null $stepSize
     * @param float|null $lastPrice
     * @param Carbon|null $lastPriceAt
     */
    public function __construct(
        protected string $pair,
        protected int $provider,
        protected int $baseAssetPrecision,
        protected int $quoteAssetPrecision,
        protected float $tickSize,
        protected bool $isSpotTradingAllowed,
        protected bool $isMarginTradingAllowed,
        protected float $filterPriceMin,
        protected ?float $filterPriceMax = null,
        protected ?float $filterLotMinQty = null,
        protected ?float $filterLotMaxQty = null,
        protected ?float $filterMinNotional = null,
        protected ?float $filterMaxNumOrders = null,
        protected ?float $stepSize = null,
        protected ?float $lastPrice = null,
        protected ?Carbon $lastPriceAt = null,
    )
    {
    }
}
