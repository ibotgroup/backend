<?php
declare(strict_types=1);

namespace App\Modules\Market\Commands;

use App\Modules\Market\Services\Updaters\PricesUpdaterService;
use App\Modules\Core\Exceptions\Services\Network\RequestException;
use Illuminate\Console\Command;
use Illuminate\Contracts\Cache\LockTimeoutException;

/**
 * Get prices command.
 */
final class GetPricesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'market:get-prices';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update market prices';

    /**
     * Execute the console command.
     *
     * @return int
     * @throws RequestException
     * @throws LockTimeoutException
     */
    public function handle(): int
    {
        $this->getPricesUpdaterService()
            ->update();

        return 0;
    }

    /**
     * Get {@see PricesUpdaterService::class}.
     *
     * @return PricesUpdaterService
     */
    private function getPricesUpdaterService(): PricesUpdaterService
    {
        return app(PricesUpdaterService::class);
    }
}
