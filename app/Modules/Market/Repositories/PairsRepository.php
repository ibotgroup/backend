<?php
declare(strict_types=1);

namespace App\Modules\Market\Repositories;

use App\Modules\Bots\Enum\Trades\OrderTypeEnum;
use App\Modules\Core\Contracts\Repositories\RepositoryContract;
use App\Modules\Core\Exceptions\Repositories\InArgumentArrayIsEmptyException;
use App\Modules\Core\Repositories\BaseRepository;
use App\Modules\Market\Contracts\MarketModuleContract;
use App\Modules\Market\Models\Pair;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

/**
 * Pairs repository.
 */
final class PairsRepository extends BaseRepository implements MarketModuleContract, RepositoryContract
{
    /**
     * Constructor.
     *
     * @param Pair $model
     */
    public function __construct(protected Pair $model)
    {
    }

    /**
     * Find by pair.
     *
     * @param string $pair
     * @param int $provider
     * @return Pair|null
     */
    public function findByPair(
        string $pair,
        int $provider,
    ): ?Pair
    {
        return $this->model
            ->newQuery()
            ->where('pair', $pair)
            ->where('provider', $provider)
            ->first();
    }

    /**
     * Get traded list by provider
     *
     * @param int $provider
     * @return Collection
     */
    public function getTradedListByProvider(
        int $provider,
    ): Collection
    {
        return $this->model
            ->newQuery()
            ->where('provider', $provider)
            ->whereHas('orderTypes', function (Builder $builder) {
                $builder->where('type',OrderTypeEnum::TYPE_LIMIT_LABEL);
            })
            ->where('is_spot_trading_allowed', true)
            ->orderBy('pair')
            ->get();
    }

    /**
     * Get list by provider
     *
     * @param int $provider
     * @return Collection
     */
    public function getListByProvider(
        int $provider,
    ): Collection
    {
        return $this->model
            ->newQuery()
            ->where('provider', $provider)
            ->orderBy('pair')
            ->get();
    }

    /**
     * Get pairs that exists.
     *
     * @param array $pairs
     * @param int $provider
     * @return Collection
     * @throws InArgumentArrayIsEmptyException
     */
    public function getListExists(
        array $pairs,
        int $provider,
    ): Collection
    {
        $this->checkInArgument($pairs);

        return $this->model
            ->newQuery()
            ->whereIn('pair', $pairs)
            ->where('provider', $provider)
            ->get();
    }

    /**
     * Get pairs not exists.
     *
     * @param array $pairs
     * @param int $provider
     * @return Collection
     * @throws InArgumentArrayIsEmptyException
     */
    public function getListThatNotExists(
        array $pairs,
        int $provider,
    ): Collection
    {
        $this->checkInArgument($pairs);

        return $this->model
            ->newQuery()
            ->whereNotIn('pair', $pairs)
            ->where('provider', $provider)
            ->get();
    }
}
