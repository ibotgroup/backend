<?php
declare(strict_types=1);

namespace App\Modules\Market\Models;

use App\Modules\Core\Models\Model;
use App\Modules\Market\Contracts\MarketModuleContract;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Order type.
 *
 * @property-read int $id
 * @property-read int $pairs_id
 * @property string $type
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Pair $pair
 */
final class OrderType extends Model implements MarketModuleContract
{
    /** {@inheritdoc} */
    protected $table = 'orders_types';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'pairs_id' => 'integer',
        'type' => 'string',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    /**
     * {@see Pair::class}.
     *
     * @return BelongsTo
     */
    public function pair(): BelongsTo
    {
        return $this->belongsTo(Pair::class, 'pairs_id');
    }
}
