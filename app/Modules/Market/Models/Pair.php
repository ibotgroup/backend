<?php
declare(strict_types=1);

namespace App\Modules\Market\Models;

use App\Modules\Core\Contracts\Models\Api;
use App\Modules\Core\Contracts\Models\ApiOption;
use App\Modules\Core\Dto\Api\ApiOptionDto;
use App\Modules\Core\Exceptions\Models\ModelNotExistsException;
use App\Modules\Core\Models\Model;
use App\Modules\Market\Contracts\MarketModuleContract;
use App\Modules\Market\Dto\Api\Pair\PairDto;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;
use JetBrains\PhpStorm\Pure;

/**
 * Pair model.
 *
 * @property-read int $id
 * @property string $pair
 * @property int $provider
 * @property int $base_asset_precision
 * @property int $quote_asset_precision
 * @property string $tick_size
 * @property string $step_size
 * @property float $last_price
 * @property Carbon|null $last_price_at
 * @property boolean $is_spot_trading_allowed
 * @property boolean $is_margin_trading_allowed
 * @property float|null $filter_price_min
 * @property float|null $filter_price_max
 * @property float|null $filter_lot_min_qty
 * @property float|null $filter_lot_max_qty
 * @property float|null $filter_min_notional
 * @property float|null $filter_max_num_orders
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property Collection|OrderType[] $orderTypes
 * @property-read float $tickSizeAsFloat
 * @property-read float $stepSizeAsFloat
 */
final class Pair extends Model implements MarketModuleContract, ApiOption, Api
{
    /** {@inheritdoc} */
    protected $table = 'pairs';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'pair' => 'string',
        'provider' => 'integer',
        'base_asset_precision' => 'integer',
        'quote_asset_precision' => 'integer',
        'tick_size' => 'string',
        'step_size' => 'string',
        'last_price' => 'float',
        'last_price_at' => 'datetime',
        'is_spot_trading_allowed' => 'boolean',
        'is_margin_trading_allowed' => 'boolean',
        'filter_price_min' => 'float',
        'filter_price_max' => 'float',
        'filter_lot_min_qty' => 'float',
        'filter_lot_max_qty' => 'float',
        'filter_min_notional' => 'float',
        'filter_max_num_orders' => 'float',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    /**
     * {@see OrderType::class}.
     *
     * @return HasMany
     */
    public function orderTypes(): HasMany
    {
        return $this->hasMany(OrderType::class, 'pairs_id');
    }

    /**
     * Get attribute "tickSizeAsFloat".
     *
     * @return float
     */
    public function getTickSizeAsFloatAttribute(): float
    {
        return (float)$this->tick_size;
    }

    /**
     * Get attribute "stepSizeAsFloat".
     *
     * @return float
     */
    public function getStepSizeAsFloatAttribute(): float
    {
        return (float)$this->step_size;
    }

    /**
     * To api option.
     *
     * @return ApiOptionDto
     */
    #[Pure]
    public function toApiOption(): ApiOptionDto
    {
        return new ApiOptionDto(
            $this->pair,
            $this->pair
        );
    }

    /**
     * To api dto.
     *
     * @return PairDto
     * @throws ModelNotExistsException
     */
    public function toApi(): PairDto
    {
        $this->throwIfNotExists();

        return new PairDto(
            $this->pair,
            $this->provider,
            $this->base_asset_precision,
            $this->quote_asset_precision,
            $this->tickSizeAsFloat,
            $this->is_spot_trading_allowed,
            $this->is_margin_trading_allowed,
            $this->filter_price_min,
            $this->filter_price_max,
            $this->filter_lot_min_qty,
            $this->filter_lot_max_qty,
            $this->filter_min_notional,
            $this->filter_max_num_orders,
            $this->stepSizeAsFloat,
            $this->last_price,
            $this->last_price_at,
        );
    }
}
