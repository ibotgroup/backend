<?php
declare(strict_types=1);

namespace App\Modules\Bots\Contracts\Services\Traders\Grid;

use App\Modules\Bots\Contracts\BotsModuleContract;
use App\Modules\Bots\Dto\Traders\Grid\GridOptionsDto;
use Illuminate\Support\Collection;

/**
 * ModeContract.
 */
interface ModeContract extends BotsModuleContract
{
    /**
     * Constructor.
     *
     * @param GridOptionsDto $options
     */
    public function __construct(GridOptionsDto $options);

    /**
     * Get price lines.
     *
     * @return Collection
     */
    public function getPriceLines(): Collection;
}
