<?php
declare(strict_types=1);

namespace App\Modules\Bots\Contracts\Models\Algorithms;

use App\Modules\Bots\Contracts\BotsModuleContract;
use App\Modules\Core\Models\ModelContract;

/**
 * AlgorithmInterface.
 */
interface AlgorithmContract extends ModelContract, BotsModuleContract
{
}
