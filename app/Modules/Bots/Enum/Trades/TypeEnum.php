<?php
declare(strict_types=1);

namespace App\Modules\Bots\Enum\Trades;

use App\Modules\Bots\Contracts\BotsModuleContract;
use App\Modules\Core\Enum\EnumContract;

/**
 * Type enum.
 */
final class TypeEnum implements EnumContract, BotsModuleContract
{
    public const TYPE_BUY = 1;
    public const TYPE_SELL = 2;

    public const TYPES = [
        self::TYPE_BUY,
        self::TYPE_SELL,
    ];

    public const TYPES_TEXT = [
        self::TYPE_BUY => 'BUY',
        self::TYPE_SELL => 'SELL'
    ];
}
