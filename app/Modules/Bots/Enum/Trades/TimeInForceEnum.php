<?php
declare(strict_types=1);

namespace App\Modules\Bots\Enum\Trades;

use App\Modules\Bots\Contracts\BotsModuleContract;
use App\Modules\Core\Enum\EnumContract;

/**
 * Time in force enum.
 */
final class TimeInForceEnum implements EnumContract, BotsModuleContract
{
    public const GTC = 'GTC';
    public const IOC = 'IOC';
    public const FOK = 'FOK';
}
