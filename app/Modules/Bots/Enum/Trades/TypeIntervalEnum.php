<?php
declare(strict_types=1);

namespace App\Modules\Bots\Enum\Trades;

use App\Modules\Bots\Contracts\BotsModuleContract;
use App\Modules\Core\Enum\EnumContract;

/**
 * Type interval enum.
 */
final class TypeIntervalEnum implements EnumContract, BotsModuleContract
{
    public const INTERVAL_MINUTE_1 = '1m';
    public const INTERVAL_MINUTE_3 = '3m';
    public const INTERVAL_MINUTE_5 = '5m';
    public const INTERVAL_MINUTE_15 = '15m';
    public const INTERVAL_MINUTE_30 = '30m';
    public const INTERVAL_HOUR_1 = '1h';
    public const INTERVAL_HOUR_2 = '2h';
    public const INTERVAL_HOUR_4 = '4h';
    public const INTERVAL_HOUR_6 = '6h';
    public const INTERVAL_HOUR_8 = '8h';
    public const INTERVAL_HOUR_12 = '12h';
    public const INTERVAL_DAY_1 = '1d';
    public const INTERVAL_DAY_3 = '3d';
    public const INTERVAL_WEEK_1 = '1w';
    public const INTERVAL_MONTH_1 = '1M';

    public const INTERVALS = [
        self::INTERVAL_MINUTE_1,
        self::INTERVAL_MINUTE_3,
        self::INTERVAL_MINUTE_5,
        self::INTERVAL_MINUTE_15,
        self::INTERVAL_MINUTE_30,
        self::INTERVAL_HOUR_1,
        self::INTERVAL_HOUR_2,
        self::INTERVAL_HOUR_4,
        self::INTERVAL_HOUR_6,
        self::INTERVAL_HOUR_8,
        self::INTERVAL_HOUR_12,
        self::INTERVAL_DAY_1,
        self::INTERVAL_DAY_3,
        self::INTERVAL_WEEK_1,
        self::INTERVAL_MONTH_1,
    ];
}
