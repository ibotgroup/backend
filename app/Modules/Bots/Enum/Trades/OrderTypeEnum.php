<?php
declare(strict_types=1);

namespace App\Modules\Bots\Enum\Trades;

use App\Modules\Bots\Contracts\BotsModuleContract;
use App\Modules\Core\Enum\EnumContract;

/**
 * Order enum.
 */
final class OrderTypeEnum implements EnumContract, BotsModuleContract
{
    public const TYPE_LIMIT = 1;
    public const TYPE_MARKET = 2;

    public const TYPE_LIMIT_LABEL = 'LIMIT';
    public const TYPE_MARKET_LABEL = 'MARKET';

    public const TYPES = [
        self::TYPE_LIMIT,
        self::TYPE_MARKET,
    ];

    public const TYPES_TEXT = [
        self::TYPE_LIMIT => self::TYPE_LIMIT_LABEL,
        self::TYPE_MARKET => self::TYPE_MARKET_LABEL,
    ];
}
