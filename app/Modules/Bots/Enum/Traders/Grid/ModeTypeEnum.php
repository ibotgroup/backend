<?php
declare(strict_types=1);

namespace App\Modules\Bots\Enum\Traders\Grid;

use App\Modules\Bots\Contracts\BotsModuleContract;
use App\Modules\Core\Enum\EnumContract;

/**
 * Mode type enum.
 */
final class ModeTypeEnum implements EnumContract, BotsModuleContract
{
    public const TYPE_ARITHMETIC = 1;
    public const TYPE_GEOMETRIC = 2;
}
