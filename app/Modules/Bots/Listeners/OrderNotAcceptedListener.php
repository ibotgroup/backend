<?php
declare(strict_types=1);

namespace App\Modules\Bots\Listeners;

use App\Modules\Bots\Services\BotsService;
use App\Modules\Core\Contracts\EventContract;
use App\Modules\Core\Contracts\ListenerContract;
use App\Modules\Deals\Events\OrderNotAcceptedEvent;
use Throwable;

/**
 * Order not accepted listener.
 */
final class OrderNotAcceptedListener implements ListenerContract
{
    /**
     * {@inheritDoc}
     * @throws Throwable
     */
    public function handle(OrderNotAcceptedEvent|EventContract $event): void
    {
        $this->getBotsService()
            ->stopByErrorReason($event->deal->bot, $event->dealError);
    }

    /**
     * Get {@see BotsService::class}.
     *
     * @return BotsService
     */
    private function getBotsService(): BotsService
    {
        return app(BotsService::class);
    }
}
