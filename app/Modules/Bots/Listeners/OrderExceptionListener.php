<?php
declare(strict_types=1);

namespace App\Modules\Bots\Listeners;

use App\Modules\Bots\Services\TraderService;
use App\Modules\Core\Contracts\EventContract;
use App\Modules\Core\Contracts\ListenerContract;
use App\Modules\Deals\Events\OrdersExceptionEvent;
use Throwable;

/**
 * Order exceptions listener.
 */
final class OrderExceptionListener implements ListenerContract
{
    /**
     * {@inheritDoc}
     * @throws Throwable
     */
    public function handle(OrdersExceptionEvent|EventContract $event): void
    {
        $this->getTraderService()
            ->dealsProcessed($event->bot, false, true);
    }

    /**
     * Get {@see TraderService::class}.
     *
     * @return TraderService
     */
    private function getTraderService(): TraderService
    {
        return app(TraderService::class);
    }
}
