<?php
declare(strict_types=1);

namespace App\Modules\Bots\Listeners;

use App\Modules\Bots\Exceptions\Services\Trader\BotIterationIsNotPendingOrMonitoringException;
use App\Modules\Bots\Services\TraderService;
use App\Modules\Core\Contracts\EventContract;
use App\Modules\Core\Contracts\ListenerContract;
use App\Modules\Deals\Events\OrdersProcessedEvent;
use Throwable;

/**
 * Order processed listener.
 */
final class OrdersProcessedListener implements ListenerContract
{
    /**
     * {@inheritDoc}
     * @throws Throwable
     */
    public function handle(OrdersProcessedEvent|EventContract $event): void
    {
        try {
            $this->getTraderService()
                ->dealsProcessed($event->bot, $event->iterationCompleted);
        }catch (BotIterationIsNotPendingOrMonitoringException) {}
    }

    /**
     * Get {@see TraderService::class}.
     *
     * @return TraderService
     */
    private function getTraderService(): TraderService
    {
        return app(TraderService::class);
    }
}
