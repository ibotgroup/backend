<?php
declare(strict_types=1);

namespace App\Modules\Bots\Exceptions\Traders\GridTrader;

use Exception;

/**
 * GridMustGreatThanZeroException.
 */
class GridMustGreatThanZeroException extends Exception implements GridTraderException
{
}
