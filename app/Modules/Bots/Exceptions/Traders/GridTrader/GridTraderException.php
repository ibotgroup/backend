<?php
declare(strict_types=1);

namespace App\Modules\Bots\Exceptions\Traders\GridTrader;

use App\Modules\Bots\Exceptions\Traders\TradersException;

/**
 * GridTraderException.
 */
interface GridTraderException extends TradersException
{

}
