<?php
declare(strict_types=1);

namespace App\Modules\Bots\Exceptions\Traders\GridTrader\Conditions;

use Exception;

/**
 * CurrentPriceOutOfGridException.
 */
class CurrentPriceOutOfGridException extends Exception implements ConditionsException
{
}
