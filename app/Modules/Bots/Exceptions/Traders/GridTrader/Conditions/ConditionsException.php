<?php
declare(strict_types=1);

namespace App\Modules\Bots\Exceptions\Traders\GridTrader\Conditions;

use App\Modules\Bots\Exceptions\Traders\GridTrader\GridTraderException;

/**
 * ConditionsException.
 */
interface ConditionsException extends GridTraderException
{
}
