<?php
declare(strict_types=1);

namespace App\Modules\Bots\Exceptions\Traders\GridTrader\Conditions;

use Exception;
use JetBrains\PhpStorm\Pure;
use Throwable;

/**
 * InitialMarginLessThanMinimalException.
 */
class AmountPerOrderLessThanMinimalException extends Exception implements ConditionsException
{
    /**
     * Constructor.
     *
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     * @param float|null $minQuantity
     */
    #[Pure]
    public function __construct($message = "", $code = 0, Throwable $previous = null, private ?float $minQuantity = null)
    {
        parent::__construct($message, $code, $previous);
    }

    /**
     * Get min quantity.
     *
     * @return float|null
     */
    public function getMinQuantity(): ?float
    {
        return $this->minQuantity;
    }
}
