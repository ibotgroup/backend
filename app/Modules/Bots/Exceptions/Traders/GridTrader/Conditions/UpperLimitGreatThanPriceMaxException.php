<?php
declare(strict_types=1);

namespace App\Modules\Bots\Exceptions\Traders\GridTrader\Conditions;

use Exception;

/**
 * UpperLimitGreatThanPriceMaxException.
 */
class UpperLimitGreatThanPriceMaxException extends Exception implements ConditionsException
{
}
