<?php
declare(strict_types=1);

namespace App\Modules\Bots\Exceptions\Traders\GridTrader\Conditions;

use Exception;

/**
 * CountOrdersLessThanThreeException.
 */
class CountOrdersLessThanThreeException extends Exception implements ConditionsException
{

}
