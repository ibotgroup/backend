<?php
declare(strict_types=1);

namespace App\Modules\Bots\Exceptions\Traders\GridTrader\Conditions;

use Exception;

/**
 * LowerLimitLessThanPriceMinException.
 */
class LowerLimitLessThanPriceMinException extends Exception implements ConditionsException
{
}
