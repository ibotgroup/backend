<?php
declare(strict_types=1);

namespace App\Modules\Bots\Exceptions\Traders\GridTrader\Conditions;

use Exception;

/**
 * LowerPriceGreatThanHighPriceException.
 */
class LowerLimitGreatThanUpperLimitException extends Exception implements ConditionsException
{
}
