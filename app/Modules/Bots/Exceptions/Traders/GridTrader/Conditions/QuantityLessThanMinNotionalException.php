<?php
declare(strict_types=1);

namespace App\Modules\Bots\Exceptions\Traders\GridTrader\Conditions;

use Exception;

/**
 * QuantityLessThanMinNotionalException.
 */
class QuantityLessThanMinNotionalException extends Exception implements ConditionsException
{

}
