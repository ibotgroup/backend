<?php
declare(strict_types=1);

namespace App\Modules\Bots\Exceptions\Traders\GridTrader;

use Exception;

/**
 * UnsupportedTypeException.
 */
class UnsupportedTypeException extends Exception implements GridTraderException
{
}
