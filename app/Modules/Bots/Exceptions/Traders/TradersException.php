<?php
declare(strict_types=1);

namespace App\Modules\Bots\Exceptions\Traders;

use App\Modules\Bots\Exceptions\BotException;

/**
 * TradersException.
 */
interface TradersException extends BotException
{
}
