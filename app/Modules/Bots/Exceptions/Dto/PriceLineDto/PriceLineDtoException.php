<?php
declare(strict_types=1);

namespace App\Modules\Bots\Exceptions\Dto\PriceLineDto;

use App\Modules\Bots\Exceptions\Dto\DtoException;

/**
 * PriceLineDtoException.
 */
interface PriceLineDtoException extends DtoException
{
}
