<?php
declare(strict_types=1);

namespace App\Modules\Bots\Exceptions\Dto\PriceLineDto;

/**
 * TypeIncorrectException.
 */
class TypeIncorrectException extends \Exception implements PriceLineDtoException
{

}
