<?php
declare(strict_types=1);

namespace App\Modules\Bots\Exceptions\Dto;

use App\Modules\Bots\Exceptions\BotException;

/**
 * DtoException.
 */
interface DtoException extends BotException
{
}
