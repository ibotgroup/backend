<?php
declare(strict_types=1);

namespace App\Modules\Bots\Exceptions\Services;

use App\Modules\Bots\Exceptions\BotException;

/**
 * ServicesException.
 */
interface ServicesException extends BotException
{
}
