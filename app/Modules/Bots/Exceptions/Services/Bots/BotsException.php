<?php
declare(strict_types=1);

namespace App\Modules\Bots\Exceptions\Services\Bots;

use App\Modules\Bots\Exceptions\Services\ServicesException;

/**
 * BotsException.
 */
interface BotsException extends ServicesException
{
}
