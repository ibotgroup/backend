<?php
declare(strict_types=1);

namespace App\Modules\Bots\Exceptions\Services\Bots;

use Exception;

/**
 * BotNotLockedException.
 */
final class BotNotLockedException extends Exception implements BotsException
{
}
