<?php
declare(strict_types=1);

namespace App\Modules\Bots\Exceptions\Services\Bots;

use Exception;

/**
 * BotAlreadyLockedException.
 */
final class BotAlreadyLockedException extends Exception implements BotsException
{
}
