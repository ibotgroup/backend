<?php
declare(strict_types=1);

namespace App\Modules\Bots\Exceptions\Services\Trader;

use Exception;

/**
 * StatusIsNotActiveException.
 */
final class StatusIsNotActiveException extends Exception implements TraderException
{

}
