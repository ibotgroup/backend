<?php
declare(strict_types=1);

namespace App\Modules\Bots\Exceptions\Services\Trader;

use Exception;

/**
 * PriceDoesNotUpdatedException.
 */
final class PriceDoesNotUpdatedException extends Exception implements TraderException
{

}
