<?php
declare(strict_types=1);

namespace App\Modules\Bots\Exceptions\Services\Trader;

use Exception;

/**
 * BotIterationIsNotPendingOrMonitoringException.
 */
final class BotIterationIsNotPendingOrMonitoringException extends Exception implements TraderException
{

}
