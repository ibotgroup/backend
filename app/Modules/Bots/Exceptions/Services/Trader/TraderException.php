<?php
declare(strict_types=1);

namespace App\Modules\Bots\Exceptions\Services\Trader;

use App\Modules\Bots\Exceptions\Services\ServicesException;

/**
 * TraderException.
 */
interface TraderException extends ServicesException
{
}
