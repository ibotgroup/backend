<?php
declare(strict_types=1);

namespace App\Modules\Bots\Exceptions\Services\Trader;

/**
 * Stopped exception.
 */
final class StoppedException extends \Exception implements TraderException
{
}
