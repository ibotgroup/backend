<?php
declare(strict_types=1);

namespace App\Modules\Bots\Exceptions\Services\Trader;

use Exception;

/**
 * BotIterationIsPendingException.
 */
final class BotIterationIsPendingException extends Exception implements TraderException
{

}
