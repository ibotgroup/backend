<?php
declare(strict_types=1);

namespace App\Modules\Bots\Exceptions\Enum\Trades;

use Exception;

/**
 * LimitOrderPriceRequiredException.
 */
class LimitOrderPriceRequiredException extends Exception implements TradesException
{

}
