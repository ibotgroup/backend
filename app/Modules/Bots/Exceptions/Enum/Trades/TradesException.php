<?php
declare(strict_types=1);

namespace App\Modules\Bots\Exceptions\Enum\Trades;

use App\Modules\Bots\Exceptions\Enum\EnumException;

/**
 * TradesException.
 */
interface TradesException extends EnumException
{
}
