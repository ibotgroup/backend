<?php
declare(strict_types=1);

namespace App\Modules\Bots\Exceptions\Enum;

use App\Modules\Bots\Exceptions\BotException;

/**
 * EnumException.
 */
interface EnumException extends BotException
{
}
