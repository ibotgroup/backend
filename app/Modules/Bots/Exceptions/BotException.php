<?php
declare(strict_types=1);

namespace App\Modules\Bots\Exceptions;

use App\Modules\Bots\Contracts\BotsModuleContract;
use Throwable;

/**
 * BotException.
 */
interface BotException extends Throwable, BotsModuleContract
{
}
