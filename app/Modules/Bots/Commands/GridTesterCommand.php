<?php
declare(strict_types=1);

namespace App\Modules\Bots\Commands;

use App\Modules\Bots\Dto\Traders\Grid\GridOptionsDto;
use App\Modules\Bots\Exceptions\Dto\PriceLineDto\TypeIncorrectException;
use App\Modules\Bots\Exceptions\Traders\GridTrader\Conditions\ConditionsException;
use App\Modules\Bots\Exceptions\Traders\GridTrader\Conditions\LowerLimitGreatThanUpperLimitException;
use App\Modules\Bots\Exceptions\Traders\GridTrader\Conditions\LowerLimitLessThanPriceMinException;
use App\Modules\Bots\Exceptions\Traders\GridTrader\Conditions\UpperLimitGreatThanPriceMaxException;
use App\Modules\Bots\Exceptions\Traders\GridTrader\GridMustGreatThanZeroException;
use App\Modules\Bots\Exceptions\Traders\GridTrader\GridTraderException;
use App\Modules\Bots\Exceptions\Traders\GridTrader\UnsupportedTypeException;
use App\Modules\Bots\Services\Traders\Grid\GridService;
use App\Modules\Core\Exceptions\Models\ModelNotExistsException;
use App\Modules\Exchanges\Enum\ProviderEnum;
use App\Modules\Market\Services\MarketDataService;
use Illuminate\Console\Command;

/**
 * Grid tester command.
 */
final class GridTesterCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bot:grid-tester {lowerPrice} {upperPrice} {amountPerOrder} {grids} {type} {pair}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Grid tester';

    /**
     * Execute the console command.
     *
     * @return int
     * @throws TypeIncorrectException
     * @throws ConditionsException
     * @throws LowerLimitGreatThanUpperLimitException
     * @throws LowerLimitLessThanPriceMinException
     * @throws UpperLimitGreatThanPriceMaxException
     * @throws GridMustGreatThanZeroException
     * @throws GridTraderException
     * @throws UnsupportedTypeException
     * @throws ModelNotExistsException
     */
    public function handle(): int
    {
        $pair = $this->getMarketDataService()
            ->findByPair(
                $this->argument('pair'),
                ProviderEnum::BINANCE_SPOT
            );

        $deals = $this->getGridService()
            ->gridTrade(
                new GridOptionsDto(
                    (float)$this->argument('lowerPrice'),
                    (float)$this->argument('upperPrice'),
                    $pair->last_price,
                    null,
                    (float)$this->argument('amountPerOrder'),
                    (int)$this->argument('grids'),
                    $pair->tickSizeAsFloat,
                    (int)$this->argument('type'),
                    $pair->toApi()
                )
            );

        dumpDealDtoCollection($deals, $pair);

        return 0;
    }

    /**
     * Get {@see GridService::class}.
     *
     * @return GridService
     */
    private function getGridService(): GridService
    {
        return app(GridService::class);
    }

    /**
     * Get {@see MarketDataService::class}.
     *
     * @return MarketDataService
     */
    private function getMarketDataService(): MarketDataService
    {
        return app(MarketDataService::class);
    }
}
