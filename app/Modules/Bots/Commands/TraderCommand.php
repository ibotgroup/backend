<?php
declare(strict_types=1);

namespace App\Modules\Bots\Commands;

use App\Modules\Bots\Exceptions\Dto\PriceLineDto\TypeIncorrectException;
use App\Modules\Bots\Exceptions\Services\Trader\BotIterationIsPendingException;
use App\Modules\Bots\Exceptions\Services\Trader\StatusIsNotActiveException;
use App\Modules\Bots\Exceptions\Services\Trader\StoppedException;
use App\Modules\Bots\Exceptions\Traders\GridTrader\Conditions\ConditionsException;
use App\Modules\Bots\Exceptions\Traders\GridTrader\Conditions\LowerLimitGreatThanUpperLimitException;
use App\Modules\Bots\Exceptions\Traders\GridTrader\Conditions\LowerLimitLessThanPriceMinException;
use App\Modules\Bots\Exceptions\Traders\GridTrader\Conditions\UpperLimitGreatThanPriceMaxException;
use App\Modules\Bots\Exceptions\Traders\GridTrader\GridMustGreatThanZeroException;
use App\Modules\Bots\Exceptions\Traders\GridTrader\GridTraderException;
use App\Modules\Bots\Exceptions\Traders\GridTrader\UnsupportedTypeException;
use App\Modules\Bots\Services\TraderService;
use App\Modules\Core\Exceptions\Models\ModelNotExistsException;
use App\Modules\Core\Exceptions\Traits\Lock\TransactionNotStartedException;
use Illuminate\Console\Command;
use Throwable;
use function sprintf;

/**
 * Trades command.
 */
final class TraderCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bot:trader';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Bot trader';

    /**
     * Execute the console command.
     *
     * @return int
     * @throws TypeIncorrectException
     * @throws BotIterationIsPendingException
     * @throws StatusIsNotActiveException
     * @throws StoppedException
     * @throws ConditionsException
     * @throws LowerLimitGreatThanUpperLimitException
     * @throws LowerLimitLessThanPriceMinException
     * @throws UpperLimitGreatThanPriceMaxException
     * @throws GridMustGreatThanZeroException
     * @throws GridTraderException
     * @throws UnsupportedTypeException
     * @throws ModelNotExistsException
     * @throws TransactionNotStartedException
     * @throws Throwable
     */
    public function handle(): int
    {
        $limit = 43200;

        while ($limit > 0) {
            $limit--;

            $count = $this->getTraderService()
                ->tradesUpdater();

            $this->info(sprintf("Was updated trades for %s bots.", $count));

            sleep(1);
        }

        return 0;
    }

    /**
     * Get {@see TraderService::class}.
     *
     * @return TraderService
     */
    private function getTraderService(): TraderService
    {
        return app(TraderService::class);
    }
}
