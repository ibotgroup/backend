<?php
declare(strict_types=1);

namespace App\Modules\Bots\Tasks;

use App\Modules\Bots\Exceptions\Services\Bots\BotNotLockedException;
use App\Modules\Bots\Services\TraderService;
use App\Modules\Core\Exceptions\Traits\Lock\TransactionNotStartedException;
use App\Modules\Core\Jobs\AbstractJob;
use App\Modules\Core\Jobs\RateLimited;
use App\Modules\Market\Contracts\MarketModuleContract;
use DateTime;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\Middleware\WithoutOverlapping;
use Illuminate\Queue\SerializesModels;
use Throwable;
use function app;

/**
 * TraderJob.
 */
final class TraderJob extends AbstractJob implements MarketModuleContract, ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Update prices in database from system binance api.
     *
     * @throws BotNotLockedException
     * @throws TransactionNotStartedException
     * @throws Throwable
     */
    public function handle(): void
    {
        $this->info();

        $this->getTraderService()
            ->tradesUpdater();
    }

    /**
     * {@inheritDoc}
     */
    public function getId(): string
    {
        return 'bots:trader';
    }

    /**
     * {@inheritDoc}
     */
    public function getWeight(): int
    {
        return 0;
    }

    /**
     * Determine the time at which the job should timeout.
     *
     * @return DateTime
     */
    public function retryUntil(): DateTime
    {
        return now()->addMinutes(5);
    }

    /**
     * Get the middleware the job should pass through.
     *
     * @return array
     */
    public function middleware(): array
    {
        return [
            (new WithoutOverlapping($this->getId()))
                ->expireAfter(300)
                ->dontRelease(),
            new RateLimited($this->getId(), 1, true)
        ];
    }

    /**
     * Get {@see TraderService::class}.
     *
     * @return TraderService
     */
    private function getTraderService(): TraderService
    {
        return app(TraderService::class);
    }
}
