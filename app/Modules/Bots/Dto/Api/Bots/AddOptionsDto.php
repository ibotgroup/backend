<?php
declare(strict_types=1);

namespace App\Modules\Bots\Dto\Api\Bots;

use App\Modules\Core\Dto\BaseDto;
use Illuminate\Support\Collection;

/**
 * This dto returns by api
 * for add form options.
 *
 * @property-read Collection $accesses
 * @property-read Collection $pairs
 */
final class AddOptionsDto extends BaseDto
{
    /**
     * Constructor.
     *
     * @param Collection $accesses
     * @param Collection $pairs
     */
    public function __construct(
        protected Collection $accesses,
        protected Collection $pairs,
    ) {
    }
}
