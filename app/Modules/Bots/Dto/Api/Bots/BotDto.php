<?php
declare(strict_types=1);

namespace App\Modules\Bots\Dto\Api\Bots;


use App\Modules\Accesses\Dto\Api\Access\AccessDto;
use App\Modules\Bots\Contracts\BotsModuleContract;
use App\Modules\Bots\Dto\Api\Algorithms\Algorithms\AlgorithmGridDto;
use App\Modules\Core\Dto\BaseDto;
use App\Modules\Market\Dto\Api\Pair\PairDto;

/**
 * Bot dto.
 *
 * @property-read string $id
 * @property-read AccessDto $access
 * @property-read AlgorithmGridDto $algorithm
 * @property-read PairDto $pair
 * @property-read int $status
 * @property-read string $statusText
 * @property-read string|null $stoppedReasonText
 * @property-read int $countDeals
 * @property-read string $createdAt
 */
final class BotDto extends BaseDto implements BotsModuleContract
{
    /**
     * Constructor.
     *
     * @param string $id
     * @param AccessDto $access
     * @param AlgorithmGridDto $algorithm
     * @param PairDto $pair
     * @param int $status
     * @param string $statusText
     * @param string|null $stoppedReasonText
     * @param int $countDeals
     * @param string $createdAt
     */
    public function __construct(
        protected string $id,
        protected AccessDto $access,
        protected AlgorithmGridDto $algorithm,
        protected PairDto $pair,
        protected int $status,
        protected string $statusText,
        protected ?string $stoppedReasonText,
        protected int $countDeals,
        protected string $createdAt,
    ) {
    }
}
