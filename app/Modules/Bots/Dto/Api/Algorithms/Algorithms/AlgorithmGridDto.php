<?php
declare(strict_types=1);

namespace App\Modules\Bots\Dto\Api\Algorithms\Algorithms;

use App\Modules\Core\Dto\BaseDto;

/**
 * Algorithm grid dto.
 *
 * @property-read int $type
 * @property-read string $typeText
 * @property-read float $lowerLimit
 * @property-read float $upperLimit
 * @property-read int $grids
 * @property-read float|null $initialMargin
 * @property-read float|null $amountPerOrder
 * @property-read boolean $closeAtStop
 * @property-read float|null $activationTrigger
 * @property-read float|null $lowStopTrigger
 * @property-read float|null $highStopTrigger
 */
class AlgorithmGridDto extends BaseDto
{
    /**
     * Constructor.
     *
     * @param int $type
     * @param string $typeText
     * @param float $lowerLimit
     * @param float $upperLimit
     * @param int $grids
     * @param float|null $initialMargin
     * @param float|null $amountPerOrder
     * @param bool $closeAtStop
     * @param float|null $activationTrigger
     * @param float|null $lowStopTrigger
     * @param float|null $highStopTrigger
     */
    public function __construct(
        protected int $type,
        protected string $typeText,
        protected float $lowerLimit,
        protected float $upperLimit,
        protected int $grids,
        protected ?float $initialMargin = null,
        protected ?float $amountPerOrder = null,
        protected bool $closeAtStop = false,
        protected ?float $activationTrigger = null,
        protected ?float $lowStopTrigger = null,
        protected ?float $highStopTrigger = null,
    ) {

    }
}
