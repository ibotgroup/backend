<?php
declare(strict_types=1);

namespace App\Modules\Bots\Dto\Traders\Deals;

use App\Modules\Bots\Enum\Trades\OrderTypeEnum;
use App\Modules\Bots\Enum\Trades\TypeEnum;
use App\Modules\Bots\Exceptions\Enum\Trades\LimitOrderPriceRequiredException;
use App\Modules\Core\Dto\BaseDto;
use JetBrains\PhpStorm\ExpectedValues;

/**
 * DealDto.
 *
 * @property-read int $type
 * @property-read int $orderType
 * @property-read float|null $price
 * @property-read float|null $marketPrice
 * @property-read float|null $quantity
 */
final class DealDto extends BaseDto
{
    /**
     * Constructor.
     *
     * @param int $type
     * @param int $orderType
     * @param float|null $price
     * @param float|null $marketPrice
     * @param float|null $quantity
     * @throws LimitOrderPriceRequiredException
     */
    public function __construct(
        #[ExpectedValues(TypeEnum::TYPES)] protected int $type,
        #[ExpectedValues(OrderTypeEnum::TYPES)] protected int $orderType,
        protected ?float $price = null,
        protected ?float $marketPrice = null,
        protected ?float $quantity = null,
    )
    {
        if ($this->orderType !== OrderTypeEnum::TYPE_MARKET &&
            $price === null
        ) {
            throw new LimitOrderPriceRequiredException();
        }
    }
}
