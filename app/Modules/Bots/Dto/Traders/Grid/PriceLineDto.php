<?php
declare(strict_types=1);

namespace App\Modules\Bots\Dto\Traders\Grid;

use App\Modules\Bots\Enum\Trades\TypeEnum;
use App\Modules\Bots\Exceptions\Dto\PriceLineDto\TypeIncorrectException;
use App\Modules\Bots\Contracts\BotsModuleContract;
use App\Modules\Core\Dto\BaseDto;
use JetBrains\PhpStorm\ExpectedValues;

/**
 * PriceLineDto.
 *
 * @property-read float $price
 * @property-read int $type
 */
class PriceLineDto extends BaseDto implements BotsModuleContract
{
    /**
     * Constructor.
     *
     * @param float $price
     * @param int $type
     * @throws TypeIncorrectException
     */
    public function __construct(
        protected float $price,
        #[ExpectedValues(TypeEnum::TYPES)] protected int $type,
    )
    {
        $this->checkType($type);
    }

    /**
     * Check type.
     *
     * @param int $type
     * @throws TypeIncorrectException
     */
    private function checkType(int $type)
    {
        if(!in_array($type, [TypeEnum::TYPE_BUY, TypeEnum::TYPE_SELL])) {
            throw new TypeIncorrectException();
        }
    }
}
