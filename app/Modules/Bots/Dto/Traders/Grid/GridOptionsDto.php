<?php
declare(strict_types=1);

namespace App\Modules\Bots\Dto\Traders\Grid;

use App\Modules\Bots\Contracts\BotsModuleContract;
use App\Modules\Core\Dto\BaseDto;
use App\Modules\Deals\Dto\Api\DealDto;
use App\Modules\Market\Dto\Api\Pair\PairDto;

/**
 * Grid options dto.
 *
 * @property-read float $gridLowerLimit
 * @property-read float $gridUpperLimit
 * @property float $marketPrice
 * @property-read float|null $initialMargin
 * @property-read float|null $orderAmount
 * @property-read int $grids
 * @property-read float $tickSize
 * @property-read int $type
 * @property-read PairDto $pair
 * @property DealDto|null $lastFilledDeal
 */
final class GridOptionsDto extends BaseDto implements BotsModuleContract
{
    /** @var array|string[] Excluded read-only. */
    protected array $excludedReadOnly = ['marketPrice', 'lastFilledDeal'];

    /**
     * Constructor.
     *
     * @param float $gridLowerLimit
     * @param float $gridUpperLimit
     * @param float $marketPrice
     * @param float|null $initialMargin
     * @param float|null $orderAmount
     * @param int $grids
     * @param float $tickSize
     * @param int $type
     * @param PairDto $pair
     * @param DealDto|null $lastFilledDeal
     */
    public function __construct(
        protected float $gridLowerLimit,
        protected float $gridUpperLimit,
        protected float $marketPrice,
        protected ?float $initialMargin,
        protected ?float $orderAmount,
        protected int $grids,
        protected float $tickSize,
        protected int $type,
        protected PairDto $pair,
        protected ?DealDto $lastFilledDeal = null,
    )
    {
    }
}
