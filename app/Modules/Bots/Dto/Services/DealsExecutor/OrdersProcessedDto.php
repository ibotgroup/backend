<?php
declare(strict_types=1);

namespace App\Modules\Bots\Dto\Services\DealsExecutor;

use App\Modules\Core\Dto\BaseDto;
use App\Modules\Deals\Contracts\DealsModuleContract;
use Illuminate\Support\Collection;

/**
 * Orders processed dto.
 *
 * @property-read Collection|OrderProcessedDto[] $list
 */
final class OrdersProcessedDto extends BaseDto implements DealsModuleContract
{
    /**
     * Constructor.
     *
     * @param Collection $list
     */
    public function __construct(
        protected Collection $list,
    )
    {
    }

    /**
     * Count errors.
     *
     * @return int
     */
    public function countErrors(): int
    {
        return $this->list
            ->filter(fn(OrderProcessedDto $dto) => $dto->catch)
            ->count();
    }

    /**
     * Count errors.
     *
     * @return int
     */
    public function countSuccess(): int
    {
        return $this->list
            ->filter(fn(OrderProcessedDto $dto) => !$dto->catch)
            ->count();
    }

    /**
     * Get set orders.
     *
     * @return OrdersProcessedDto
     */
    public function set(): self
    {
        return new self(
            $this->list
                ->filter(fn(OrderProcessedDto $dto) => $dto->set)
        );
    }

    /**
     * Get filled orders.
     *
     * @return OrdersProcessedDto
     */
    public function triggered(): self
    {
        return new self(
            $this->list
                ->filter(fn(OrderProcessedDto $dto) => $dto->triggered)
        );
    }

    /**
     * Get cancelled orders.
     *
     * @return OrdersProcessedDto
     */
    public function cancelled(): self
    {
        return new self(
            $this->list
                ->filter(fn(OrderProcessedDto $dto) => $dto->cancelled)
        );
    }

    /**
     * Get cancel (manually) orders.
     *
     * @return OrdersProcessedDto
     */
    public function cancel(): self
    {
        return new self(
            $this->list
                ->filter(fn(OrderProcessedDto $dto) => $dto->cancel)
        );
    }

    /**
     * Get failed orders.
     *
     * @return OrdersProcessedDto
     */
    public function failed(): self
    {
        return new self(
            $this->list
                ->filter(fn(OrderProcessedDto $dto) => $dto->failed)
        );
    }
}
