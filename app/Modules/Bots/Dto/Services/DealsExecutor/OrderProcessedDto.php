<?php
declare(strict_types=1);

namespace App\Modules\Bots\Dto\Services\DealsExecutor;

use App\Modules\Core\Dto\BaseDto;
use App\Modules\Deals\Contracts\DealsModuleContract;
use App\Modules\Deals\Models\Deal;

/**
 * Order processed dto.
 *
 * @property-read Deal $deal
 * @property-read bool $set
 * @property-read bool $triggered
 * @property-read bool $cancelled
 * @property-read bool $cancel
 * @property-read bool $failed
 * @property-read bool $catch
 */
final class OrderProcessedDto extends BaseDto implements DealsModuleContract
{
    /**
     * Constructor.
     *
     * @param Deal $deal
     * @param bool $set
     * @param bool $triggered
     * @param bool $cancelled
     * @param bool $cancel
     * @param bool $failed
     * @param bool $catch
     */
    public function __construct(
        protected Deal $deal,
        protected bool $set = false,
        protected bool $triggered = false,
        protected bool $cancelled = false,
        protected bool $cancel = false,
        protected bool $failed = false,
        protected bool $catch = false
    )
    {
    }
}
