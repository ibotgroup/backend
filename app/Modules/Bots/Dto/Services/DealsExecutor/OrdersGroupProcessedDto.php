<?php
declare(strict_types=1);

namespace App\Modules\Bots\Dto\Services\DealsExecutor;

use App\Modules\Core\Dto\BaseDto;
use App\Modules\Deals\Contracts\DealsModuleContract;
use Illuminate\Support\Collection;

/**
 * Orders group processed dto.
 *
 * @property-read Collection|OrdersProcessedDto $list
 */
final class OrdersGroupProcessedDto extends BaseDto implements DealsModuleContract
{
    /**
     * Constructor.
     *
     * @param Collection $list
     */
    public function __construct(
        protected Collection $list,
    )
    {
    }

    /**
     * Has errors.
     *
     * @return bool
     */
    public function isSuccess(): bool
    {
        return $this->countErrors() === 0;
    }

    /**
     * Has errors.
     *
     * @return bool
     */
    public function hasErrors(): bool
    {
        return $this->countErrors() > 0;
    }

    /**
     * Count errors.
     *
     * @return int
     */
    public function countErrors(): int
    {
        $count = 0;

        $this->list
            ->map(function (OrdersProcessedDto $dto) use(&$count) {
                $count += $dto->countErrors();
            });

        return $count;
    }

    /**
     * Count success.
     *
     * @return int
     */
    public function countSuccess(): int
    {
        $count = 0;

        $this->list
            ->map(function (OrdersProcessedDto $dto) use(&$count) {
                $count += $dto->countSuccess();
            });

        return $count;
    }

    /**
     * Count set.
     *
     * @return int
     */
    public function countSet(): int
    {
        $count = 0;

        $this->list
            ->map(function (OrdersProcessedDto $dto) use(&$count) {
                $count += $dto->set()->list->count();
            });

        return $count;
    }

    /**
     * Count triggered.
     *
     * @return int
     */
    public function countTriggered(): int
    {
        $count = 0;

        $this->list
            ->map(function (OrdersProcessedDto $dto) use(&$count) {
                $count += $dto->triggered()->list->count();
            });

        return $count;
    }

    /**
     * Count cancelled.
     *
     * @return int
     */
    public function countCancelled(): int
    {
        $count = 0;

        $this->list
            ->map(function (OrdersProcessedDto $dto) use(&$count) {
                $count += $dto->cancelled()->list->count();
            });

        return $count;
    }

    /**
     * Count cancel (manually).
     *
     * @return int
     */
    public function countCancel(): int
    {
        $count = 0;

        $this->list
            ->map(function (OrdersProcessedDto $dto) use(&$count) {
                $count += $dto->cancel()->list->count();
            });

        return $count;
    }

    /**
     * Count cancelled.
     *
     * @return int
     */
    public function countFailed(): int
    {
        $count = 0;

        $this->list
            ->map(function (OrdersProcessedDto $dto) use(&$count) {
                $count += $dto->failed()->list->count();
            });

        return $count;
    }
}
