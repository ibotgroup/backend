<?php
declare(strict_types=1);

namespace App\Modules\Bots\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Bots\Requests\DeleteRequest;
use App\Modules\Bots\Models\Bot;
use App\Modules\Bots\Requests\ListRequest;
use App\Modules\Bots\Contracts\BotsModuleContract;
use App\Modules\Bots\Requests\AddOptionsRequest;
use App\Modules\Bots\Requests\AddRequest;
use App\Modules\Bots\Services\BotsService;
use App\Modules\Core\Attributes\Controllers\CatchResponse;
use App\Modules\Core\Attributes\Controllers\Transactional;
use App\Modules\Core\Dto\ResponseDto;
use App\Modules\Core\Exceptions\Traits\Lock\TransactionNotStartedException;

/**
 * Bots controller.
 */
final class BotsController extends Controller implements BotsModuleContract
{
    /**
     * List access.
     *
     * @param ListRequest $request
     * @return ResponseDto
     */
    #[CatchResponse]
    public function list(ListRequest $request): ResponseDto
    {
        return new ResponseDto(
            $this->getBotsService()
                ->list(
                    ...$request->validated()
                )
        );
    }

    /**
     * Add new bot.
     *
     * @param AddRequest $request
     * @return ResponseDto
     */
    #[Transactional]
    #[CatchResponse]
    public function add(AddRequest $request): ResponseDto
    {
        $this->getBotsService()
            ->add(
                ...$request->validated()
            );

        return new ResponseDto();
    }

    /**
     * Get add options.
     *
     * @param AddOptionsRequest $request
     * @return ResponseDto
     */
    #[CatchResponse]
    public function addOptions(AddOptionsRequest $request): ResponseDto
    {
        return new ResponseDto(
            $this->getBotsService()
                ->getAddOptions(
                    ...$request->validated()
                )
        );
    }

    /**
     * Delete bot.
     *
     * @param Bot $bot
     * @return ResponseDto
     * @throws TransactionNotStartedException
     */
    #[Transactional]
    #[CatchResponse]
    public function delete(Bot $bot): ResponseDto
    {
        Bot::refreshAndLockForUpdate($bot);
        app(DeleteRequest::class);

        $this->getBotsService()
            ->delete($bot);

        return new ResponseDto();
    }

    /**
     * See {@see BotsService::class}.
     *
     * @return BotsService
     */
    private function getBotsService(): BotsService
    {
        return app(BotsService::class);
    }
}
