<?php
declare(strict_types=1);

namespace App\Modules\Bots\Repositories;

use App\Modules\Bots\Contracts\BotsModuleContract;
use App\Modules\Bots\Models\Bot;
use App\Modules\Bots\Models\BotIteration;
use App\Modules\Core\Contracts\Repositories\RepositoryContract;
use App\Modules\Core\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

/**
 * Bots repository.
 */
final class BotsRepository extends BaseRepository implements BotsModuleContract, RepositoryContract
{
    /**
     * Constructor.
     *
     * @param Bot $model
     */
    public function __construct(protected Bot $model)
    {
    }

    /**
     * Get list bots.
     *
     * @param int $userId
     * @return Collection
     */
    public function getList(
        int $userId,
    ): Collection
    {
        return $this->model
            ->newQuery()
            ->where('users_id', $userId)
            ->orderBy('created_at')
            ->get();
    }

    /**
     * Get active bots.
     *
     * @return Collection
     */
    public function getActiveBots(): Collection
    {
        return $this->model
            ->newQuery()
            ->where('status', Bot::STATUS_ACTIVE)
            ->orderBy('created_at')
            ->get();
    }

    /**
     * Get active bots.
     *
     * @return Collection
     */
    public function getReadyBots(): Collection
    {
        return $this->model
            ->newQuery()
            ->where('status', Bot::STATUS_ACTIVE)
            ->where(static function(Builder $builder) {
                $builder->doesntHave('botIteration')
                    ->orWhereHas('botIteration', static function(Builder $builder) {
                        $builder->where('status', BotIteration::STATUS_PROCESSED);
                    });
            })
            ->orderBy('created_at')
            ->get();
    }

    /**
     * Get max possible orders.
     *
     * @param int $userId
     * @return int
     */
    public function getMaxPossibleOrders(int $userId): int
    {
        return (int)$this->model
            ->newQuery()
            ->select('bots.*')
            ->where('bots.users_id', $userId)
            ->leftJoin('algorithms_grid as ag', 'bots.algorithms_grid_id', '=', 'ag.id')
            ->whereIn('bots.status', [Bot::STATUS_INACTIVE, Bot::STATUS_ACTIVE])
            ->orderBy('bots.created_at')
            ->sum('ag.grids');
    }
}
