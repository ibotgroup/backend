<?php
declare(strict_types=1);

namespace App\Modules\Bots\Models\Algorithms;

use App\Modules\Bots\Contracts\Models\Algorithms\AlgorithmContract;
use App\Modules\Core\Contracts\Models\Api;
use App\Modules\Core\Exceptions\Models\ModelNotExistsException;
use App\Modules\Core\Models\Model;
use App\Modules\Bots\Dto\Api\Algorithms\Algorithms\AlgorithmGridDto;
use Carbon\Carbon;

/**
 * Algorithm model.
 *
 * @property-read int $id
 * @property int $type
 * @property float $lower_limit
 * @property float $upper_limit
 * @property int $grids
 * @property float $initial_margin
 * @property float|null $amount_per_order
 * @property boolean $stop_at_close
 * @property float $activation_trigger
 * @property float $low_stop_trigger
 * @property float $high_stop_trigger
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 */
final class AlgorithmGrid extends Model implements Api, AlgorithmContract
{
    const TYPE_ARITHMETIC = 1;
    const TYPE_GEOMETRIC = 2;

    const TYPES = [
        self::TYPE_ARITHMETIC,
        self::TYPE_GEOMETRIC,
    ];

    const TYPES_TEXT = [
        self::TYPE_ARITHMETIC => 'Arithmetic',
        self::TYPE_GEOMETRIC => 'Geometric',
    ];

    /** {@inheritdoc} */
    protected $table = 'algorithms_grid';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'type' => 'integer',
        'lower_limit' => 'float',
        'upper_limit' => 'float',
        'grids' => 'integer',
        'initial_margin' => 'float',
        'amount_per_order' => 'float',
        'stop_at_close' => 'boolean',
        'activation_trigger' => 'float',
        'low_stop_trigger' => 'float',
        'high_stop_trigger' => 'float',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    /**
     * To api.
     *
     * @return AlgorithmGridDto
     * @throws ModelNotExistsException
     */
    public function toApi(): AlgorithmGridDto
    {
        $this->throwIfNotExists();

        return new AlgorithmGridDto(
            $this->type,
            self::TYPES_TEXT[$this->type] ?? (string) $this->type,
            $this->lower_limit,
            $this->upper_limit,
            $this->grids,
            $this->initial_margin,
            $this->amount_per_order,
            $this->stop_at_close,
            $this->activation_trigger,
            $this->low_stop_trigger,
            $this->high_stop_trigger,
        );
    }
}
