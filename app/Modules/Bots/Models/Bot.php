<?php
declare(strict_types=1);

namespace App\Modules\Bots\Models;

use App\Modules\Bots\Dto\Api\Bots\BotDto;
use App\Modules\Core\Contracts\Models\Api;
use App\Modules\Core\Exceptions\Models\ModelNotExistsException;
use App\Modules\Core\Services\NetworkService;
use App\Modules\Deals\Models\Deal;
use App\Modules\Deals\Models\DealError;
use App\Modules\Market\Models\Pair;
use App\Modules\Users\Models\User;
use App\Modules\Bots\Models\Algorithms\AlgorithmGrid;
use App\Modules\Bots\Contracts\Models\Algorithms\AlgorithmContract;
use App\Modules\Accesses\Models\Access;
use App\Modules\Bots\Contracts\BotsModuleContract;
use App\Modules\Core\Models\Model;
use Carbon\Carbon;
use Illuminate\Contracts\Cache\LockTimeoutException;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;
use Throwable;

/**
 * Bot model.
 *
 * @property-read int $id
 * @property-read int $sid
 * @property-read int $bots_iteration_id
 * @property-read int $users_id
 * @property-read int $access_id
 * @property-read int $algorithms_grid_id
 * @property-read int $pairs_id
 * @property int $status
 * @property-read int $deals_errors_id
 * @property int $stopped_reason
 * @property boolean $is_locked
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read BotIteration|null $botIteration
 * @property-read User $user
 * @property-read Access $access
 * @property-read AlgorithmContract|AlgorithmGrid|null $algorithm
 * @property-read Pair $pair
 * @property-read Collection|Deal[] $deals
 * @property-read Collection|Deal[] $dealsPendingOrSet
 * @property-read Collection|Deal[] $dealsSet
 * @property-read Collection|Deal[] $dealsTriggered
 * @property-read DealError|null $dealError
 * @property-read string $statusText
 * @property-read boolean $isActive
 * @property-read boolean $isStopped
 * @property-read string $stoppedReasonText
 * @property-read boolean $stopPriceReached
 * @property-read boolean $stopPriceHighReached
 * @property-read boolean $stopPriceLowReached
 * @property-read int|null $stoppedReasonByDeals
 */
final class Bot extends Model implements BotsModuleContract, Api
{
    const STATUS_INACTIVE = 1;
    const STATUS_ACTIVE = 2;
    const STATUS_DELETED = 3;
    const STATUS_STOPPED = 4;

    const STOPPED_REASON_HIGHEST_REACHED = 1;
    const STOPPED_REASON_LOWEST_REACHED = 2;
    const STOPPED_REASON_UNKNOWN_ERROR = 3;

    const STOPPED_REASON_LIST_TEXT = [
        self::STOPPED_REASON_HIGHEST_REACHED => 'The price has reached the top level.',
        self::STOPPED_REASON_LOWEST_REACHED => 'The price has reached the bottom level.',
        self::STOPPED_REASON_UNKNOWN_ERROR => 'Unknown error.',
    ];

    const STATUSES_TEXT = [
        self::STATUS_INACTIVE => 'Inactive',
        self::STATUS_ACTIVE => 'Active',
        self::STATUS_DELETED => 'Deleted',
        self::STATUS_STOPPED => 'Stopped',
    ];

    const PAIR_BTC_USDT = 'BTCUSDT';
    const PAIR_ETH_USDT = 'ETHUSDT';

    const PAIRS = [
        self::PAIR_BTC_USDT,
        self::PAIR_ETH_USDT,
    ];

    /** {@inheritdoc} */
    protected $table = 'bots';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'sid' => 'integer',
        'bots_iteration_id' => 'integer',
        'users_id' => 'integer',
        'settings_api_id' => 'integer',
        'algorithms_grid_id' => 'integer',
        'pairs_id' => 'integer',
        'status' => 'integer',
        'deals_errors_id' => 'integer',
        'stopped_reason' => 'integer',
        'is_locked' => 'boolean',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    /**
     * {@see BotIteration::class}.
     *
     * @return BelongsTo
     */
    public function botIteration(): BelongsTo
    {
        return $this->belongsTo(BotIteration::class, 'bots_iteration_id');
    }

    /**
     * {@see User::class}.
     *
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'users_id');
    }

    /**
     * {@see Access::class}.
     *
     * @return BelongsTo
     */
    public function access(): BelongsTo
    {
        return $this->belongsTo(Access::class, 'accesses_id');
    }

    /**
     * {@see AlgorithmContract::class}.
     *
     * @return BelongsTo
     */
    public function algorithm(): BelongsTo
    {
        return $this->belongsTo(AlgorithmGrid::class, 'algorithms_grid_id');
    }

    /**
     * {@see Pair::class}.
     *
     * @return BelongsTo
     */
    public function pair(): BelongsTo
    {
        return $this->belongsTo(Pair::class, 'pairs_id');
    }

    /**
     * {@see Deal::class}.
     *
     * @return HasMany
     */
    public function deals(): HasMany
    {
        return $this->hasMany(Deal::class, 'bots_id');
    }

    /**
     * {@see Deal::class}.
     *
     * @return HasMany
     */
    public function dealsPendingOrSet(): HasMany
    {
        return $this->hasMany(Deal::class, 'bots_id')
            ->whereIn('status', [Deal::STATUS_PENDING, Deal::STATUS_SET])
            ->orderBy('status', 'ASC');
    }

    /**
     * {@see Deal::class}.
     *
     * @return HasMany
     */
    public function dealsSet(): HasMany
    {
        return $this->hasMany(Deal::class, 'bots_id')
            ->where('status', Deal::STATUS_SET);
    }

    /**
     * {@see Deal::class}.
     *
     * @return HasMany
     */
    public function dealsTriggered(): HasMany
    {
        return $this->hasMany(Deal::class, 'bots_id')
            ->where('status', Deal::STATUS_TRIGGERED)
            ->orderBy('triggered_at', 'DESC');
    }

    /**
     * {@see DealError::class}.
     *
     * @return BelongsTo
     */
    public function dealError(): BelongsTo
    {
        return $this->belongsTo(DealError::class, 'deals_errors_id');
    }

    /**
     * Get attribute "statusText".
     *
     * @return string
     */
    public function getStatusTextAttribute(): string
    {
        return ((string)self::STATUSES_TEXT[$this->status] ?? (string)$this->status);
    }

    /**
     * Get attribute "stoppedReasonText".
     *
     * @return string
     */
    public function getStoppedReasonTextAttribute(): string
    {
        if ($this->stopped_reason) {
            return ((string)(self::STOPPED_REASON_LIST_TEXT[$this->stopped_reason] ?? $this->stopped_reason));
        }

        return $this->dealError ? $this->dealError->message : "";
    }

    /**
     * Get attribute "isActive".
     *
     * @return bool
     */
    public function getIsActiveAttribute(): bool
    {
        return $this->status === self::STATUS_ACTIVE;
    }

    /**
     * Get attribute "isStopped".
     *
     * @return bool
     */
    public function getIsStoppedAttribute(): bool
    {
        return $this->status === self::STATUS_STOPPED;
    }

    /**
     * Get attribute "stopPriceReached".
     *
     * @return bool
     */
    public function getStopPriceReachedAttribute(): bool
    {
        return $this->stopPriceHighReached || $this->stopPriceLowReached;
    }

    /**
     * Get attribute "stopPriceHighReached".
     *
     * @return bool
     */
    public function getStopPriceHighReachedAttribute(): bool
    {
        /** @var Deal|null $lowest */
        $highest = $this->deals
            ->sortByDesc('price')
            ->where('status', Deal::STATUS_TRIGGERED)
            ->first();

        return $highest && $highest->price >= $highest->bot->algorithm->upper_limit;
    }

    /**
     * Get attribute "stopPriceLowReached".
     *
     * @return bool
     */
    public function getStopPriceLowReachedAttribute(): bool
    {
        /** @var Deal|null $lowest */
        $lowest = $this->deals
            ->sortBy('price')
            ->where('status', Deal::STATUS_TRIGGERED)
            ->first();

        return $lowest && $lowest->price <= $lowest->bot->algorithm->lower_limit;
    }

    /**
     * Get attribute "stoppedReasonByDeals".
     *
     * @return int|null
     */
    public function getStoppedReasonByDealsAttribute(): ?int
    {
        if ($this->stopPriceHighReached) {
            return self::STOPPED_REASON_HIGHEST_REACHED;
        } else if ($this->stopPriceLowReached) {
            return self::STOPPED_REASON_LOWEST_REACHED;
        }

        return null;
    }

    /**
     * To api dto.
     *
     * @return BotDto
     * @throws ModelNotExistsException
     */
    public function toApi(): BotDto
    {
        $this->throwIfNotExists();

        return new BotDto(
            (string)$this->sid,
            $this->access->toApi(),
            $this->algorithm->toApi(),
            $this->pair->toApi(),
            $this->status,
            $this->statusText,
            $this->stoppedReasonText,
            $this->deals()->count(),
            $this->created_at?->format('Y-m-d H:i:s')
        );
    }

    /**
     * Try to dto api.
     *
     * @return BotDto|null
     */
    public function tryToApi(): ?BotDto
    {
        try {
            return $this->toApi();
        } catch (Throwable) {
        }

        return null;
    }

    /**
     * Is owner.
     *
     * @param User $user
     * @return bool
     */
    public function isOwner(User $user): bool
    {
        return $this->users_id === $user->id;
    }

    /**
     * Lock await.
     *
     * @param callable $callable
     * @throws LockTimeoutException
     */
    public function lockAwaitOptimalTiming(callable $callable): void
    {
        parent::lockAwait(
            $this->deals()
                ->count() * NetworkService::TOTAL_TIMEOUT,
            $callable
        );
    }

    /**
     * Set some events.
     *
     * @return void
     */
    protected static function booted(): void
    {
        static::creating(function (self $model) {
            $model->sid = rndSid();
        });
    }
}
