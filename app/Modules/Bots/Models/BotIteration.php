<?php
declare(strict_types=1);

namespace App\Modules\Bots\Models;

use App\Modules\Bots\Contracts\BotsModuleContract;
use App\Modules\Core\Models\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Bot iteration model.
 *
 * @property-read int $id
 * @property-read int $bots_id
 * @property int $status
 * @property Carbon|null $price_at
 * @property boolean $processed_after_monitoring
 * @property boolean $closing_deals
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Bot $bot
 * @property-read bool $isPending
 * @property-read bool $isProcessed
 * @property-read bool $isMonitoring
 * @property-read bool $isPricesUpdated
 */
final class BotIteration extends Model implements BotsModuleContract
{
    public const STATUS_PENDING = 1;
    public const STATUS_PROCESSED = 2;
    public const STATUS_MONITORING = 3;

    public const STATUSES = [];

    /** {@inheritdoc} */
    protected $table = 'bots_iteration';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'bots_id' => 'integer',
        'status' => 'integer',
        'price_at' => 'datetime',
        'processed_after_monitoring' => 'boolean',
        'closing_deals' => 'boolean',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    /**
     * {@see Bot::class}.
     *
     * @return BelongsTo
     */
    public function bot(): BelongsTo
    {
        return $this->belongsTo(Bot::class, 'bots_id');
    }

    /**
     * Get attribute "isPending".
     *
     * @return bool
     */
    public function getIsPendingAttribute(): bool
    {
        return $this->status === self::STATUS_PENDING;
    }

    /**
     * Get attribute "isProcessed".
     *
     * @return bool
     */
    public function getIsProcessedAttribute(): bool
    {
        return $this->status === self::STATUS_PROCESSED;
    }

    /**
     * Get attribute "isMonitoring".
     *
     * @return bool
     */
    public function getIsMonitoringAttribute(): bool
    {
        return $this->status === self::STATUS_MONITORING;
    }

    /**
     * Get attribute "isPricesUpdated".
     *
     * @return bool
     */
    public function getIsPricesUpdatedAttribute(): bool
    {
        return $this->bot->pair->last_price_at > $this->price_at;
    }
}
