<?php
declare(strict_types=1);

namespace App\Modules\Bots\Services;

use App\Modules\Accesses\Models\Access;
use App\Modules\Accesses\Services\AccessesService;
use App\Modules\Bots\Contracts\BotsModuleContract;
use App\Modules\Bots\Dto\Api\Bots\AddOptionsDto;
use App\Modules\Bots\Models\Algorithms\AlgorithmGrid;
use App\Modules\Bots\Models\Bot;
use App\Modules\Bots\Repositories\BotsRepository;
use App\Modules\Core\Contracts\Services\ModelManagerContract;
use App\Modules\Core\Contracts\Services\ServiceContract;
use App\Modules\Core\Exceptions\Traits\Lock\TransactionNotStartedException;
use App\Modules\Core\Facades\DB;
use App\Modules\Deals\Models\DealError;
use App\Modules\Deals\Services\DealsService;
use App\Modules\Exchanges\Enum\ProviderEnum;
use App\Modules\Market\Models\Pair;
use App\Modules\Market\Services\MarketDataService;
use App\Modules\Users\Models\User;
use Illuminate\Support\Collection;
use Throwable;

/**
 * Bots service.
 */
final class BotsService implements ServiceContract, BotsModuleContract, ModelManagerContract
{
    /**
     * {@inheritDoc}
     */
    public function find(int $id): ?Bot
    {
        return $this->getBotsRepository()
            ->find($id);
    }

    /**
     * {@inheritDoc}
     */
    public function findBySid(string $sid): ?Bot
    {
        return $this->getBotsRepository()
            ->findBySid($sid);
    }

    /**
     * Get list.
     *
     * @param User $user
     * @return Collection
     */
    public function list(User $user): Collection
    {
        return $this->listModels($user)
            ->map(fn(Bot $bot) => $bot->toApi());
    }

    /**
     * Get models list.
     *
     * @param User $user
     * @return Collection
     */
    public function listModels(User $user): Collection
    {
        return $this->getBotsRepository()
            ->getList($user->id);
    }

    /**
     * Get ready bots for trading.
     *
     * @return Collection
     */
    public function getReadyBots(): Collection
    {
        return $this->getBotsRepository()
            ->getReadyBots();
    }

    /**
     * Add new bot.
     *
     * @param User $user
     * @param Access $access
     * @param Pair $pair
     * @param int $type
     * @param float $lowerLimit
     * @param float $upperLimit
     * @param int $grids
     * @param float $orderAmount
     * @param float|null $activationTrigger
     * @param float|null $lowStopTrigger
     * @param float|null $highStopTrigger
     * @return Bot
     */
    public function add(
        User $user,
        Access $access,
        Pair $pair,
        int $type,
        float $lowerLimit,
        float $upperLimit,
        int $grids,
        float $orderAmount,
        bool $closeAtStop = false,
        ?float $activationTrigger = null,
        ?float $lowStopTrigger = null,
        ?float $highStopTrigger = null,
    ): Bot
    {
        $algorithmGrid = new AlgorithmGrid();
        $algorithmGrid->type = $type;
        $algorithmGrid->lower_limit = $lowerLimit;
        $algorithmGrid->upper_limit = $upperLimit;
        $algorithmGrid->grids = $grids;
        $algorithmGrid->amount_per_order = $orderAmount;
        $algorithmGrid->stop_at_close = $closeAtStop;
        $algorithmGrid->activation_trigger = $activationTrigger;
        $algorithmGrid->low_stop_trigger = $lowStopTrigger;
        $algorithmGrid->high_stop_trigger = $highStopTrigger;

        $algorithmGrid->save();

        $bot = new Bot();
        $bot->access()
            ->associate($access);
        $bot->pair()
            ->associate($pair);
        $bot->status = $algorithmGrid->activation_trigger === null ? Bot::STATUS_ACTIVE : Bot::STATUS_INACTIVE;

        $bot->user()
            ->associate($user);
        $bot->algorithm()
            ->associate($algorithmGrid);
        $bot->access()
            ->associate($access);
        $bot->save();

        return $bot;
    }

    /**
     * Get add options.
     *
     * @param User $user
     * @return AddOptionsDto
     */
    public function getAddOptions(User $user): AddOptionsDto
    {
        return new AddOptionsDto(
            $this->getAccessesService()
                ->listModels($user)
                ->map(fn(Access $access) => $access->toApiOption()),
            $this->getMarketDataService()
                ->getTradedPairs(ProviderEnum::BINANCE_SPOT)
                ->map(fn(Pair $pair) => $pair->toApiOption())
        );
    }

    /**
     * Delete bot with set status deleted.
     *
     * @param Bot $bot
     */
    public function delete(Bot $bot): void
    {
        $bot->status = Bot::STATUS_DELETED;

        $bot->save();
    }

    /**
     * Stop bot.
     *
     * @param Bot $bot
     */
    public function stop(Bot $bot): void
    {
        $bot->status = Bot::STATUS_STOPPED;

        $bot->save();
    }

    /**
     * Stop bot by error reason,
     *
     * @param Bot $bot
     * @param DealError $dealError
     * @throws Throwable
     * @throws TransactionNotStartedException
     */
    public function stopByErrorReason(Bot $bot, DealError $dealError): void
    {
        try {
            DB::beginTransaction();

            $bot->status = Bot::STATUS_STOPPED;
            $bot->dealError()
                ->associate($dealError);

            $bot->save();
        } catch (Throwable $e) {
            DB::rollBack();

            throw $e;
        }
    }

    /**
     * Cancel open orders.
     *
     * @param Bot $bot
     * @throws Throwable
     */
    public function stopAfterUnknownException(Bot $bot): void
    {
        try {
            DB::beginTransaction();
            $bot->refresh();

            $bot->status = Bot::STATUS_STOPPED;
            $bot->stopped_reason = Bot::STOPPED_REASON_UNKNOWN_ERROR;
            $bot->save();

            $this->getDealsService()
                ->cancelNewByBot($bot);

            DB::commit();
        }
        catch (Throwable $e) {
            DB::rollBack();

            throw $e;
        }
    }

    /**
     * Get max possible orders.
     *
     * @param User $user
     * @return int
     */
    public function getMaxPossibleOrders(User $user): int
    {
        return $this->getBotsRepository()
            ->getMaxPossibleOrders($user->id);
    }

    /**
     * Get {@see AccessesService::class}.
     *
     * @return AccessesService
     */
    private function getAccessesService(): AccessesService
    {
        return app(AccessesService::class);
    }

    /**
     * Get {@see MarketDataService::class}.
     *
     * @return MarketDataService
     */
    private function getMarketDataService(): MarketDataService
    {
        return app(MarketDataService::class);
    }

    /**
     * Get {@see BotsRepository::class}.
     *
     * @return BotsRepository
     */
    private function getBotsRepository(): BotsRepository
    {
        return app(BotsRepository::class);
    }

    /**
     * Get {@see DealsService::class}.
     *
     * @return DealsService
     */
    private function getDealsService(): DealsService
    {
        return app(DealsService::class);
    }
}
