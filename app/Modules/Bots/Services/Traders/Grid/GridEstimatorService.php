<?php
declare(strict_types=1);

namespace App\Modules\Bots\Services\Traders\Grid;

use App\Modules\Bots\Contracts\BotsModuleContract;
use App\Modules\Bots\Dto\Traders\Grid\GridOptionsDto;
use App\Modules\Bots\Dto\Traders\Grid\PriceLineDto;
use App\Modules\Bots\Enum\Traders\Grid\ModeTypeEnum;
use App\Modules\Bots\Exceptions\Traders\GridTrader\GridMustGreatThanZeroException;
use App\Modules\Bots\Exceptions\Traders\GridTrader\UnsupportedTypeException;
use App\Modules\Bots\Services\Traders\Grid\Modes\Arithmetic;
use App\Modules\Bots\Services\Traders\Grid\Modes\Geometric;
use App\Modules\Core\Contracts\Services\ServiceContract;
use Illuminate\Support\Collection;

/**
 * GridEstimatorService.
 */
final class GridEstimatorService implements ServiceContract, BotsModuleContract
{
    /**
     * Get price lines.
     *
     * @param GridOptionsDto $options
     * @return Collection
     * @throws GridMustGreatThanZeroException
     * @throws UnsupportedTypeException
     */
    public function getPriceLines(GridOptionsDto $options): Collection
    {
        if (!in_array($options->type, [ModeTypeEnum::TYPE_ARITHMETIC, ModeTypeEnum::TYPE_GEOMETRIC])) {
            throw new UnsupportedTypeException();
        }

        if ($options->grids <= 0) {
            throw new GridMustGreatThanZeroException();
        }

        return $this->setEmptyLastFilledDeal(
            $options->type === ModeTypeEnum::TYPE_ARITHMETIC ?
                (new Arithmetic($options))->getPriceLines() :
                (new Geometric($options))->getPriceLines(),
            $options
        );
    }

    /**
     * Set empty last filled deal.
     *
     * @param Collection $deals
     * @param GridOptionsDto $options
     * @return Collection
     * @throws GridMustGreatThanZeroException
     * @throws UnsupportedTypeException
     */
    private function setEmptyLastFilledDeal(Collection $deals, GridOptionsDto $options): Collection
    {
        if ($options->lastFilledDeal) {
            $deal = $options->lastFilledDeal;

            $optionsOld = clone $options;
            $optionsOld->marketPrice = $deal->marketPrice;
            $optionsOld->lastFilledDeal =  null;

            $oldLines = $this->getPriceLines($optionsOld);

            $oldLines->each(function(PriceLineDto $priceLine, int $index) use($deal, $deals) {
                if($priceLine->price === $deal->price) {
                    $deals->forget($index);
                }
            });
        }

        return $deals;
    }
}
