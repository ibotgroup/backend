<?php
declare(strict_types=1);

namespace App\Modules\Bots\Services\Traders\Grid;

use App\Modules\Bots\Contracts\BotsModuleContract;
use App\Modules\Bots\Dto\Traders\Deals\DealDto;
use App\Modules\Bots\Dto\Traders\Grid\GridOptionsDto;
use App\Modules\Bots\Dto\Traders\Grid\PriceLineDto;
use App\Modules\Bots\Enum\Trades\OrderTypeEnum;
use App\Modules\Bots\Exceptions\Dto\PriceLineDto\TypeIncorrectException;
use App\Modules\Bots\Exceptions\Traders\GridTrader\Conditions\ConditionsException;
use App\Modules\Bots\Exceptions\Traders\GridTrader\Conditions\CountOrdersLessThanThreeException;
use App\Modules\Bots\Exceptions\Traders\GridTrader\Conditions\AmountPerOrderLessThanMinimalException;
use App\Modules\Bots\Exceptions\Traders\GridTrader\Conditions\CurrentPriceOutOfGridException;
use App\Modules\Bots\Exceptions\Traders\GridTrader\Conditions\LowerLimitGreatThanUpperLimitException;
use App\Modules\Bots\Exceptions\Traders\GridTrader\Conditions\LowerLimitLessThanPriceMinException;
use App\Modules\Bots\Exceptions\Traders\GridTrader\Conditions\UpperLimitGreatThanPriceMaxException;
use App\Modules\Bots\Exceptions\Traders\GridTrader\GridMustGreatThanZeroException;
use App\Modules\Bots\Exceptions\Traders\GridTrader\GridTraderException;
use App\Modules\Bots\Exceptions\Traders\GridTrader\UnsupportedTypeException;
use App\Modules\Core\Contracts\Services\ServiceContract;
use Illuminate\Support\Collection;

/**
 * Grid service.
 */
final class GridService implements ServiceContract, BotsModuleContract
{
    /**
     * Grid trade.
     *
     * @param GridOptionsDto $options
     * @return Collection
     * @throws ConditionsException
     * @throws GridTraderException
     * @throws GridMustGreatThanZeroException
     * @throws LowerLimitGreatThanUpperLimitException
     * @throws LowerLimitLessThanPriceMinException
     * @throws TypeIncorrectException
     * @throws UnsupportedTypeException
     * @throws UpperLimitGreatThanPriceMaxException
     */
    public function gridTrade(GridOptionsDto $options): Collection
    {
        $this->checkPreConditions($options);

        $deals = $this->priceLinesToDeals(
            $options,
            $this->getGridEstimatorService()
                ->getPriceLines($options)
        );

        $this->checkPostConditions($options, $deals);

        return $deals;
    }

    /**
     * Check conditions.
     *
     * @param GridOptionsDto $options
     * @throws LowerLimitGreatThanUpperLimitException
     * @throws LowerLimitLessThanPriceMinException
     * @throws UpperLimitGreatThanPriceMaxException
     * @throws CurrentPriceOutOfGridException
     */
    public function checkPreConditions(GridOptionsDto $options): void
    {
        if ($options->gridUpperLimit <= $options->gridLowerLimit) {
            throw new LowerLimitGreatThanUpperLimitException();
        } else if ($options->gridLowerLimit < $options->pair->filterPriceMin) {
            throw new LowerLimitLessThanPriceMinException();
        } else if ($options->gridUpperLimit > $options->pair->filterPriceMax) {
            throw new UpperLimitGreatThanPriceMaxException();
        } else if ($options->gridLowerLimit >= $options->pair->lastPrice ||
            $options->gridUpperLimit <= $options->pair->lastPrice
        ) {
            throw new CurrentPriceOutOfGridException();
        }
    }

    /**
     * Check post conditions.
     *
     * @param GridOptionsDto $options
     * @param Collection $deals
     * @throws CountOrdersLessThanThreeException
     * @throws AmountPerOrderLessThanMinimalException
     */
    public function checkPostConditions(GridOptionsDto $options, Collection $deals): void
    {
        if ($deals->count() < 3) {
            throw new CountOrdersLessThanThreeException();
        }

        $deals->each(function (DealDto $deal) use ($options, $deals) {
            if ($deal->quantity < $options->pair->filterMinNotional) {
                throw new AmountPerOrderLessThanMinimalException(
                    minQuantity: $options->pair->filterMinNotional
                );
            }
        });
    }

    /**
     * Price lines to deals.
     *
     * @param GridOptionsDto $options
     * @param Collection $priceLines
     * @return Collection
     */
    private function priceLinesToDeals(
        GridOptionsDto $options,
        Collection $priceLines
    ): Collection
    {
        return $priceLines->map(function (PriceLineDto $priceLine) use ($options) {
            return new DealDto(
                $priceLine->type,
                OrderTypeEnum::TYPE_LIMIT,
                $priceLine->price,
                $options->marketPrice,
                $options->orderAmount
            );
        });
    }

    /**
     * Get {@see GridEstimatorService::class}.
     *
     * @return GridEstimatorService
     */
    private function getGridEstimatorService(): GridEstimatorService
    {
        return app(GridEstimatorService::class);
    }
}
