<?php
declare(strict_types=1);

namespace App\Modules\Bots\Services\Traders\Grid\Modes;

use App\Modules\Bots\Contracts\Services\Traders\Grid\ModeContract;
use App\Modules\Bots\Dto\Traders\Grid\GridOptionsDto;
use App\Modules\Bots\Dto\Traders\Grid\PriceLineDto;
use App\Modules\Bots\Enum\Trades\TypeEnum;
use App\Modules\Bots\Exceptions\Dto\PriceLineDto\TypeIncorrectException;
use Illuminate\Support\Collection;

/**
 * Geometric.
 */
final class Geometric implements ModeContract
{
    /**
     * {@inheritDoc}
     */
    public function __construct(private GridOptionsDto $options) {}

    /**
     * {@inheritDoc}
     */
    public function getPriceLines(): Collection
    {
        $priceRatio = $this->getPriceRatio(
            $this->options->gridLowerLimit,
            $this->options->gridUpperLimit,
            $this->options->grids
        );

        $listBuy = $this->getTypeBuyLines(
            $this->options->grids,
            $this->options->gridLowerLimit,
            $priceRatio,
            $this->options->marketPrice,
            $this->options->tickSize
        );

        $listSell = $this->getTypeSellLines(
            $this->options->grids,
            $listBuy->count(),
            $this->options->gridUpperLimit,
            $priceRatio,
            $this->options->marketPrice,
            $this->options->tickSize
        );

        return $listBuy->merge($listSell->reverse())
            ->reverse();
    }

    /**
     * Get price ratio.
     *
     * @param float $gridLowerLimit
     * @param float $gridUpperLimit
     * @param int $grids
     * @return float
     */
    public function getPriceRatio(
        float $gridLowerLimit,
        float $gridUpperLimit,
        int $grids
    ): float
    {
        return ($gridUpperLimit / $gridLowerLimit) ** (1 / $grids);
    }

    /**
     * Get price ratio.
     *
     * @param float $gridLowerLimit
     * @param float $gridUpperLimit
     * @param int $grids
     * @return float
     */
    public function getPriceDiffPercentage(
        float $gridLowerLimit,
        float $gridUpperLimit,
        int $grids
    ): float
    {
        return (($gridUpperLimit / $gridLowerLimit) ** (1 / $grids) - 1) * 100;
    }

    /**
     * Get lines buy type.
     *
     * @param int $grids
     * @param float $gridLowerLimit
     * @param float $priceRatio
     * @param float $currentPrice
     * @param float $tickSize
     * @return Collection
     * @throws TypeIncorrectException
     */
    private function getTypeBuyLines(
        int $grids,
        float $gridLowerLimit,
        float $priceRatio,
        float $currentPrice,
        float $tickSize
    ): Collection
    {
        $list = collect();

        for ($i = 1; $i <= $grids; $i++) {
            $priceLine = $gridLowerLimit;

            if ($i > 1) {
                $priceLine = $priceLine * $priceRatio ** ($i - 1);
            }

            $priceLine = priceFloor($priceLine, $tickSize);

            if ($priceLine >= $currentPrice) {
                break;
            }

            $list->add(new PriceLineDto($priceLine, TypeEnum::TYPE_BUY));
        }

        return $list;
    }

    /**
     * Get lines sell type.
     *
     * @param int $grids
     * @param int $buyGrids
     * @param float $gridUpperLimit
     * @param float $priceDiff
     * @param float $currentPrice
     * @param float $tickSize
     * @return Collection
     * @throws TypeIncorrectException
     */
    private function getTypeSellLines(
        int $grids,
        int $buyGrids,
        float $gridUpperLimit,
        float $priceDiff,
        float $currentPrice,
        float $tickSize
    ): Collection
    {
        $list = collect();

        for ($i = 1; $i <= $grids; $i++) {
            $priceLine = $gridUpperLimit;

            if ($i > 1) {
                $priceLine = $priceLine - ($priceDiff * ($i - 1));
            }

            $priceLine = priceFloor($priceLine, $tickSize);

            if ($priceLine <= $currentPrice || $list->count() + $buyGrids >= $grids) {
                break;
            }

            $list->add(new PriceLineDto($priceLine, TypeEnum::TYPE_SELL));
        }

        return $list;
    }
}
