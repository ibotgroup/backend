<?php
declare(strict_types=1);

namespace App\Modules\Bots\Services;

use App\Modules\Bots\Contracts\BotsModuleContract;
use App\Modules\Bots\Dto\Traders\Grid\GridOptionsDto;
use App\Modules\Bots\Exceptions\Dto\PriceLineDto\TypeIncorrectException;
use App\Modules\Bots\Exceptions\Services\Bots\BotAlreadyLockedException;
use App\Modules\Bots\Exceptions\Services\Bots\BotNotLockedException;
use App\Modules\Bots\Exceptions\Services\Trader\BotIterationIsNotPendingOrMonitoringException;
use App\Modules\Bots\Exceptions\Services\Trader\BotIterationIsPendingException;
use App\Modules\Bots\Exceptions\Services\Trader\PriceDoesNotUpdatedException;
use App\Modules\Bots\Exceptions\Services\Trader\StatusIsNotActiveException;
use App\Modules\Bots\Exceptions\Services\Trader\StoppedException;
use App\Modules\Bots\Exceptions\Traders\GridTrader\Conditions\ConditionsException;
use App\Modules\Bots\Exceptions\Traders\GridTrader\Conditions\LowerLimitGreatThanUpperLimitException;
use App\Modules\Bots\Exceptions\Traders\GridTrader\Conditions\LowerLimitLessThanPriceMinException;
use App\Modules\Bots\Exceptions\Traders\GridTrader\Conditions\UpperLimitGreatThanPriceMaxException;
use App\Modules\Bots\Exceptions\Traders\GridTrader\GridMustGreatThanZeroException;
use App\Modules\Bots\Exceptions\Traders\GridTrader\GridTraderException;
use App\Modules\Bots\Exceptions\Traders\GridTrader\UnsupportedTypeException;
use App\Modules\Bots\Models\Algorithms\AlgorithmGrid;
use App\Modules\Bots\Models\Bot;
use App\Modules\Bots\Models\BotIteration;
use App\Modules\Bots\Services\Traders\Grid\GridService;
use App\Modules\Core\Contracts\Services\ServiceContract;
use App\Modules\Core\Exceptions\Models\ModelNotExistsException;
use App\Modules\Core\Exceptions\Traits\Lock\TransactionNotStartedException;
use App\Modules\Core\Facades\DB;
use App\Modules\Deals\Services\DealsService;
use Illuminate\Contracts\Cache\LockTimeoutException;
use JetBrains\PhpStorm\ExpectedValues;
use Throwable;

/**
 * TraderService.
 */
final class TraderService implements ServiceContract, BotsModuleContract
{
    /**
     * Trades updater by active bots.
     * This method execution updating process data
     * for all active bots with all algorithms.
     *
     * @return int
     * @throws BotIterationIsPendingException
     * @throws ConditionsException
     * @throws GridMustGreatThanZeroException
     * @throws GridTraderException
     * @throws LowerLimitGreatThanUpperLimitException
     * @throws LowerLimitLessThanPriceMinException
     * @throws ModelNotExistsException
     * @throws StatusIsNotActiveException
     * @throws StoppedException
     * @throws Throwable
     * @throws TransactionNotStartedException
     * @throws TypeIncorrectException
     * @throws UnsupportedTypeException
     * @throws UpperLimitGreatThanPriceMaxException
     */
    public function tradesUpdater(): int
    {
        return $this->getBotsService()
            ->getReadyBots()
            ->each(fn(Bot $bot) => $this->tryTrade($bot))
            ->count();
    }

    /**
     * Trade by bot.
     *
     * @param Bot $bot
     * @throws BotIterationIsPendingException
     * @throws ConditionsException
     * @throws GridMustGreatThanZeroException
     * @throws GridTraderException
     * @throws LowerLimitGreatThanUpperLimitException
     * @throws LowerLimitLessThanPriceMinException
     * @throws ModelNotExistsException
     * @throws StatusIsNotActiveException
     * @throws StoppedException
     * @throws Throwable
     * @throws TransactionNotStartedException
     * @throws TypeIncorrectException
     * @throws UnsupportedTypeException
     * @throws UpperLimitGreatThanPriceMaxException
     */
    public function tryTrade(Bot $bot): void
    {
        try {
            $this->trade($bot);
        } catch (PriceDoesNotUpdatedException | BotAlreadyLockedException | BotNotLockedException | LockTimeoutException) {
        }
    }

    /**
     * Trade by bot.
     *
     * @param Bot $bot
     * @throws BotIterationIsPendingException
     * @throws GridMustGreatThanZeroException
     * @throws PriceDoesNotUpdatedException
     * @throws StatusIsNotActiveException
     * @throws StoppedException
     * @throws Throwable
     * @throws TransactionNotStartedException
     * @throws TypeIncorrectException
     * @throws UnsupportedTypeException
     * @throws ConditionsException
     * @throws LowerLimitGreatThanUpperLimitException
     * @throws LowerLimitLessThanPriceMinException
     * @throws UpperLimitGreatThanPriceMaxException
     * @throws GridTraderException
     * @throws ModelNotExistsException
     */
    public function trade(Bot $bot): void
    {
        $bot->lockAwaitOptimalTiming(function () use ($bot) {
            try {
                DB::beginTransaction();
                Bot::refreshAndLockForUpdate($bot);

                $this->checkRules($bot);
                $this->checkStop($bot);

                try {
                    $algorithm = $bot->algorithm;
                    $deals = $this->getGridTrader()
                        ->gridTrade(
                            new GridOptionsDto(
                                $algorithm->lower_limit,
                                $algorithm->upper_limit,
                                $bot->pair->last_price,
                                $algorithm->initial_margin,
                                $algorithm->amount_per_order,
                                $algorithm->grids+1,
                                $bot->pair->tickSizeAsFloat,
                                $algorithm->type,
                                $bot->pair->toApi(),
                                $bot->dealsTriggered
                                    ->sortByDesc('triggered_at')
                                    ->first()
                                    ?->toApi()
                            )
                        );

                    $changes = $this->getDealsService()
                        ->replaceDeals($bot, $deals);
                    $this->updateBotIteration($bot, $changes > 0  ? BotIteration::STATUS_PENDING : BotIteration::STATUS_MONITORING);

                    dumpDealDtoCollection($deals, $bot->pair);
                } catch (ConditionsException) {
                    $this->updateBotIteration($bot, BotIteration::STATUS_MONITORING);
                }

                DB::commit();
            } catch (StoppedException) {
                DB::commit();
            } catch (Throwable $e) {
                DB::rollBack();

                throw $e;
            }
        });
    }

    /**
     * Deals processed.
     *
     * @param Bot $bot
     * @param bool $iterationCompleted
     * @param bool $processedAfterMonitoring
     * @throws BotIterationIsNotPendingOrMonitoringException
     * @throws Throwable
     */
    public function dealsProcessed(Bot $bot, bool $iterationCompleted, bool $processedAfterMonitoring = false): void
    {
        try {
            DB::beginTransaction();
            $bot->refresh();

            if (!($bot->botIteration &&
                (
                    $bot->botIteration->isPending ||
                    $bot->botIteration->isMonitoring ||
                    $bot->botIteration->closing_deals
                ))
            ) {
                throw new BotIterationIsNotPendingOrMonitoringException();
            }

            if ($bot->botIteration
                    ->closing_deals && $bot->dealsSet()
                    ->count() === 0
            ) {
                $bot->botIteration->closing_deals = false;
                $bot->botIteration->status = BotIteration::STATUS_PROCESSED;
            } else {
                $bot->botIteration->status = $iterationCompleted ?
                    BotIteration::STATUS_PROCESSED :
                    BotIteration::STATUS_MONITORING;
                $bot->botIteration->processed_after_monitoring = $processedAfterMonitoring;
            }

            $bot->botIteration->save();

            DB::commit();
        } catch (Throwable $e) {
            DB::rollBack();

            throw $e;
        }
    }

    /**
     * Check stop.
     *
     * @param Bot $bot
     * @throws StoppedException
     */
    public function checkStop(Bot $bot): void
    {
        if ($bot->stopPriceReached) {
            $bot->refresh();

            $bot->status = Bot::STATUS_STOPPED;
            $bot->stopped_reason = $bot->stoppedReasonByDeals;
            $bot->save();

            if ($bot->algorithm instanceof AlgorithmGrid && $bot->algorithm->stop_at_close) {
                $this->getDealsService()
                    ->cancelNewByBot($bot);

                $bot->botIteration->status = BotIteration::STATUS_PROCESSED;
                $bot->botIteration->closing_deals = true;
                $bot->botIteration->save();
            }

            throw new StoppedException();
        }
    }

    /**
     * Check rules.
     *
     * @param Bot $bot
     * @throws BotIterationIsPendingException
     * @throws PriceDoesNotUpdatedException
     * @throws StatusIsNotActiveException
     */
    private function checkRules(Bot $bot): void
    {
        if (!$bot->isActive) {
            throw new StatusIsNotActiveException();
        } else if ($bot->botIteration) {
            if ($bot->botIteration && $bot->botIteration->isPending) {
                throw new BotIterationIsPendingException();
            } else if (!$bot->botIteration->isPricesUpdated) {
                throw new PriceDoesNotUpdatedException();
            }
        }
    }

    /**
     * Update bot iteration.
     *
     * @param Bot $bot
     * @param int $status
     */
    private function updateBotIteration(
        Bot $bot,
        #[ExpectedValues(BotIteration::STATUSES)] int $status
    ): void
    {
        $botIteration = new BotIteration();
        $botIteration->status = $status;
        $botIteration->price_at = $bot->pair
            ->last_price_at;

        $botIteration->bot()
            ->associate($bot);
        $botIteration->save();

        $bot->botIteration()
            ->associate($botIteration);
        $bot->save();
    }

    /**
     * Get {@see GridService::class}.
     *
     * @return GridService
     */
    private function getGridTrader(): GridService
    {
        return app(GridService::class);
    }

    /**
     * Get {@see DealsService::class}.
     *
     * @return DealsService
     */
    private function getDealsService(): DealsService
    {
        return app(DealsService::class);
    }

    /**
     * Get {@see BotsService::class}.
     *
     * @return BotsService
     */
    private function getBotsService(): BotsService
    {
        return app(BotsService::class);
    }
}
