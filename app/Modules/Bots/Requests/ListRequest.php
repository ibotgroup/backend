<?php
declare(strict_types=1);

namespace App\Modules\Bots\Requests;

use App\Modules\Accesses\Contracts\AccessesModuleContract;
use App\Modules\Users\Models\User;
use Illuminate\Foundation\Http\FormRequest;

/**
 * List request.
 */
final class ListRequest extends FormRequest implements AccessesModuleContract
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [];
    }

    /**
     * {@inheritDoc}
     */
    public function validated(): array
    {
        $data = parent::validated();
        $data['user'] = User::query()->find(1);

        return $data;
    }
}
