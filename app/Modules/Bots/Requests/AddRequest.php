<?php
declare(strict_types=1);

namespace App\Modules\Bots\Requests;

use App\Modules\Accesses\Contracts\AccessesModuleContract;
use App\Modules\Accesses\Services\AccessesService;
use App\Modules\Bots\Dto\Traders\Grid\GridOptionsDto;
use App\Modules\Bots\Exceptions\Dto\PriceLineDto\TypeIncorrectException;
use App\Modules\Bots\Exceptions\Traders\GridTrader\Conditions\ConditionsException;
use App\Modules\Bots\Exceptions\Traders\GridTrader\Conditions\AmountPerOrderLessThanMinimalException;
use App\Modules\Bots\Exceptions\Traders\GridTrader\Conditions\CurrentPriceOutOfGridException;
use App\Modules\Bots\Exceptions\Traders\GridTrader\Conditions\LowerLimitGreatThanUpperLimitException;
use App\Modules\Bots\Exceptions\Traders\GridTrader\Conditions\LowerLimitLessThanPriceMinException;
use App\Modules\Bots\Exceptions\Traders\GridTrader\Conditions\QuantityLessThanMinNotionalException;
use App\Modules\Bots\Exceptions\Traders\GridTrader\Conditions\UpperLimitGreatThanPriceMaxException;
use App\Modules\Bots\Exceptions\Traders\GridTrader\GridMustGreatThanZeroException;
use App\Modules\Bots\Exceptions\Traders\GridTrader\GridTraderException;
use App\Modules\Bots\Exceptions\Traders\GridTrader\UnsupportedTypeException;
use App\Modules\Bots\Models\Algorithms\AlgorithmGrid;
use App\Modules\Bots\Services\BotsService;
use App\Modules\Bots\Services\Traders\Grid\GridService;
use App\Modules\Core\Exceptions\Models\ModelNotExistsException;
use App\Modules\Core\Exceptions\Services\Network\RequestException;
use App\Modules\Exchanges\Dto\Services\Providers\BinanceProvider\AccountDto;
use App\Modules\Exchanges\Enum\ProviderEnum;
use App\Modules\Exchanges\Exceptions\Providers\BinanceProvider\BinanceProviderException;
use App\Modules\Exchanges\Services\BinanceService;
use App\Modules\Market\Models\Pair;
use App\Modules\Market\Services\MarketDataService;
use App\Modules\Users\Models\User;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Collection;
use Illuminate\Validation\Rule;
use JetBrains\PhpStorm\ArrayShape;
use function sprintf;

/**
 * Add access request.
 */
final class AddRequest extends FormRequest implements AccessesModuleContract
{
    /** @var AccountDto|null Binance account. */
    private ?AccountDto $account = null;

    /** @var Collection|null Deals dto lit. */
    private ?Collection $deals = null;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    #[ArrayShape([
        'accessesId' => "string",
        'pair' => "array",
        'type' => "array",
        'lowerLimit' => "string",
        'upperLimit' => "string",
        'grids' => "string",
        'orderAmount' => "string",
        'initialMargin' => "string",
        'closeAtStop' => "string",
        'activationTrigger' => "string",
        'lowStopTrigger' => "string",
        'highStopTrigger' => "string"
    ])]
    public function rules(): array
    {
        return [
            'accessesId' => 'required|numeric|exists:accesses,sid',
            'pair' => [
                'required',
                Rule::in(
                    $this->getMarketDataService()
                        ->getTradedPairs(ProviderEnum::BINANCE_SPOT)
                        ->map(fn(Pair $pair) => $pair->pair)
                )
            ],
            'type' => [
                'required',
                Rule::in(AlgorithmGrid::TYPES)
            ],
            'lowerLimit' => 'required|numeric',
            'upperLimit' => 'required|numeric',
            'grids' => 'required|integer|min:2',
            'orderAmount' => 'required|numeric|gt:0',
            'closeAtStop' => 'required|boolean',
            'activationTrigger' => 'nullable|numeric|gt:0',
            'lowStopTrigger' => 'nullable|numeric|gt:0',
            'highStopTrigger' => 'nullable|numeric|gt:0',
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function validated(): array
    {
        $data = parent::validated();
        $data['access'] = $this->getAccessesService()
            ->findBySid($this['accessesId']);
        $data['pair'] = $this->getMarketDataService()
            ->findByPair($this['pair'], ProviderEnum::BINANCE_SPOT);
        $data['user'] = User::query()->find(1);
        $data['type'] = (int)$data['type'];
        $data['lowerLimit'] = (float)$data['lowerLimit'];
        $data['upperLimit'] = (float)$data['upperLimit'];
        $data['grids'] = (int)$data['grids'];
        $data['orderAmount'] = (float)$data['orderAmount'];
        $data['activationTrigger'] = isset($data['activationTrigger']) ? (float)$data['activationTrigger'] : null;
        $data['lowStopTrigger'] = isset($data['lowStopTrigger']) ? (float)$data['lowStopTrigger'] : null;
        $data['highStopTrigger'] = isset($data['highStopTrigger']) ? (float)$data['highStopTrigger'] : null;

        unset($data['accessesId']);

        return $data;
    }

    /**
     * {@inheritDoc}
     */
    protected function getValidatorInstance(): Validator
    {
        return parent::getValidatorInstance()
            ->after(function (Validator $validator) {
                if (count($validator->failed()) === 0) {
                    $this->after($validator);
                }
            });
    }

    /**
     * Post validation method.
     *
     * @param Validator $validator
     * @return void
     * @throws ConditionsException
     * @throws GridMustGreatThanZeroException
     * @throws GridTraderException
     * @throws ModelNotExistsException
     * @throws RequestException
     * @throws TypeIncorrectException
     * @throws UnsupportedTypeException
     */
    private function after(Validator $validator): void
    {
        $access = $this->getAccessesService()
            ->findBySid($this['accessesId']);

        if (!$access->isOwner(User::query()->find(1))) {
            $validator->errors()->add('accessesId', 'This access can\'t select.');
        } else {
            $pair = $this->getMarketDataService()
                ->findByPair($this['pair'], ProviderEnum::BINANCE_SPOT);

            $this->getAndCheckAllowedSpotTrading($validator);
            $this->isGridAllowed($validator, $pair);
        }
    }

    /**
     * Get api permissions.
     *
     * @param Validator $validator
     * @return void
     * @throws RequestException
     */
    private function getAndCheckAllowedSpotTrading(Validator $validator): void
    {
        $access = $this->getAccessesService()
            ->findBySid($this['accessesId']);

        try {
            $this->account = $this->getBinanceService()
                ->getBinanceByAccess($access)
                ->account();

            if (!$this->account->isAllowedSpotTrading()) {
                $validator->errors()
                    ->add('accessesId', 'Required allow trading via API.');
            }
        } catch (BinanceProviderException) {
            $message = 'Incorrect API key or secret.';

            $validator->errors()
                ->add('accessesId', $message);
            $validator->errors()
                ->add('accessesId', $message);
        }
    }

    /**
     * Is grid allowed.
     *
     * @param Validator $validator
     * @param Pair $pair
     * @throws ConditionsException
     * @throws GridMustGreatThanZeroException
     * @throws GridTraderException
     * @throws ModelNotExistsException
     * @throws TypeIncorrectException
     * @throws UnsupportedTypeException
     */
    private function isGridAllowed(Validator $validator, Pair $pair): void
    {
        $maxOrders = $this->getBotsService()
            ->getMaxPossibleOrders(User::query()->find(1));

        if ($maxOrders + $this['grids'] > $pair->filter_max_num_orders) {
            $validator->errors()
                ->add('grids', sprintf('Grid count great than allowed %s.', $pair->filter_max_num_orders));

            return;
        }

        try {
            $this->deals = $this->getGridService()
                ->gridTrade(
                    new GridOptionsDto(
                        $this['lowerLimit'],
                        $this['upperLimit'],
                        $pair->last_price,
                        null,
                        $this['orderAmount'],
                        $this['grids'],
                        $pair->tickSizeAsFloat,
                        $this['type'],
                        $pair->toApi(),
                    )
                );

            $this->checkBalances($validator, $pair);
        } catch (LowerLimitGreatThanUpperLimitException) {
            $this->addLowerUpperErrors($validator, 'Upper limit great than lower limit.');
        } catch (LowerLimitLessThanPriceMinException) {
            $this->addLowerUpperErrors($validator, 'Lower limit less than min price.');
        } catch (UpperLimitGreatThanPriceMaxException) {
            $this->addLowerUpperErrors($validator, 'Upper limit great than max price.');
        } catch (CurrentPriceOutOfGridException) {
            $this->addLowerUpperErrors($validator, 'Current price out of grid.');
        } catch (QuantityLessThanMinNotionalException) {
            $validator->errors()
                ->add('orderAmount', 'Quantity per order less than min notional.');
        } catch (AmountPerOrderLessThanMinimalException $e) {
            $validator->errors()
                ->add('orderAmount', sprintf('Min amount equals %s quote asset.', $e->getMinQuantity()));
        }
    }

    /**
     * Add lower upper errors.
     *
     * @param Validator $validator
     * @param string $message
     */
    private function addLowerUpperErrors(Validator $validator, string $message)
    {
        $validator->errors()
            ->add('lowerLimit', $message);
        $validator->errors()
            ->add('upperLimit', $message);
    }

    /**
     * Check balances.
     *
     * @param Validator $validator
     * @param Pair $pair
     */
    private function checkBalances(Validator $validator, Pair $pair): void
    {
        $balances = $this->account
            ->getSymbolBalances($this['pair']);

        if (!$balances) {
            $validator->errors()
                ->add('orderAmount', 'You does\'t have enough money in required assets.');

            return;
        }

        $orderAmountMin = $this['orderAmount'] * ($this->deals->count() - 1);

        if ($orderAmountMin > $balances->quoteFree) {
            $validator->errors()
                ->add(
                    'orderAmount',
                    sprintf('You does\'t have enough money in quote asset. Your balance %s.', $balances->quoteFree)
                );
        }

        $avgPrice = ($this['lowerLimit'] + $this['upperLimit']) / 2;

        if ($orderAmountMin / $avgPrice > $balances->baseFree) {
            $validator->errors()
                ->add(
                    'orderAmount',
                    sprintf('You does\'t have enough money in base asset. Your balance %s.', $balances->baseFree)
                );
        }
    }

    /**
     * Get {@see AccessesService::class}.
     *
     * @return AccessesService
     */
    private function getAccessesService(): AccessesService
    {
        return app(AccessesService::class);
    }

    /**
     * Get {@see MarketDataService::class}.
     *
     * @return MarketDataService
     */
    private function getMarketDataService(): MarketDataService
    {
        return app(MarketDataService::class);
    }

    /**
     * Get {@see BinanceService::class}.
     *
     * @return BinanceService
     */
    private function getBinanceService(): BinanceService
    {
        return app(BinanceService::class);
    }

    /**
     * Get {@see GridService::class}.
     *
     * @return GridService
     */
    private function getGridService(): GridService
    {
        return app(GridService::class);
    }

    /**
     * Get {@see BotsService::class}.
     *
     * @return BotsService
     */
    private function getBotsService(): BotsService
    {
        return app(BotsService::class);
    }
}
