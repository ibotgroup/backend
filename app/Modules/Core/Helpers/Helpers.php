<?php
declare(strict_types=1);

use App\Modules\Bots\Dto\Traders\Deals\DealDto;
use App\Modules\Bots\Enum\Trades\OrderTypeEnum;
use App\Modules\Bots\Enum\Trades\TypeEnum;
use App\Modules\Market\Models\Pair;
use Illuminate\Support\Collection;

function tryJsonDecode(string $json): mixed
{
    try {
        return json_decode(json: $json, flags: JSON_THROW_ON_ERROR);
    }
    catch (Throwable) {}

    return null;
}

function priceCeil(float $val, float|int $precision = 0.01): float
{
    if (is_float($precision)) {
        $precision = strlen(explode('.', rtrim(sprintf("%f", $precision), '0'))[1]);
    }

    if ($precision < 0) {
        $precision = 0;
    }

    $reg = $val + 0.5 / (10 ** $precision);
    return round($reg, $precision, $reg > 0 ? PHP_ROUND_HALF_DOWN : PHP_ROUND_HALF_UP);
}

function priceFloor(float $val, float|int $precision = 0.01): float
{
    if (is_float($precision)) {
        $precision = strlen(explode('.', rtrim(sprintf("%f", $precision), '0'))[1]);
    }

    if ($precision < 0) {
        $precision = 0;
    }

    $reg = $val - 0.5 / (10 ** $precision);
    return round($reg, $precision, $reg > 0 ? PHP_ROUND_HALF_UP : PHP_ROUND_HALF_DOWN);
}


function rndSid(): int
{
    return mt_rand(1000000000000000, 9223372036854775);
}

function dumpDealDtoCollection(Collection $deals, Pair $pair): void
{
    echo sprintf("\t      Market price: %s\n", $pair->last_price);
    echo "==================================================\n";


    $deals->each(function (DealDto $deal) use ($pair) {
        echo sprintf(
            "Order %s\t%s %s:\t\t%s\n",
            TypeEnum::TYPES_TEXT[$deal->type],
            OrderTypeEnum::TYPES_TEXT[$deal->orderType],
            priceCeil($deal->quantity, $pair->stepSizeAsFloat),
            $deal->price,
        );
    });

    echo "==================================================\n\n\n";
}
