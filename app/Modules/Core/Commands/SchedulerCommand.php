<?php
declare(strict_types=1);

namespace App\Modules\Core\Commands;

use App\Modules\Core\Contracts\CoreModuleContract;
use App\Modules\Core\Services\Scheduler\SchedulerService;
use Illuminate\Console\Command;
use Throwable;
use function sprintf;

/**
 * Scheduler command.
 */
final class SchedulerCommand extends Command implements CoreModuleContract
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'core:scheduler {--jobs=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Scheduler command';

    /**
     * Execute the console command.
     *
     * @return int
     * @throws Throwable
     */
    public function handle(): int
    {
        $jobs = $this->option('jobs');

        if ($jobs) {
            $jobs = explode(',', $jobs);
        }
        else {
            $jobs = [];
        }

        $stopRequested = false;

        pcntl_signal(SIGTERM, function () use (&$stopRequested) {
            $stopRequested = true;
        });

        $limit = 86440;

        while ($limit > 0) {
            if ($stopRequested) {
                $this->info('Terminating by received stop signal');

                break;
            }

            $limit--;

            try {
                $int = $this->getSchedulerService()
                    ->everySecond($jobs);

                $this->info(sprintf('Added %s tasks', $int));

                sleep(1);
            } catch (Throwable) {
            }
        }

        return 1;
    }

    /**
     * Get {@see SchedulerService::class}.
     *
     * @return SchedulerService
     */
    private function getSchedulerService(): SchedulerService
    {
        return app(SchedulerService::class);
    }
}
