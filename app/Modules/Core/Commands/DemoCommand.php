<?php
declare(strict_types=1);

namespace App\Modules\Core\Commands;

use App\Modules\Core\Contracts\CoreModuleContract;
use App\Modules\Core\Traits\Tasks\WeightControl;
use Illuminate\Console\Command;

/**
 * Demo command.
 */
final class DemoCommand extends Command implements CoreModuleContract
{
    use WeightControl;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'core:demo';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Demo command';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        return 0;
    }
}
