<?php
declare(strict_types=1);

namespace App\Modules\Core\Commands;

use App\Modules\Core\Contracts\CoreModuleContract;
use App\Modules\Users\Services\UsersService;
use Illuminate\Console\Command;

/**
 * Create user command.
 */
final class CreateUserCommand extends Command implements CoreModuleContract
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:create {email} {password}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create new user';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        $userService = $this->getUsersService();

        if ($userService->getCountUsers() > 0) {
            return -1;
        }

        $email = trim($this->argument('email'));
        $password = trim($this->argument('password'));

        $userService->create(
            $email,
            $email,
            $password
        );

        $this->info('User created');

        return 0;
    }

    /**
     * Get {@see UsersService::class}.
     *
     * @return UsersService
     */
    private function getUsersService(): UsersService
    {
        return app(UsersService::class);
    }
}
