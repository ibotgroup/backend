<?php
declare(strict_types=1);

namespace App\Modules\Core\Repositories;

use App\Modules\Core\Contracts\Repositories\RepositoryContract;
use App\Modules\Core\Exceptions\Repositories\InArgumentArrayIsEmptyException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * Base repository.
 */
abstract class BaseRepository implements RepositoryContract
{
    /**
     * Find by id.
     *
     * @param int|string $id
     * @return Model|null
     */
    public function find(int|string $id): ?Model
    {
        return $this->model
            ->newQuery()
            ->find($id);
    }

    /**
     * Find by sid.
     *
     * @param int|string $sid
     * @return Model|null
     */
    public function findBySid(int|string $sid): ?Model
    {
        return $this->model
            ->newQuery()
            ->where('sid', $sid)
            ->first();
    }

    /**
     * Get all data.
     *
     * @param array $exclude
     * @param string $orderBy
     * @param string $orderType
     * @param bool $lockForUpdate
     * @return Collection
     */
    public function all(array $exclude = [], string $orderBy = 'id', string $orderType = 'ASC', bool $lockForUpdate = false): Collection
    {
        $query = $this->model->newQuery()
            ->orderBy($orderBy, $orderType);

        if (count($exclude) > 0) {
            $query->whereNotIn('id', $exclude);
        }

        if ($lockForUpdate) {
            $query->lockForUpdate();
        }

        return $query->get();
    }

    /**
     * Get by ids.
     *
     * @param Collection|array $ids
     * @return Collection
     */
    public function getByIds(Collection|array $ids): Collection
    {
        if (count($ids) === 0) {
            return collect();
        }

        return $this->model
            ->newQuery()
            ->whereIn('id', $ids)
            ->get();
    }

    /**
     * Count all.
     *
     * @return int
     */
    public function count(): int
    {
        return $this->model
            ->newQuery()
            ->count();
    }

    /**
     * Get models with limit and offset.
     *
     * @param int $limit
     * @param int $offset
     * @param bool $lockForUpdate
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getWithLimitOffset(int $limit, int $offset, bool $lockForUpdate = false): \Illuminate\Database\Eloquent\Collection
    {
        $query = $this->model
            ->newQuery()
            ->orderBy('id', 'ASC')
            ->limit($limit)
            ->offset($offset);

        if ($lockForUpdate) {
            $query->lockForUpdate();
        }

        return $query->get();
    }

    /**
     * Get models with limit and offset.
     *
     * @param int $id
     * @return Model|null
     */
    public function getAndLockForUpdate(int $id): ?Model
    {
        return $this->model
            ->newQuery()
            ->where('id', $id)
            ->lockForUpdate()
            ->first();
    }

    /**
     * Check in argument.
     *
     * @param array $argument
     * @throws InArgumentArrayIsEmptyException
     */
    protected function checkInArgument(array $argument): void
    {
        if (count($argument) === 0) {
            throw new InArgumentArrayIsEmptyException();
        }
    }
}
