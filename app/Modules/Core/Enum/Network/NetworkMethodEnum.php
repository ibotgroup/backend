<?php
declare(strict_types=1);

namespace App\Modules\Core\Enum\Network;

use App\Modules\Core\Enum\EnumContract;

/**
 * NetworkMethodEnum.
 */
final class NetworkMethodEnum implements EnumContract
{
    public const METHOD_GET = 'GET';
    public const METHOD_POST = 'POST';
    public const METHOD_PUT = 'PUT';
    public const METHOD_DELETE = 'DELETE';
    public const METHOD_OPTIONS = 'OPTIONS';
    public const METHOD_PATCH = 'PATCH';

    public const METHODS = [
        self::METHOD_GET,
        self::METHOD_POST,
        self::METHOD_PUT,
        self::METHOD_DELETE,
        self::METHOD_OPTIONS,
        self::METHOD_PATCH,
    ];
}
