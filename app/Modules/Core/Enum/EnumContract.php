<?php
declare(strict_types=1);

namespace App\Modules\Core\Enum;

use App\Modules\Core\Contracts\CoreModuleContract;

/**
 * EnumInterface.
 */
interface EnumContract extends CoreModuleContract
{

}
