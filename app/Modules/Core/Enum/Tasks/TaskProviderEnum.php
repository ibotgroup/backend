<?php
declare(strict_types=1);

namespace App\Modules\Core\Enum\Tasks;

/**
 * Task provider enum.
 */
final class TaskProviderEnum
{
    public const PROVIDER_DEFAULT = 1;
    public const PROVIDER_BINANCE = 2;
    public const PROVIDER_PLANING = 3;

    public const PROVIDERS = [
        self::PROVIDER_DEFAULT,
        self::PROVIDER_BINANCE,
        self::PROVIDER_PLANING,
    ];
}
