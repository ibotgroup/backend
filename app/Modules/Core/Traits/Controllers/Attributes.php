<?php
declare(strict_types=1);

namespace App\Modules\Core\Traits\Controllers;

use App\Modules\Core\Attributes\Controllers\CatchRedirectResponse;
use App\Modules\Core\Attributes\Controllers\CatchResponse;
use App\Modules\Core\Attributes\Controllers\Transactional;
use App\Modules\Core\Exceptions\Traits\Controllers\OnlyOneCatchAttributeAllowedException;
use App\Modules\Core\Facades\DB;
use App\Modules\Logs\Loggers\Logger;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;
use Illuminate\Validation\ValidationException;
use ReflectionException;
use ReflectionObject;
use Throwable;

/**
 * This trait adds attributes support to controllers.
 */
trait Attributes
{
    private array $_attrCache = [];

    /**
     * {@inheritDoc}
     * @throws ValidationException
     * @throws AuthorizationException
     * @throws ReflectionException
     * @throws Throwable
     */
    public function callAction($method, $parameters)
    {
        $transactional = $this->findTransactionalAttribute($method);

        try {
            if ($transactional) {
                DB::beginTransaction();
            }

            $data = parent::callAction($method, $parameters);

            if ($transactional) {
                DB::commit();
            }

            return $data;
        } catch (ValidationException | AuthorizationException $e) {
            throw $e;
        } catch (Throwable $e) {
            if ($transactional) {
                DB::rollBack();
            }

            return $this->catchCallAction($method, $e, $transactional);
        }
    }

    /**
     * Process exception case.
     *
     * @param string $method
     * @param Throwable $e
     * @param Transactional|null $transactional
     * @return Response|RedirectResponse
     * @throws ReflectionException
     * @throws Throwable
     */
    protected function catchCallAction(string $method, Throwable $e, Transactional $transactional = null): Response|RedirectResponse
    {
        $useCatch = $this->findCatchResponseAttribute($method);

        if ($transactional && $transactional->logger) {
            $this->getLogger()
                ->rollbackTransaction($e);
        }

        if ($useCatch) {
            if ((!$transactional || !$transactional->logger) && $useCatch->logger) {
                $this->getLogger()
                    ->registerException($e);
            }

            if ($useCatch instanceof CatchRedirectResponse) {
                $redirect = redirect()
                    ->route($useCatch->route);

                if (null !== $useCatch->message) {
                    $redirect->withErrors(__($useCatch->message));
                }

                return $redirect;
            } else if ($useCatch instanceof CatchResponse) {
                return response(
                    (new $useCatch->errorClass)
                        ->toArray(),
                    $useCatch->errorCode
                );
            }
        }

        throw $e;
    }

    /**
     * Find {@see CatchResponse::class} attribute.
     *
     * @param string $method
     * @return CatchResponse|CatchRedirectResponse|null
     * @throws ReflectionException
     * @throws OnlyOneCatchAttributeAllowedException
     */
    protected function findCatchResponseAttribute(string $method): CatchResponse|CatchRedirectResponse|null
    {
        return $this->getActionAttributes($method)
            ->filter(fn(object $object) => $object instanceof CatchResponse ||
                $object instanceof CatchRedirectResponse
            )
            ->first();
    }

    /**
     * Find {@see Transactional::class} attribute.
     *
     * @param string $method
     * @return Transactional|null
     * @throws ReflectionException
     * @throws OnlyOneCatchAttributeAllowedException
     */
    protected function findTransactionalAttribute(string $method): ?Transactional
    {
        return $this->getActionAttributes($method)
            ->filter(fn(object $object) => $object instanceof Transactional)
            ->first();
    }

    /**
     * Get attributes execution method.
     *
     * @param string $method
     * @return Collection
     * @throws ReflectionException
     * @throws OnlyOneCatchAttributeAllowedException
     */
    protected function getActionAttributes(string $method): Collection
    {
        if (isset($this->_attrCache[$method])) {
            return $this->_attrCache[$method];
        }

        $methodAttributes = collect();
        $reflection = new ReflectionObject($this);
        $catchUsed = false;

        foreach ($reflection->getMethod($method)
                     ->getAttributes() as $attribute) {
            $instance = $attribute->newInstance();

            if ($instance instanceof CatchResponse ||
                $instance instanceof CatchRedirectResponse
            ) {
                if ($catchUsed) {
                    throw new OnlyOneCatchAttributeAllowedException();
                }

                $methodAttributes->add($instance);
                $catchUsed = true;
            } else if ($instance instanceof Transactional) {
                $methodAttributes->add($instance);
            }
        }

        $this->_attrCache[$method] = $methodAttributes;

        return $methodAttributes;
    }

    /**
     * Get {@see Logger::class}.
     *
     * @return Logger
     */
    protected function getLogger(): Logger
    {
        return app(Logger::class);
    }
}
