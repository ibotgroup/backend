<?php
declare(strict_types=1);

namespace App\Modules\Core\Traits;

use App\Modules\Core\Exceptions\Traits\Lock\TransactionNotStartedException;
use App\Modules\Core\Facades\DB;
use App\Modules\Core\Locks\AtomicLocks;
use Illuminate\Contracts\Cache\LockTimeoutException;
use Illuminate\Database\Eloquent\Model;
use function sprintf;

/**
 * Lock trait.
 */
trait Lock
{
    /**
     * Get model by id with lock for update.
     *
     * @param int $id
     * @param array|null $columns
     * @return Model|null
     * @throws TransactionNotStartedException
     */
    public static function getAndLockForUpdate(int $id, array $columns = null): ?static
    {
        static::checkTransaction();

        $query = static::query()
            ->where('id', $id)
            ->lockForUpdate();

        if ($columns && count($columns) > 0) {
            $query->select($columns);
        }

        return $query->first();
    }

    /**
     * Set lock for model by id.
     *
     * @param int $id
     * @throws TransactionNotStartedException
     */
    public static function modelLockForUpdate(int $id): void
    {
        static::checkTransaction();
        static::getAndLockForUpdate($id, ['id']);
    }

    /**
     * Get lock for db row and refresh model after it.
     *
     * @param Model $model
     * @throws TransactionNotStartedException
     */
    public static function refreshAndLockForUpdate(Model $model): void
    {
        static::checkTransaction();
        static::modelLockForUpdate($model->id);

        $model->refresh();
    }

    /**
     * Check that transaction was started.
     *
     * @throws TransactionNotStartedException
     */
    public static function checkTransaction()
    {
        if (DB::transactionLevel() === 0) {
            throw new TransactionNotStartedException();
        }
    }

    /**
     * Lock await.
     *
     * @param int $seconds
     * @param callable $callable
     * @throws LockTimeoutException
     */
    public function lockAwait(int $seconds, callable $callable): void
    {
        AtomicLocks::lockAwait(
            $seconds,
            $seconds,
            sprintf('%s_%s_atomic', $this->table, $this->id),
            $callable
        );
    }

    /**
     * Lock skip.
     *
     * @param int $seconds
     * @param callable $callable
     */
    public function lockSkip(int $seconds, callable $callable): void
    {
        AtomicLocks::lockSkip(
            $seconds,
            sprintf('%s_%s_atomic', $this->table, $this->id),
            $callable
        );
    }
}
