<?php
declare(strict_types=1);

namespace App\Modules\Core\Traits\Tasks;

use App\Modules\Core\Locks\AtomicLocks;
use DateTime;
use Illuminate\Contracts\Cache\LockTimeoutException;
use Illuminate\Support\Facades\Cache;

/**
 * WeightControl trait.
 */
trait WeightControl
{
    private ?int $leftWeight = null;
    private DateTime|null $leftWeightAt = null;

    /**
     * {@inheritDoc}
     */
    public function leftWeight(): ?int
    {
        return $this->leftWeight;
    }

    /**
     * {@inheritDoc}
     */
    public function leftWeightAt(): ?DateTime
    {
        return $this->leftWeightAt;
    }

    /**
     * Set left weight.
     *
     * @param int|null $weight
     * @param DateTime|null $at
     * @throws LockTimeoutException
     */
    public function setLeftWeight(?int $weight, ?DateTime $at)
    {
        $this->leftWeight = $weight;
        $this->leftWeightAt = $at;

        if ($weight !== null) {
            $this->putLeftWeigh($weight);
        }
    }

    /**
     * Increment left weight.
     *
     * @param int|null $weight
     * @throws LockTimeoutException
     */
    protected function putLeftWeigh(?int $weight): void
    {
        AtomicLocks::lockAwait(
            1,
            1,
            'binance_left_weight_lock',
            function () use ($weight) {
                Cache::put('binance_left_weight', $weight);
            }
        );
    }

    /**
     * Get increment left weight.
     *
     * @return int|null
     */
    protected function getIncrementedLeftWeight(): ?int
    {
        $leftWeightRaw = Cache::get('binance_left_weight');

        return $leftWeightRaw !== null ? (int)$leftWeightRaw : null;
    }

    /**
     * Get and increment left weight
     *
     * @param int $weight
     * @return bool
     * @throws LockTimeoutException
     */
    protected function checkAndIncrementLeftWeight(int $weight): bool
    {
        $status = false;

        AtomicLocks::lockAwait(
            1,
            1,
            'binance_left_weight_lock',
            function () use ($weight, &$status) {
                $incrementedWeight = $this->getIncrementedLeftWeight();

                if ($incrementedWeight !== null && $incrementedWeight >= $weight) {
                    Cache::decrement('binance_left_weight', $weight);

                    $status = true;
                }
            }
        );

        return $status;
    }
}
