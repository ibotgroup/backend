<?php
declare(strict_types=1);

namespace App\Modules\Core\Traits;


use App\Modules\Core\Exceptions\Traits\ProtectedProperties\NotAllowedChangingPropertyException;
use App\Modules\Core\Exceptions\Traits\ProtectedProperties\PropertyNotExistsException;

/**
 * ProtectedProperties.
 */
trait ProtectedProperties
{
    /**
     * Get property DTO.
     *
     * @param string $name
     * @return mixed
     * @throws PropertyNotExistsException
     */
    public function __get(string $name): mixed
    {
        if (!property_exists($this, $name)) {
            throw new PropertyNotExistsException();
        }

        return $this->$name;
    }

    /**
     * Set property DTO.
     *
     * @param string $name
     * @param mixed $value
     * @return mixed
     * @throws NotAllowedChangingPropertyException
     */
    public function __set(string $name, mixed $value): void
    {
        if (property_exists($this, 'excludedReadOnly') &&
            in_array($name, $this->excludedReadOnly)) {
            $this->$name = $value;
        } else {
            throw new NotAllowedChangingPropertyException();
        }
    }
}
