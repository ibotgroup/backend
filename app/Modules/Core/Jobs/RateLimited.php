<?php
declare(strict_types=1);

namespace App\Modules\Core\Jobs;

use Illuminate\Contracts\Redis\LimiterTimeoutException;
use Illuminate\Support\Facades\Redis;

/**
 * Rate limited.
 */
final class RateLimited
{
    /**
     * Constructor.
     *
     * @param string $key
     * @param int $decay
     * @param bool $delete
     */
    public function __construct(private string $key, private int $decay, private bool $delete)
    {
    }

    /**
     * Handle.
     *
     * @param mixed $job
     * @param callable $next
     * @throws LimiterTimeoutException
     */
    public function handle(mixed $job, callable $next)
    {
        Redis::throttle($this->key)
            ->block(0)
            ->allow(1)
            ->every($this->decay)
            ->then(function () use ($job, $next) {
                $next($job);
            }, function () use ($job) {
                if ($this->delete) {
                    $job->delete();
                }
                else  {
                    $job->release($this->decay);
                }
            });
    }
}
