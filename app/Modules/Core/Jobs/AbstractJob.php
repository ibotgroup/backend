<?php
declare(strict_types=1);

namespace App\Modules\Core\Jobs;


use App\Modules\Core\Contracts\Jobs\JobContract;
use App\Modules\Core\Contracts\Jobs\WeightControlled;

/**
 * Abstract job.
 */
abstract class AbstractJob implements JobContract
{
    /**
     * Print info about job.
     */
    public function info(): void
    {
        if (!config('app.debug')) {
            return;
        }

        if ($this instanceof WeightControlled) {
            dump(sprintf('%s. Weight %s', $this->getId(), $this->getIncrementedLeftWeight()));
        } else {
            dump(sprintf('%s.', $this->getId()));
        }
    }
}
