<?php
declare(strict_types=1);

namespace App\Modules\Core\Models;

use App\Modules\Core\Contracts\CoreModuleContract;

/**
 * ModelInterface.
 */
interface ModelContract extends CoreModuleContract
{

}
