<?php
declare(strict_types=1);

namespace App\Modules\Core\Models;

use App\Modules\Core\Exceptions\Models\ModelNotExistsException;
use App\Modules\Core\Traits\Lock;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model as BaseModel;

/**
 * Abstract model.
 */
abstract class Model extends BaseModel implements ModelContract
{
    use HasFactory, Lock;

    /**
     * Throw if not exists.
     *
     * @throws ModelNotExistsException
     */
    protected function throwIfNotExists()
    {
        if (!$this->exists || !$this->id) {
            throw new ModelNotExistsException();
        }
    }
}
