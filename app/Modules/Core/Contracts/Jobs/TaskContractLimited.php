<?php
declare(strict_types=1);

namespace App\Modules\Core\Contracts\Jobs;

use App\Modules\Core\Contracts\CoreModuleContract;

/**
 * TaskContractLimited.
 */
interface TaskContractLimited extends CoreModuleContract
{
    /**
     * Get pool limit.
     *
     * @return int
     */
    public function getPoolLimit(): int;

    /**
     * Get interval limit.
     *
     * @return int|null
     */
    public function getIntervalLimit(): ?int;

    /**
     * Cancel overdue limit.
     *
     * @return bool
     */
    public function cancelOverdueLimit(): bool;

    /**
     * Without overlapping.
     *
     * @return bool
     */
    public function withoutOverlapping(): bool;
}
