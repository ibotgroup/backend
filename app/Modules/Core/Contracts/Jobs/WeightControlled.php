<?php
declare(strict_types=1);

namespace App\Modules\Core\Contracts\Jobs;

use App\Modules\Core\Contracts\CoreModuleContract;
use DateTime;

/**
 * WeightControlled.
 */
interface WeightControlled extends CoreModuleContract
{
    /** Left weight. */
    public function leftWeight(): ?int;

    /** Left weight at. */
    public function leftWeightAt(): ?DateTime;
}
