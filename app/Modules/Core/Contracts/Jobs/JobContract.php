<?php
declare(strict_types=1);

namespace App\Modules\Core\Contracts\Jobs;

use App\Modules\Core\Contracts\CoreModuleContract;

/**
 * TaskContract.
 */
interface JobContract extends CoreModuleContract
{
    /**
     * Get task id.
     *
     * @return string
     */
    public function getId(): string;
    /**
     * Get weight.
     *
     * @return int
     */
    public function getWeight(): int;

    /**
     * Handle task.
     */
    public function handle(): void;
}
