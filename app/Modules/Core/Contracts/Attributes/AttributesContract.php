<?php
declare(strict_types=1);

namespace App\Modules\Core\Contracts\Attributes;

use App\Modules\Core\Contracts\CoreModuleContract;

/**
 * AttributesContract.
 */
interface AttributesContract extends CoreModuleContract
{

}
