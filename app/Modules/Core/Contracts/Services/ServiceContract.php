<?php
declare(strict_types=1);

namespace App\Modules\Core\Contracts\Services;

use App\Modules\Core\Contracts\CoreModuleContract;

/**
 * ServiceContract.
 */
interface ServiceContract extends CoreModuleContract
{

}
