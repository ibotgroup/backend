<?php
declare(strict_types=1);

namespace App\Modules\Core\Contracts\Services;

use App\Modules\Core\Contracts\CoreModuleContract;
use App\Modules\Core\Models\ModelContract;

/**
 * Model.
 */
interface ModelManagerContract extends CoreModuleContract
{
    /**
     * Find.
     *
     * @param int $id
     * @return ModelContract|null
     */
    public function find(int $id): ?ModelContract;

    /**
     * Find by sid.
     *
     * @param string $sid
     * @return ModelContract|null
     */
    public function findBySid(string $sid): ?ModelContract;
}
