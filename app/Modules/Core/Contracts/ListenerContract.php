<?php
declare(strict_types=1);

namespace App\Modules\Core\Contracts;

/**
 * EventContract.
 */
interface ListenerContract extends CoreModuleContract
{
    /**
     * Handle event.
     *
     * @param EventContract $event
     */
    public function handle(EventContract $event): void;
}
