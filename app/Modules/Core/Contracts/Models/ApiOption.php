<?php
declare(strict_types=1);

namespace App\Modules\Core\Contracts\Models;

use App\Modules\Core\Contracts\CoreModuleContract;
use App\Modules\Core\Dto\Api\ApiOptionDto;

/**
 * Api options.
 */
interface ApiOption extends CoreModuleContract
{
    public function toApiOption(): ApiOptionDto;
}
