<?php
declare(strict_types=1);

namespace App\Modules\Core\Contracts\Models;

use App\Modules\Core\Contracts\CoreModuleContract;
use App\Modules\Core\Dto\BaseDto;

/**
 * LightApi.
 */
interface LightApi extends CoreModuleContract
{
    public function toApi(): BaseDto;
}
