<?php
declare(strict_types=1);

namespace App\Modules\Core\Contracts\Models;

use App\Modules\Core\Contracts\CoreModuleContract;
use App\Modules\Core\Dto\BaseDto;

/**
 * Api.
 */
interface Api extends CoreModuleContract
{
    public function toApi(): BaseDto;
}
