<?php
declare(strict_types=1);

namespace App\Modules\Core\Contracts\Base;

use App\Modules\Core\Contracts\CoreModuleContract;

/**
 * InvokeContract
 */
interface InvokeContract extends CoreModuleContract
{
}
