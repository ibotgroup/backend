<?php
declare(strict_types=1);

namespace App\Modules\Core\Contracts\Repositories;

use App\Modules\Core\Contracts\CoreModuleContract;

/**
 * RepositoryContract.
 */
interface RepositoryContract extends CoreModuleContract
{

}
