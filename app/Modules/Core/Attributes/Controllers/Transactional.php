<?php
declare(strict_types=1);

namespace App\Modules\Core\Attributes\Controllers;

use App\Modules\Core\Traits\ProtectedProperties;
use Attribute;

/**
 * This attribute controller-level
 * which instruction abstract controller
 * that it method must exception in transaction.
 *
 * @property-read bool $logger
 */
#[Attribute(Attribute::TARGET_METHOD)]
class Transactional
{
    use ProtectedProperties;

    /**
     * Constructor.
     *
     * @param bool $logger
     */
    public function __construct(
        private bool $logger = true,
    )
    {
    }
}
