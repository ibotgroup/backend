<?php
declare(strict_types=1);

namespace App\Modules\Core\Attributes\Controllers;

use App\Modules\Core\Dto\BaseDto;
use App\Modules\Core\Dto\ResponseErrorDto;
use App\Modules\Core\Exceptions\Attributes\Controllers\CatchResponse\ErrorClassNotSupportedException;
use App\Modules\Core\Traits\ProtectedProperties;
use Attribute;

/**
 * This attribute controller-level
 * which instruction abstract controller
 * that this method must execution in try/catch
 * and response with specified error class.
 *
 * @property-read string $errorClass
 * @property-read int $errorCode
 * @property-read bool $logger
 */
#[Attribute(Attribute::TARGET_METHOD)]
class CatchResponse
{
    use ProtectedProperties;

    /**
     * Constructor..
     *
     * @param string $errorClass
     * @param int $errorCode
     * @param bool $logger
     * @throws ErrorClassNotSupportedException
     */
    public function __construct(
        private string $errorClass = ResponseErrorDto::class,
        private int $errorCode = 503,
        private bool $logger = true,
    )
    {
        if (!is_subclass_of($this->errorClass, BaseDto::class)) {
            throw new ErrorClassNotSupportedException();
        }
    }
}
