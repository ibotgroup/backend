<?php
declare(strict_types=1);

namespace App\Modules\Core\Attributes\Controllers;

use App\Modules\Core\Traits\ProtectedProperties;
use Attribute;

/**
 * This attribute controller-level
 * which instruction abstract controller
 * that this method must execution in try/catch
 * and redirect to specified page.
 *
 *
 * @property-read string $route
 * @property-read string|null $message
 * @property-read bool $logger
 */
#[Attribute(Attribute::TARGET_METHOD)]
class CatchRedirectResponse
{
    use ProtectedProperties;

    /**
     * Constructor.
     *
     * @param string $route
     * @param string|null $message
     * @param bool $logger
     */
    public function __construct(
        private string $route,
        private ?string $message = null,
        private bool $logger = true,
    )
    {
    }
}
