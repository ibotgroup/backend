<?php
declare(strict_types=1);

namespace App\Modules\Core\Validation;

use Illuminate\Validation\Validator;

/**
 * This class validating type is numeric value.
 */
final class Numeric
{
    public const NAME = 'numeric';

    /**
     * Validate.
     *
     * @param string $attribute
     * @param mixed $value
     * @param array $parameters
     * @param Validator $validator
     * @return bool
     */
    public function validate(string $attribute, mixed $value, array $parameters, Validator $validator): bool
    {
        return is_numeric($value);
    }
}
