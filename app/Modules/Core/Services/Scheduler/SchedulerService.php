<?php
declare(strict_types=1);

namespace App\Modules\Core\Services\Scheduler;

use App\Modules\Bots\Tasks\TraderJob;
use App\Modules\Core\Contracts\Services\ServiceContract;
use App\Modules\Core\Locks\AtomicLocks;
use App\Modules\Deals\Tasks\DealsPreExecutorJob;
use App\Modules\Deals\Tasks\DealsPreUpdaterJob;
use App\Modules\Market\Tasks\ExchangeInfoJob;
use App\Modules\Market\Tasks\PricesUpdaterJob;

/**
 * Scheduler service.
 */
final class SchedulerService implements ServiceContract
{
    /**
     * Add system task.
     *
     * @param array $jobs
     * @return int
     */
    public function everySecond(array $jobs): int
    {
        $count = 0;

        if (count($jobs) === 0 || in_array('trader', $jobs)) {
            TraderJob::dispatch();

            $count++;
        }

        if (count($jobs) === 0 || in_array('deals-updater', $jobs)) {
            DealsPreUpdaterJob::dispatch();

            $count++;
        }

        if (count($jobs) === 0 || in_array('deals-executor', $jobs)) {
            DealsPreExecutorJob::dispatch();

            $count++;
        }

        if (count($jobs) === 0 || in_array('prices', $jobs)) {
            PricesUpdaterJob::dispatch();

            $count++;
        }

        if (count($jobs) === 0 || in_array('exchanges-info', $jobs)) {
            ExchangeInfoJob::dispatch();

            $count++;
        }

        return $count;
    }
}
