<?php
declare(strict_types=1);

namespace App\Modules\Core\Services;

use App\Modules\Core\Dto\Network\RequestDto;
use App\Modules\Core\Dto\Network\RestResponseDto;
use App\Modules\Core\Enum\Network\NetworkMethodEnum;
use App\Modules\Core\Exceptions\Services\Network\BadRequestException;
use App\Modules\Core\Exceptions\Services\Network\RequestException;
use App\Modules\Core\Contracts\Services\ServiceContract;
use App\Modules\Logs\Loggers\Logger;
use GuzzleHttp\Client;
use GuzzleHttp\TransferStats;
use Throwable;

/**
 * NetworkService.
 */
final class NetworkService implements ServiceContract
{
    public const CONNECT_TIMEOUT = 20;
    public const READ_TIMEOUT = 10;
    public const TOTAL_TIMEOUT = 30;

    /**
     * Execute request.
     *
     * @param RequestDto $request
     * @return RestResponseDto
     * @throws BadRequestException
     * @throws RequestException
     */
    public function restRequest(
        RequestDto $request,
    ): RestResponseDto
    {
        $log = [];
        $log['uri'] = $request->uri;
        $log['method'] = $request->method;
        $log['code'] = null;

        try {
            $client = new Client();

            $body = [
                'allow_redirects' => false,
                'connect_timeout' => self::CONNECT_TIMEOUT,
                'read_timeout' => self::READ_TIMEOUT,
                'on_stats' => function (TransferStats $stats) use (&$log) {
                    $log['stats'] = $stats->getHandlerStats();

                    if ($stats->hasResponse()) {
                        $log['code'] = $stats->getResponse()->getStatusCode();
                        $log['response']['headers'] = $stats->getResponse()->getHeaders();
                        $log['response']['content'] = json_decode($stats->getResponse()->getBody()->__toString());
                    }
                },
                'headers' => $request->headers
            ];

            $body['query'] = $request->query;

            if ($request->sendRequestInQuery) {
                $body['query'] = array_merge($body['query'], $request->request);
            }

            if (!$request->disableBody && in_array($request->method, [NetworkMethodEnum::METHOD_POST, NetworkMethodEnum::METHOD_PUT]) && count($request->request) > 0) {
                $body['body'] = json_encode($request->request);
            }

            $log['request'] = $body;
            unset($log['request']['on_stats']);

            $response = $client->request($request->method, $request->uri, $body);

            if ($response->getStatusCode() == 200) {
                return new RestResponseDto(
                    json_decode($response->getBody()->__toString()),
                    $response
                );
            } else {
                throw new RequestException();
            }
        } catch (\GuzzleHttp\Exception\RequestException $e) {
            if (config('app.dump_allow')) {
                dump($e, [$log, $request]);
            }

            throw new BadRequestException(
                previous: $e,
                fullResponse: $e->getResponse(),
                response: tryJsonDecode(
                    $e->getResponse()
                        ->getBody()
                        ->__toString()
                )
            );
        } catch (Throwable $e) {
            $this->getLogger()
                ->registerException($e, [$log, $request]);

            throw new RequestException($e->getMessage(), 0, $e);
        }
    }

    /**
     * Get {@see Logger::class}.
     *
     * @return Logger
     */
    private function getLogger(): Logger
    {
        return app(Logger::class);
    }
}
