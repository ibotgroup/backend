<?php
declare(strict_types=1);

namespace App\Modules\Core\Locks;

use Illuminate\Support\Facades\Cache;

/**
 * Atomic locks.
 */
class AtomicLocks
{
    /**
     * Lock method.
     *
     * @param int $lockSeconds
     * @param int $awaitSeconds
     * @param string $name
     * @param callable $callable
     * @throws \Illuminate\Contracts\Cache\LockTimeoutException
     */
    public static function lockAwait(int $lockSeconds, int $awaitSeconds, string $name, callable $callable)
    {
        $lock = Cache::lock(
            $name,
            $lockSeconds
        );

        try {
            $lock->block($awaitSeconds);

            $callable();
        } finally {
            optional($lock)->release();
        }
    }

    /**
     * Lock or skip.
     *
     * @param int $lockSeconds
     * @param string $name
     * @param callable $callable
     * @return bool|null
     */
    public static function lockSkip(int $lockSeconds, string $name, callable $callable): ?bool
    {
        return Cache::lock(
            $name,
            $lockSeconds
        )->get($callable);
    }
}
