<?php
declare(strict_types=1);

namespace App\Modules\Core\Dto\Network;

use App\Modules\Core\Dto\BaseDto;
use Psr\Http\Message\ResponseInterface;
use stdClass;

/**
 * Rest response dto..
 *
 * @property-read array|stdClass $data
 * @property-read ResponseInterface $response
 */
final class RestResponseDto extends BaseDto
{
    /**
     * Constructor.
     *
     * @param array|stdClass $data
     * @param ResponseInterface $response
     */
    public function __construct(
        protected array|stdClass $data,
        protected ResponseInterface $response,
    )
    {
    }
}
