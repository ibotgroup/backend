<?php
declare(strict_types=1);

namespace App\Modules\Core\Dto\Network;

use App\Modules\Core\Dto\BaseDto;
use App\Modules\Core\Enum\Network\NetworkMethodEnum;
use JetBrains\PhpStorm\ExpectedValues;

/**
 * RequestDto.
 *
 * @property-read string $uri
 * @property-read string $method
 * @property-read array $query
 * @property-read array $request
 * @property-read array $headers
 * @property-read bool $sendRequestInQuery
 * @property-read bool $disableBody
 */
final class RequestDto extends BaseDto
{
    /**
     * Constructor.
     *
     * @param string $uri
     * @param string $method
     * @param array $query
     * @param array $request
     * @param array $headers
     * @param bool $sendRequestInQuery
     * @param bool $disableBody
     */
    public function __construct(
        protected string $uri,
        #[ExpectedValues(NetworkMethodEnum::METHODS)] protected string $method,
        protected array $query = [],
        protected array $request = [],
        protected array $headers = [],
        protected bool $sendRequestInQuery = false,
        protected bool $disableBody = false,
    )
    {
    }
}
