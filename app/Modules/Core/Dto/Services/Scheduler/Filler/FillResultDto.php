<?php
declare(strict_types=1);

namespace App\Modules\Core\Dto\Services\Scheduler\Filler;

use App\Modules\Core\Dto\BaseDto;

/**
 * Fill result dto.
 *
 * @property-read int $cancelled
 * @property-read int $filled
 */
final class FillResultDto extends BaseDto
{
    /**
     * Constructor.
     *
     * @param int $cancelled
     * @param int $filled
     */
    public function __construct(
        protected int $cancelled,
        protected int $filled,
    )
    {

    }
}
