<?php
declare(strict_types=1);

namespace App\Modules\Core\Dto;

use Illuminate\Support\Collection;

/**
 * Response DTO.
 */
class ResponseDto extends BaseDto
{
    /** @var array|string[] Excluded read-only. */
    protected array $excludedReadOnly = ['data'];

    /** @var BaseDto|Collection|null Data. */
    protected BaseDto|Collection|null $data;

    /** @var bool Status request. */
    protected bool $status = true;

    /**
     * Constructor.
     *
     * @param BaseDto|Collection|null $data
     * @param bool|null $status
     */
    public function __construct(
        BaseDto|Collection|null $data = null,
        ?bool $status = null
    )
    {
        if ($data !== null) {
            $this->data = $data;
        }

        if ($status !== null) {
            $this->status = $status;
        }
    }
}
