<?php
declare(strict_types=1);

namespace App\Modules\Core\Dto\Api;

use App\Modules\Core\Contracts\CoreModuleContract;
use App\Modules\Core\Dto\BaseDto;

/**
 * Api option dto.
 *
 * @property-read string $value
 * @property-read string $label
 */
class ApiOptionDto extends BaseDto implements CoreModuleContract
{
    /**
     * Constructor.
     *
     * @param string $value
     * @param string $label
     */
    public function __construct(
        protected string $value,
        protected string $label,
    ) {
    }
}
