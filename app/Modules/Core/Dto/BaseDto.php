<?php
declare(strict_types=1);

namespace App\Modules\Core\Dto;

use App\Modules\Core\Contracts\CoreModuleContract;
use App\Modules\Core\Traits\ProtectedProperties;
use Illuminate\Contracts\Support\Arrayable;
use JsonSerializable;

/**
 * Abstract dto class.
 */
abstract class BaseDto implements JsonSerializable, Arrayable, Immutable, CoreModuleContract
{
    use ProtectedProperties;

    /**
     * {@inheritDoc}
     */
    public function toArray(): array
    {
        $data = get_object_vars($this);

        if (array_key_exists('excludedReadOnly', $data)) {
            unset($data['excludedReadOnly']);
        }

        return $data;
    }

    /**
     * {@inheritDoc}
     */
    public function jsonSerialize(): array
    {
        return $this->toArray();
    }
}
