<?php
declare(strict_types=1);

namespace App\Modules\Core\Dto;

use JetBrains\PhpStorm\Pure;

/**
 * DTO for response error in api.
 */
class ResponseErrorDto extends ResponseDto
{
    /**
     * Constructor.
     */
    #[Pure]
    public function __construct()
    {
        parent::__construct(status: false);
    }
}
