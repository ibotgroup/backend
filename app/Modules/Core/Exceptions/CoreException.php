<?php
declare(strict_types=1);

namespace App\Modules\Core\Exceptions;

use App\Modules\Core\Contracts\CoreModuleContract;

/**
 * CoreException.
 */
interface CoreException extends CoreModuleContract
{

}
