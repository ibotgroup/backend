<?php
declare(strict_types=1);

namespace App\Modules\Core\Exceptions\Services;

use App\Modules\Core\Exceptions\CoreException;

/**
 * ServicesException.
 */
interface ServicesException extends CoreException
{
}
