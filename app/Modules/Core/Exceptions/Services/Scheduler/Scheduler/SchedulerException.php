<?php
declare(strict_types=1);

namespace App\Modules\Core\Exceptions\Services\Scheduler\Scheduler;


use App\Modules\Core\Exceptions\Services\Scheduler\SchedulerException as BaseSchedulerException;

/**
 * SchedulerException.
 */
interface SchedulerException extends BaseSchedulerException
{

}
