<?php
declare(strict_types=1);

namespace App\Modules\Core\Exceptions\Services\Scheduler\Scheduler;

/**
 * TaskNotLockedException.
 */
class TaskNotLockedException extends \Exception implements SchedulerException
{

}
