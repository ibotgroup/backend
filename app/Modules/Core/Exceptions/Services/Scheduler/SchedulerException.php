<?php
declare(strict_types=1);

namespace App\Modules\Core\Exceptions\Services\Scheduler;

use App\Modules\Core\Exceptions\Services\ServicesException;

/**
 * ServicesException.
 */
interface SchedulerException extends ServicesException
{
}
