<?php
declare(strict_types=1);

namespace App\Modules\Core\Exceptions\Services\Scheduler\Dispatcher;


use App\Modules\Core\Exceptions\Services\Scheduler\SchedulerException as BaseSchedulerException;

/**
 * DispatcherException.
 */
interface DispatcherException extends BaseSchedulerException
{

}
