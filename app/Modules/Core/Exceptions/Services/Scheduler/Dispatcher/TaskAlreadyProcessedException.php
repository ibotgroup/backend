<?php
declare(strict_types=1);

namespace App\Modules\Core\Exceptions\Services\Scheduler\Dispatcher;

use Exception;

/**
 * TaskAlreadyProcessedException.
 */
class TaskAlreadyProcessedException extends Exception implements DispatcherException
{

}
