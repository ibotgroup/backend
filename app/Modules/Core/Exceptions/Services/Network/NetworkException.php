<?php
declare(strict_types=1);

namespace App\Modules\Core\Exceptions\Services\Network;

use App\Modules\Core\Exceptions\Services\ServicesException;

/**
 * NetworkException.
 */
interface NetworkException extends ServicesException
{
}
