<?php
declare(strict_types=1);

namespace App\Modules\Core\Exceptions\Services\Network;

use JetBrains\PhpStorm\Pure;
use Psr\Http\Message\ResponseInterface;
use Throwable;

/**
 * BadRequestException.
 */
class BadRequestException extends RequestException
{
    /**
     * Constructor.
     *
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     * @param ResponseInterface|null $fullResponse
     * @param mixed|null $response
     */
    #[Pure]
    public function __construct(
        $message = "",
        $code = 0,
        Throwable $previous = null,
        protected ?ResponseInterface $fullResponse = null,
        protected mixed $response = null,
    )
    {
        parent::__construct($message, $code, $previous);
    }

    /**
     * Get full response.
     *
     * @return ResponseInterface
     */
    public function getFullResponse(): ResponseInterface
    {
        return $this->fullResponse;
    }

    /**
     * Get response.
     *
     * @return mixed
     */
    public function getResponse(): mixed
    {
        return $this->response;
    }
}
