<?php
declare(strict_types=1);

namespace App\Modules\Core\Exceptions\Services\Network;

use Exception;

/**
 * RequestException.
 */
class RequestException extends Exception implements NetworkException
{
}
