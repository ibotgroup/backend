<?php
declare(strict_types=1);

namespace App\Modules\Core\Exceptions\Facades;

use App\Modules\Core\Exceptions\DB\TransactionNotStartedException as TransactionNotStartedBase;
use Exception;


/**
 * TransactionNotStarted.
 */
class TransactionNotStartedException extends Exception implements FacadesException, TransactionNotStartedBase
{

}
