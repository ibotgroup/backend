<?php
declare(strict_types=1);

namespace App\Modules\Core\Exceptions\Facades;

use App\Modules\Core\Exceptions\CoreException;

/**
 * FacadesException.
 */
interface FacadesException extends CoreException
{

}
