<?php
declare(strict_types=1);

namespace App\Modules\Core\Exceptions\Attributes\Controllers;

use App\Modules\Core\Exceptions\Attributes\AttributesException;

/**
 * ControllersException.
 */
interface ControllersException extends AttributesException
{
}
