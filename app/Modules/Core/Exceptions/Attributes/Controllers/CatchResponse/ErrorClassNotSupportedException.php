<?php
declare(strict_types=1);

namespace App\Modules\Core\Exceptions\Attributes\Controllers\CatchResponse;

use Exception;

/**
 * ErrorClassNotSupportedException.
 */
class ErrorClassNotSupportedException extends Exception implements CatchResponseException
{
}
