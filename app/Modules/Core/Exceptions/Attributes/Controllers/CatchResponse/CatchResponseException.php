<?php
declare(strict_types=1);

namespace App\Modules\Core\Exceptions\Attributes\Controllers\CatchResponse;

use App\Modules\Core\Exceptions\Attributes\Controllers\ControllersException;

/**
 * CatchResponseException.
 */
interface CatchResponseException extends ControllersException
{
}
