<?php
declare(strict_types=1);

namespace App\Modules\Core\Exceptions\Attributes;

use App\Modules\Core\Exceptions\CoreException;

/**
 * AttributesException.
 */
interface AttributesException extends CoreException
{
}
