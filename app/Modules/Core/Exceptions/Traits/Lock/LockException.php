<?php
declare(strict_types=1);

namespace App\Modules\Core\Exceptions\Traits\Lock;

use App\Modules\Core\Exceptions\Traits\TraitsException;

/**
 * LockException.
 */
interface LockException extends TraitsException
{
}
