<?php
declare(strict_types=1);

namespace App\Modules\Core\Exceptions\Traits\Controllers;

use App\Modules\Core\Exceptions\Traits\TraitsException;

/**
 * ControllersException.
 */
interface ControllersException extends TraitsException
{

}
