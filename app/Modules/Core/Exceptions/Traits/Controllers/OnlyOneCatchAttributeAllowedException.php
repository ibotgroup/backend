<?php
declare(strict_types=1);

namespace App\Modules\Core\Exceptions\Traits\Controllers;

use Exception;

/**
 * OnlyOneCatchAttributeAllowedException.
 */
class OnlyOneCatchAttributeAllowedException extends Exception implements ControllersException
{

}
