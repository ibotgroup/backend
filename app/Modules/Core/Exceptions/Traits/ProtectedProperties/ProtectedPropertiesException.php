<?php
declare(strict_types=1);

namespace App\Modules\Core\Exceptions\Traits\ProtectedProperties;


use App\Modules\Core\Exceptions\Traits\TraitsException;

/**
 * ProtectedPropertiesException.
 */
interface ProtectedPropertiesException extends TraitsException
{

}
