<?php
declare(strict_types=1);

namespace App\Modules\Core\Exceptions\Traits\ProtectedProperties;

/**
 * PropertyNotExistsException.
 */
class PropertyNotExistsException extends \Exception implements ProtectedPropertiesException
{

}
