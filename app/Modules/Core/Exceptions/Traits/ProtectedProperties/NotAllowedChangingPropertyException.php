<?php
declare(strict_types=1);

namespace App\Modules\Core\Exceptions\Traits\ProtectedProperties;

/**
 * NotAllowedChangingPropertyException.
 */
class NotAllowedChangingPropertyException extends \Exception implements ProtectedPropertiesException
{

}
