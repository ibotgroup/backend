<?php
declare(strict_types=1);

namespace App\Modules\Core\Exceptions\Traits;


use App\Modules\Core\Exceptions\CoreException;

/**
 * TraitsException.
 */
interface TraitsException extends CoreException
{

}
