<?php
declare(strict_types=1);

namespace App\Modules\Core\Exceptions\Models;

use App\Modules\Core\Exceptions\CoreException;

/**
 * ModelsException.
 */
interface ModelsException extends CoreException
{

}
