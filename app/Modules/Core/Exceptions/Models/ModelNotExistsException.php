<?php
declare(strict_types=1);

namespace App\Modules\Core\Exceptions\Models;

use Exception;

/**
 * ModelNotExistsException.
 */
class ModelNotExistsException extends Exception implements ModelsException
{
}
