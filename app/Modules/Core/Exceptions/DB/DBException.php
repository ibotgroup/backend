<?php
declare(strict_types=1);

namespace App\Modules\Core\Exceptions\DB;


use App\Modules\Core\Exceptions\CoreException;

/**
 * DBException.
 */
interface DBException extends CoreException
{

}
