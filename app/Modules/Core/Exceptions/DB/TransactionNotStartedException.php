<?php
declare(strict_types=1);

namespace App\Modules\Core\Exceptions\DB;

/**
 * TransactionNotStarted.
 */
interface TransactionNotStartedException extends DBException
{

}
