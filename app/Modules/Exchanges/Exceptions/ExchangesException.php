<?php
declare(strict_types=1);

namespace App\Modules\Exchanges\Exceptions;

use App\Modules\Exchanges\Contracts\ExchangesModuleContract;
use Throwable;

/**
 * ExchangesException.
 */
interface ExchangesException extends Throwable, ExchangesModuleContract
{
}
