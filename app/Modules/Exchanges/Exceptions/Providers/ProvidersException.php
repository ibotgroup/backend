<?php
declare(strict_types=1);

namespace App\Modules\Exchanges\Exceptions\Providers;

use App\Modules\Exchanges\Exceptions\ExchangesException;

/**
 * ProvidersException.
 */
interface ProvidersException extends ExchangesException
{
}
