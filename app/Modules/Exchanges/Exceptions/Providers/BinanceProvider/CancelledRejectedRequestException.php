<?php
declare(strict_types=1);

namespace App\Modules\Exchanges\Exceptions\Providers\BinanceProvider;

/**
 * CancelledRejectedRequestException.
 */
class CancelledRejectedRequestException extends BaseRequestException
{

}
