<?php
declare(strict_types=1);

namespace App\Modules\Exchanges\Exceptions\Providers\BinanceProvider;

/**
 * GeneralRequestException.
 */
class GeneralRequestException extends BaseRequestException
{

}
