<?php
declare(strict_types=1);

namespace App\Modules\Exchanges\Exceptions\Providers\BinanceProvider;

use App\Modules\Exchanges\Exceptions\Providers\ProvidersException;

/**
 * BinanceProviderException.
 */
interface BinanceProviderException extends ProvidersException
{
}
