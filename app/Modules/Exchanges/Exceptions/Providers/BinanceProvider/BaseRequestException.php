<?php
declare(strict_types=1);

namespace App\Modules\Exchanges\Exceptions\Providers\BinanceProvider;

use App\Modules\Core\Exceptions\Services\Network\BadRequestException;
use Throwable;

/**
 * Base request exception.
 */
abstract class BaseRequestException extends BadRequestException implements BinanceProviderException
{
    /**
     * Constructor.
     *
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     * @param mixed|null $response
     * @param int|null $errCode
     * @param string|null $errMsg
     */
    public function __construct(
        $message = "",
        $code = 0,
        Throwable $previous = null,
        mixed $response = null,
        protected ?int $errCode = null,
        protected ?string $errMsg = null
    )
    {
        parent::__construct($message, $code, $previous, $response);
    }

    /**
     * Get response error code.
     *
     * @return int|null
     */
    public function getErrCode(): ?int
    {
        return $this->errCode;
    }

    /**
     * Get message error code.
     *
     * @return string|null
     */
    public function getErrMsg(): ?string
    {
        return $this->errMsg;
    }
}
