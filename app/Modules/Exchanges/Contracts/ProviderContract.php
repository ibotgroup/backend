<?php
declare(strict_types=1);

namespace App\Modules\Exchanges\Contracts;

/**
 * ProviderContract.
 */
interface ProviderContract extends ExchangesModuleContract
{
}
