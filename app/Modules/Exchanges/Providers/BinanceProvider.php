<?php
declare(strict_types=1);

namespace App\Modules\Exchanges\Providers;

use App\Modules\Accesses\Dto\Api\Access\KeysDto;
use App\Modules\Bots\Enum\Trades\OrderTypeEnum;
use App\Modules\Bots\Enum\Trades\TimeInForceEnum;
use App\Modules\Core\Contracts\Services\ServiceContract;
use App\Modules\Core\Contracts\Jobs\WeightControlled;
use App\Modules\Core\Dto\Network\RequestDto;
use App\Modules\Core\Enum\Network\NetworkMethodEnum;
use App\Modules\Core\Exceptions\Services\Network\BadRequestException;
use App\Modules\Core\Exceptions\Services\Network\RequestException;
use App\Modules\Core\Services\NetworkService;
use App\Modules\Core\Traits\Tasks\WeightControl;
use App\Modules\Exchanges\Contracts\ExchangesModuleContract;
use App\Modules\Exchanges\Contracts\ProviderContract;
use App\Modules\Exchanges\Dto\Services\Providers\BinanceProvider\AccountDto;
use App\Modules\Exchanges\Enum\ProviderErrorEnum;
use App\Modules\Exchanges\Exceptions\Providers\BinanceProvider\CancelledRejectedRequestException;
use App\Modules\Exchanges\Exceptions\Providers\BinanceProvider\ErrorMsgReceivedRequestException;
use App\Modules\Exchanges\Exceptions\Providers\BinanceProvider\FilterFailureRequestException;
use App\Modules\Exchanges\Exceptions\Providers\BinanceProvider\GeneralRequestException;
use App\Modules\Exchanges\Exceptions\Providers\BinanceProvider\NewOrderRejectedRequestException;
use App\Modules\Exchanges\Exceptions\Providers\BinanceProvider\RequestIssueRequestException;
use App\Modules\Exchanges\Exceptions\Providers\BinanceProvider\UnknownRequestException;
use DateTime;
use Exception;
use Illuminate\Support\Collection;
use JetBrains\PhpStorm\ExpectedValues;
use Psr\Http\Message\ResponseInterface;
use stdClass;
use function app;
use function sprintf;

/**
 * BinanceService.
 */
final class BinanceProvider implements ServiceContract, ExchangesModuleContract, ProviderContract, WeightControlled
{
    use WeightControl;

    private const API_HOST = 'https://api.binance.com';

    public const WEIGHT_LIMIT_PER_MINUTE = 1200;

    /**
     * Constructor.
     *
     * @param KeysDto $keys
     */
    public function __construct(private KeysDto $keys)
    {
    }

    /**
     * Get server time.
     *
     * @return stdClass
     * @throws ErrorMsgReceivedRequestException
     * @throws GeneralRequestException
     * @throws RequestException
     * @throws RequestIssueRequestException
     * @throws UnknownRequestException
     */
    public function getServerTime(): StdClass
    {
        return $this->request('/api/v3/time', NetworkMethodEnum::METHOD_GET);
    }

    /**
     * Get exchange time.
     *
     * @return stdClass
     * @throws ErrorMsgReceivedRequestException
     * @throws GeneralRequestException
     * @throws RequestException
     * @throws RequestIssueRequestException
     * @throws UnknownRequestException
     */
    public function getExchangeInfo(): StdClass
    {
        return $this->request('/api/v3/exchangeInfo', NetworkMethodEnum::METHOD_GET);
    }

    /**
     * Get order book.
     *
     * @param string $symbol
     * @param int $limit
     * @return stdClass
     * @throws ErrorMsgReceivedRequestException
     * @throws GeneralRequestException
     * @throws RequestException
     * @throws RequestIssueRequestException
     * @throws UnknownRequestException
     */
    public function gerOrderBook(string $symbol, int $limit): StdClass
    {
        return $this->request('/api/v3/depth', NetworkMethodEnum::METHOD_GET, ['symbol' => $symbol, 'limit' => $limit]);
    }

    /**
     * New market order.
     *
     * @param string $symbol
     * @param string $side
     * @param float $quantity
     * @param bool $test
     * @return stdClass
     * @throws CancelledRejectedRequestException
     * @throws ErrorMsgReceivedRequestException
     * @throws FilterFailureRequestException
     * @throws GeneralRequestException
     * @throws NewOrderRejectedRequestException
     * @throws RequestException
     * @throws RequestIssueRequestException
     * @throws UnknownRequestException
     */
    public function newMarketOrder(string $symbol, string $side, float $quantity, bool $test = false): StdClass
    {
        $query = [
            'symbol' => $symbol,
            'side' => $side,
            'quantity' => $quantity,
            'type' => OrderTypeEnum::TYPE_MARKET_LABEL,
        ];

        return $this->newOrder($query, $test);
    }

    /**
     * New limit order.
     *
     * @param string $symbol
     * @param string $side
     * @param float $quantity
     * @param float $price
     * @param bool $test
     * @return stdClass
     * @throws CancelledRejectedRequestException
     * @throws ErrorMsgReceivedRequestException
     * @throws FilterFailureRequestException
     * @throws GeneralRequestException
     * @throws NewOrderRejectedRequestException
     * @throws RequestException
     * @throws RequestIssueRequestException
     * @throws UnknownRequestException
     */
    public function newLimitOrder(
        string $symbol,
        string $side,
        float $quantity,
        float $price,
        bool $test = false
    ): StdClass
    {
        $query = [
            'symbol' => $symbol,
            'side' => $side,
            'quantity' => $quantity,
            'price' => $price,
            'timeInForce' => TimeInForceEnum::GTC,
            'type' => OrderTypeEnum::TYPE_LIMIT_LABEL,
        ];

        return $this->newOrder($query, $test);
    }

    /**
     * Universal method for create new order.
     *
     * @param array $query
     * @param bool $test
     * @return stdClass
     * @throws CancelledRejectedRequestException
     * @throws ErrorMsgReceivedRequestException
     * @throws FilterFailureRequestException
     * @throws GeneralRequestException
     * @throws NewOrderRejectedRequestException
     * @throws RequestException
     * @throws RequestIssueRequestException
     * @throws UnknownRequestException
     */
    public function newOrder(array $query, bool $test = false): StdClass
    {
        $uri = '/api/v3/order';

        if ($test) {
            $uri = sprintf('%s/test', $uri);
        }

        $query['timestamp'] = time() * 1000;
        $query['signature'] = $this->sign($query);

        return $this->request($uri, NetworkMethodEnum::METHOD_POST, $query);
    }

    /**
     * Open orders.
     *
     * @param string $symbol
     * @return Collection
     * @throws ErrorMsgReceivedRequestException
     * @throws GeneralRequestException
     * @throws RequestException
     * @throws RequestIssueRequestException
     * @throws UnknownRequestException
     */
    public function openOrders(string $symbol): Collection
    {
        $query = [];
        $query['symbol'] = $symbol;
        $query['timestamp'] = time() * 1000;
        $query['signature'] = $this->sign($query);

        return $this->request('/api/v3/openOrders', NetworkMethodEnum::METHOD_GET, $query);
    }

    /**
     * Cancel order.
     *
     * @param string $symbol
     * @param int|null $orderId
     * @param string|null $origClientOrderId
     * @return stdClass
     * @throws CancelledRejectedRequestException
     * @throws ErrorMsgReceivedRequestException
     * @throws GeneralRequestException
     * @throws RequestException
     * @throws RequestIssueRequestException
     * @throws UnknownRequestException
     */
    public function cancelOrder(
        string $symbol,
        int $orderId = null,
        string $origClientOrderId = null
    ): StdClass
    {
        $query = [];
        $query['symbol'] = $symbol;

        if ($orderId !== null) {
            $query['orderId'] = $orderId;
        }

        if ($origClientOrderId !== null) {
            $query['origClientOrderId'] = $origClientOrderId;
        }

        $query['timestamp'] = time() * 1000;
        $query['signature'] = $this->sign($query);

        return $this->request('/api/v3/order', NetworkMethodEnum::METHOD_DELETE, $query);
    }

    /**
     * Query order.
     *
     * @param string $symbol
     * @param int|null $orderId
     * @param string|null $origClientOrderId
     * @return stdClass
     * @throws ErrorMsgReceivedRequestException
     * @throws GeneralRequestException
     * @throws RequestException
     * @throws RequestIssueRequestException
     * @throws UnknownRequestException
     */
    public function queryOrder(
        string $symbol,
        int $orderId = null,
        string $origClientOrderId = null
    ): stdClass
    {
        $query = [];
        $query['symbol'] = $symbol;

        if ($orderId !== null) {
            $query['orderId'] = $orderId;
        }

        if ($origClientOrderId !== null) {
            $query['origClientOrderId'] = $origClientOrderId;
        }

        $query['timestamp'] = time() * 1000;
        $query['signature'] = $this->sign($query);

        return $this->request('/api/v3/order', NetworkMethodEnum::METHOD_GET, $query);
    }

    /**
     * Get kline.
     *
     * @param string $symbol
     * @param string $interval
     * @param int $limit
     * @param int|null $startTime
     * @param int|null $endTime
     * @return Collection
     * @throws ErrorMsgReceivedRequestException
     * @throws GeneralRequestException
     * @throws RequestException
     * @throws RequestIssueRequestException
     * @throws UnknownRequestException
     */
    public function getKLines(string $symbol, string $interval, int $limit, int $startTime = null, int $endTime = null): Collection
    {
        $query = [];
        $query['symbol'] = $symbol;
        $query['interval'] = $interval;
        $query['limit'] = $limit;

        if ($startTime !== null) {
            $query['startTime'] = $startTime;
        }

        if ($endTime !== null) {
            $query['endTime'] = $endTime;
        }

        return $this->request('/api/v3/klines', NetworkMethodEnum::METHOD_GET, $query);
    }

    /**
     * Get ticker price.
     *
     * @param string|null $symbol
     * @return Collection
     * @throws ErrorMsgReceivedRequestException
     * @throws GeneralRequestException
     * @throws RequestException
     * @throws RequestIssueRequestException
     * @throws UnknownRequestException
     */
    public function getTickerPrice(string $symbol = null): Collection
    {
        $query = [];

        if ($symbol) {
            $query = ['symbol' => $symbol];
        }

        return $this->request('/api/v3/ticker/price', NetworkMethodEnum::METHOD_GET, $query);
    }

    /**
     * Get bool ticker.
     *
     * @param string|null $symbol
     * @return Collection
     * @throws ErrorMsgReceivedRequestException
     * @throws GeneralRequestException
     * @throws RequestException
     * @throws RequestIssueRequestException
     * @throws UnknownRequestException
     */
    public function getBoolTicker(string $symbol = null): Collection
    {
        $query = [];

        if ($symbol) {
            $query = ['symbol' => $symbol];
        }

        return $this->request('/api/v3/ticker/bookTicker', NetworkMethodEnum::METHOD_GET, $query);
    }

    /**
     * Get bool ticker.
     *
     * @return stdClass
     * @throws CancelledRejectedRequestException
     * @throws ErrorMsgReceivedRequestException
     * @throws FilterFailureRequestException
     * @throws GeneralRequestException
     * @throws NewOrderRejectedRequestException
     * @throws RequestException
     * @throws RequestIssueRequestException
     * @throws UnknownRequestException
     */
    public function ping(): stdClass
    {
        return $this->request('/api/v3/ping', NetworkMethodEnum::METHOD_GET);
    }

    /**
     * Get account info.
     *
     * @return AccountDto=
     * @throws ErrorMsgReceivedRequestException
     * @throws GeneralRequestException
     * @throws RequestException
     * @throws RequestIssueRequestException
     * @throws UnknownRequestException
     */
    public function account(): AccountDto
    {
        $query = [];
        $query['timestamp'] = time() * 1000;
        $query['signature'] = $this->sign($query);

        return new AccountDto(
            $this->request('/api/v3/account', NetworkMethodEnum::METHOD_GET, $query)
        );
    }

    /**
     * Sign request.
     *
     * @param array $query
     * @return string
     */
    private function sign(array $query = []): string
    {
        return hash_hmac('sha256', http_build_query($query), $this->keys->secret);
    }

    /**
     * Request.
     *
     * @param string $uri
     * @param string $method
     * @param array $query
     * @return stdClass|Collection
     * @throws CancelledRejectedRequestException
     * @throws ErrorMsgReceivedRequestException
     * @throws FilterFailureRequestException
     * @throws GeneralRequestException
     * @throws NewOrderRejectedRequestException
     * @throws RequestException
     * @throws RequestIssueRequestException
     * @throws UnknownRequestException
     * @throws Exception
     */
    private function request(
        string $uri,
        #[ExpectedValues(NetworkMethodEnum::METHODS)] string $method,
        array $query = []
    ): stdClass|Collection
    {
        try {
            $response = $this->getNetworkService()
                ->restRequest(
                    new RequestDto(
                        sprintf("%s%s", self::API_HOST, $uri),
                        $method,
                        $query,
                        [],
                        [
                            'X-MBX-APIKEY' => $this->keys->key,
                        ],
                        true,
                        true
                    )
                );

            $this->setWeights($response->response);

            return is_array($response->data) ? collect($response->data) : $response->data;
        } catch (BadRequestException $e) {
            $this->setWeights($e->getFullResponse());

            $this->processErrors($e);
        }

        return collect();
    }

    /**
     * Set left weight.
     *
     * @param ResponseInterface $response
     * @throws Exception
     */
    private function setWeights(ResponseInterface $response): void
    {
        $this->setLeftWeight(
            self::WEIGHT_LIMIT_PER_MINUTE - (int)$response
                ->getHeader('x-mbx-used-weight')[0],
            new DateTime(
                $response
                    ->getHeader('Date')[0]
            )
        );
    }

    /**
     * Process errors.
     *
     * @param BadRequestException $e
     * @throws CancelledRejectedRequestException
     * @throws ErrorMsgReceivedRequestException
     * @throws FilterFailureRequestException
     * @throws NewOrderRejectedRequestException
     * @throws UnknownRequestException
     * @throws GeneralRequestException
     * @throws RequestIssueRequestException
     */
    private function processErrors(BadRequestException $e): void
    {
        $errorResponse = $e->getResponse();

        if ($errorResponse instanceof stdClass &&
            isset($errorResponse->code) &&
            isset($errorResponse->msg) &&
            is_int($errorResponse->code)
        ) {
            $code = $errorResponse->code;
            $msg = $errorResponse->msg;

            if ($code === ProviderErrorEnum::BINANCE_CODE_ERROR_MSG_RECEIVED) {
                throw new ErrorMsgReceivedRequestException(previous: $e, errCode: $code, errMsg: $msg);
            } else if ($code === ProviderErrorEnum::BINANCE_CODE_FILTERS) {
                throw new FilterFailureRequestException(previous: $e, errCode: $code, errMsg: $msg);
            } else if ($code === ProviderErrorEnum::BINANCE_CODE_NEW_ORDER_REJECTED) {
                throw new NewOrderRejectedRequestException(previous: $e, errCode: $code, errMsg: $msg);
            } else if ($code === ProviderErrorEnum::BINANCE_CODE_CANCELLED_REJECTED) {
                throw new CancelledRejectedRequestException(previous: $e, errCode: $code, errMsg: $msg);
            } else if (str_starts_with((string)$code, '-10')) {
                throw new GeneralRequestException(previous: $e, errCode: $code, errMsg: $msg);
            } else if (str_starts_with((string)$code, '-11')) {
                throw new RequestIssueRequestException(previous: $e, errCode: $code, errMsg: $msg);
            }

            throw new UnknownRequestException(previous: $e, errCode: $code, errMsg: $msg);
        }

        throw new UnknownRequestException(
            previous: $e,
            errCode: ProviderErrorEnum::BINANCE_SYSTEM_UNKNOWN,
            errMsg: ProviderErrorEnum::BINANCE_SYSTEM_UNKNOWN_MSG
        );
    }

    /**
     * Get {@see NetworkService::class}.
     *
     * @return NetworkService
     */
    private function getNetworkService(): NetworkService
    {
        return app(NetworkService::class);
    }
}
