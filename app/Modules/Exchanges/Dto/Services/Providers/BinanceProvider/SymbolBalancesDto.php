<?php
declare(strict_types=1);

namespace App\Modules\Exchanges\Dto\Services\Providers\BinanceProvider;

use App\Modules\Core\Dto\BaseDto;
use App\Modules\Exchanges\Contracts\ExchangesModuleContract;

/**
 * Symbol balances dto.
 *
 * @property-read string $baseAsset
 * @property-read string $quoteAsset
 * @property-read string $symbol
 * @property-read float $baseFree
 * @property-read float $quoteFree
 */
final class SymbolBalancesDto extends BaseDto implements ExchangesModuleContract
{
    /**
     * Constructor.
     *
     * @param string $baseAsset
     * @param string $quoteAsset
     * @param string $symbol
     * @param float $baseFree
     * @param float $quoteFree
     */
    public function __construct(
        protected string $baseAsset,
        protected string $quoteAsset,
        protected string $symbol,
        protected float $baseFree,
        protected float $quoteFree,
    )
    {
    }
}
