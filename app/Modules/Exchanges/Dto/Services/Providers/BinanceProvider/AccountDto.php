<?php
declare(strict_types=1);

namespace App\Modules\Exchanges\Dto\Services\Providers\BinanceProvider;

use App\Modules\Core\Dto\BaseDto;
use App\Modules\Exchanges\Contracts\ExchangesModuleContract;
use stdClass;

/**
 * Account dto.
 *
 * @property-read stdClass $account
 */
final class AccountDto extends BaseDto implements ExchangesModuleContract
{
    /**
     * Constructor.
     *
     * @param stdClass $account
     */
    public function __construct(
        protected stdClass $account,
    )
    {
    }

    /**
     * Is allowed spot trading.
     *
     * @return bool
     */
    public function isAllowedSpotTrading(): bool
    {
        return $this->account->canTrade && in_array('SPOT', $this->account->permissions);
    }

    /**
     * Get symbol.
     *
     * @param string $symbol
     * @return SymbolBalancesDto|null
     */
    public function getSymbolBalances(string $symbol): ?SymbolBalancesDto
    {
        $quoteAsset = collect($this->account->balances)
            ->filter(static function (stdClass $item) use ($symbol) {
                return str_ends_with($symbol, $item->asset);
            })
            ->first();

        $base = rtrim($symbol, $quoteAsset->asset);

        $baseAsset = collect($this->account->balances)
            ->filter(static function (stdClass $item) use ($base) {
                return $item->asset === $base;
            })
            ->first();

        return $quoteAsset && $baseAsset ?
            new SymbolBalancesDto(
                $baseAsset->asset,
                $quoteAsset->asset,
                $symbol,
                (float)$baseAsset->free,
                (float)$quoteAsset->free,
            ) :
            null;
    }

    /**
     * Get free asset balance.
     *
     * @param string $asset
     * @return float|null
     */
    public function getFreeAssetBalance(string $asset): ?float
    {
        $assetItem = collect($this->account->balances)
            ->filter(fn(stdClass $item) => $item->asset === $asset)
            ->first();

        return $assetItem ? (float)$assetItem->free : null;
    }
}
