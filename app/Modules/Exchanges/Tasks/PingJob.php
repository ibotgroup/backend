<?php
declare(strict_types=1);

namespace App\Modules\Exchanges\Tasks;

use App\Modules\Core\Contracts\Jobs\WeightControlled;
use App\Modules\Core\Contracts\Services\DataUpdaterContract;
use App\Modules\Core\Exceptions\Services\Network\RequestException;
use App\Modules\Core\Jobs\AbstractJob;
use App\Modules\Core\Jobs\RateLimited;
use App\Modules\Core\Traits\Tasks\WeightControl;
use App\Modules\Exchanges\Services\BinanceService;
use App\Modules\Market\Contracts\MarketModuleContract;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Cache\LockTimeoutException;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\Middleware\WithoutOverlapping;
use Illuminate\Queue\SerializesModels;
use function app;

/**
 * PricesUpdaterJob.
 */
final class PingJob extends AbstractJob implements WeightControlled, MarketModuleContract, DataUpdaterContract, ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, WeightControl;

    /**
     * Update prices in database from system binance api.
     *
     * @throws RequestException
     * @throws LockTimeoutException
     */
    public function handle(): void
    {
        $this->info();

        $binance = $this->getSystemBinance()
            ->getSystemBinance();

        try {
            $binance->ping();
        } finally {
            $this->setLeftWeight(
                $binance->leftWeight(),
                $binance->leftWeightAt(),
            );
        }
    }

    /**
     * {@inheritDoc}
     */
    public function getId(): string
    {
        return 'exchanges:binance-ping';
    }

    /**
     * {@inheritDoc}
     */
    public function getWeight(): int
    {
        return 1;
    }

    /**
     * Get the middleware the job should pass through.
     *
     * @return array
     */
    public function middleware(): array
    {
        return [
            (new WithoutOverlapping($this->getId()))
                ->expireAfter(300)
                ->dontRelease(),
            new RateLimited($this->getId(), 60, true),
        ];
    }

    /**
     * Get {@see BinanceService::class}.
     *
     * @return BinanceService
     */
    private function getSystemBinance(): BinanceService
    {
        return app(BinanceService::class);
    }
}
