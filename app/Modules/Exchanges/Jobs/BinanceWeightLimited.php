<?php
declare(strict_types=1);

namespace App\Modules\Exchanges\Jobs;

use App\Modules\Core\Traits\Tasks\WeightControl;
use App\Modules\Exchanges\Tasks\PingJob;
use Illuminate\Contracts\Cache\LockTimeoutException;

/**
 * Rate limited.
 */
final class BinanceWeightLimited
{
    use WeightControl;

    /**
     * Constructor.
     *
     * @param int $weight
     */
    public function __construct(private int $weight)
    {
    }

    /**
     * Handle.
     *
     * @param mixed $job
     * @param callable $next
     */
    public function handle(mixed $job, callable $next)
    {
        try {
            if ($this->checkAndIncrementLeftWeight($this->weight)) {
                $next($job);
            } else {
                PingJob::dispatch();
                $job->release(60);
            }
        } catch (LockTimeoutException) {
            $job->release(60);
        }
    }
}
