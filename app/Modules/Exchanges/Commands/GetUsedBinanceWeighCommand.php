<?php
declare(strict_types=1);

namespace App\Modules\Exchanges\Commands;

use App\Modules\Core\Traits\Tasks\WeightControl;
use Illuminate\Console\Command;

/**
 * GetUsedBinanceWeighCommand.
 */
final class GetUsedBinanceWeighCommand extends Command
{
    use WeightControl;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'exchanges:get-used-binance-weight';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get used binance weight';

    /**
     * Handle.
     *
     * @return int
     */
    public function handle(): int
    {
        $this->info(sprintf('Binance weight left: %s', $this->getIncrementedLeftWeight()));

        return 0;
    }
}
