<?php
declare(strict_types=1);

namespace App\Modules\Exchanges\Commands;

use App\Modules\Core\Traits\Tasks\WeightControl;
use Illuminate\Console\Command;
use Illuminate\Contracts\Cache\LockTimeoutException;

/**
 * SetNullBinanceWeighCommand.
 */
final class SetNullBinanceWeighCommand extends Command
{
    use WeightControl;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'exchanges:set-null-binance-weight';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Set null binance weight';

    /**
     * Handle.
     *
     * @return int
     * @throws LockTimeoutException
     */
    public function handle(): int
    {
        $this->putLeftWeigh(null);
        $this->info(sprintf('Left binance weight: %s', $this->getIncrementedLeftWeight()));

        return 0;
    }
}
