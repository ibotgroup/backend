<?php
declare(strict_types=1);

namespace App\Modules\Exchanges\Enum;

use App\Modules\Core\Enum\EnumContract;
use App\Modules\Exchanges\Contracts\ExchangesModuleContract;

/**
 * ProviderEnum.
 */
final class ProviderEnum implements EnumContract, ExchangesModuleContract
{
    public const BINANCE_SPOT = 1;
}
