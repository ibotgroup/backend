<?php
declare(strict_types=1);

namespace App\Modules\Exchanges\Enum;

use App\Modules\Core\Enum\EnumContract;
use App\Modules\Exchanges\Contracts\ExchangesModuleContract;

/**
 * ProviderCommissionEnum.
 */
final class ProviderCommissionEnum implements EnumContract, ExchangesModuleContract
{
    public const BINANCE_SPOT = 0.1;
    public const BINANCE_ADJUST = 0.9;
}
