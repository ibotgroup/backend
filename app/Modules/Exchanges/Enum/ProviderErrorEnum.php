<?php
declare(strict_types=1);

namespace App\Modules\Exchanges\Enum;

use App\Modules\Core\Enum\EnumContract;
use App\Modules\Exchanges\Contracts\ExchangesModuleContract;

/**
 * Provider error enum.
 */
class ProviderErrorEnum implements EnumContract, ExchangesModuleContract
{
    public const BINANCE_SYSTEM_UNKNOWN = -1;
    public const BINANCE_CODE_ERROR_MSG_RECEIVED = -1010;
    public const BINANCE_CODE_NEW_ORDER_REJECTED = -2010;
    public const BINANCE_CODE_CANCELLED_REJECTED = -2011;
    public const BINANCE_CODE_FILTERS = -1013;

    public const BINANCE_UNKNOWN_ORDER_SENT = "Unknown order sent.";
    public const BINANCE_DUPLICATE_ORDER_SENT = "Duplicate order sent.";
    public const BINANCE_MARKET_IS_CLOSED = "Market is closed.";
    public const BINANCE_ACCOUNT_HAS_INSUFFICIENT_BALANCE = "Account has insufficient balance for requested action.";
    public const BINANCE_MARKET_ORDERS_NOT_SUPPORTED_SYMBOL = "Market orders are not supported for this symbol.";
    public const BINANCE_ICEBERG_ORDERS_SUPPORTED_SYMBOL = "Iceberg orders are not supported for this symbol.";
    public const BINANCE_STOP_LOSS_NOT_SUPPORTED_SYMBOL = "Stop loss orders are not supported for this symbol.";
    public const BINANCE_STOP_LOSS_LIMIT_NOT_SUPPORTED_SYMBOL = "Stop loss limit orders are not supported for this symbol.";
    public const BINANCE_TAKE_PROFIT_ORDERS_NOT_SUPPORTED_SYMBOL = "Take profit orders are not supported for this symbol.";
    public const BINANCE_TAKE_PROFIT_LIMIT_SUPPORTED_SYMBOL = "Take profit limit orders are not supported for this symbol.";
    public const BINANCE_PRICE_QTY_ZERO_LESS = "Price * QTY is zero or less.";
    public const BINANCE_ICEBERG_EXCEEDS_QTY = "IcebergQty exceeds QTY.";
    public const BINANCE_ACTION_DISABLED_ON_THIS_ACCOUNT = "This action disabled is on this account.";
    public const BINANCE_UNSUPPORTED_ORDER_COMBINATION = "Unsupported order combination";
    public const BINANCE_ORDER_WOULD_TRIGGER_IMMEDIATELY = "Order would trigger immediately.";
    public const BINANCE_CANCEL_ORDER_INVALID_CHECK_ORDER_Id = "Cancel order is invalid. Check origClOrdId and orderId.";
    public const BINANCE_ORDER_WOULD_IMMEDIATELY_MATCH_AND_TAKE = "Order would immediately match and take.";
    public const BINANCE_THE_RELATIONSHIP_PRICES_ORDERS_NOT_CORRECT = "The relationship of the prices for the orders is not correct.";
    public const BINANCE_OCO_ORDER_NOT_SUPPORTED_SYMBOL = "OCO orders are not supported for this symbol";
    public const BINANCE_QUOTE_ORDER_QTY_MARKET_ORDERS_NOT_SUPPORTED_SYMBOL = "Quote order qty market orders are not support for this symbol.";

    public const BINANCE_SYSTEM_UNKNOWN_MSG = 'An unknown error occured while processing the request. System exception.';
}
