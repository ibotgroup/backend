<?php
declare(strict_types=1);

namespace App\Modules\Exchanges\Enum;

use App\Modules\Core\Enum\EnumContract;
use App\Modules\Exchanges\Contracts\ExchangesModuleContract;

/**
 * ProviderStatusEnum.
 */
final class ProviderStatusEnum implements EnumContract, ExchangesModuleContract
{
    public const BINANCE_STATUS_NEW = 'NEW';
    public const BINANCE_STATUS_PARTIALLY_FILLED = 'PARTIALLY_FILLED';
    public const BINANCE_STATUS_FILLED = 'FILLED';
    public const BINANCE_STATUS_CANCELED = 'CANCELED';
    public const BINANCE_STATUS_PENDING_CANCEL = 'PENDING_CANCEL';
    public const BINANCE_STATUS_REJECTED = 'REJECTED';
    public const BINANCE_STATUS_EXPIRED = 'EXPIRED';
}
