<?php
declare(strict_types=1);

namespace App\Modules\Exchanges\Services;

use App\Modules\Accesses\Dto\Api\Access\KeysDto;
use App\Modules\Accesses\Models\Access;
use App\Modules\Exchanges\Contracts\ExchangesModuleContract;
use App\Modules\Exchanges\Providers\BinanceProvider;
use JetBrains\PhpStorm\Pure;

/**
 * System binance.
 */
final class BinanceService implements ExchangesModuleContract
{
    /**
     * Get binance by access.
     *
     * @param Access $access
     * @return BinanceProvider
     */
    #[Pure]
    public function getBinanceByAccess(Access $access): BinanceProvider
    {
        return new BinanceProvider(
            new KeysDto(
                $access->key,
                $access->secret,
            )
        );
    }

    /**
     * Get system binance.
     *
     * @return BinanceProvider
     */
    public function getSystemBinance(): BinanceProvider
    {
        return new BinanceProvider(
            new KeysDto(
                config('exchanges.system.binance.key'),
                config('exchanges.system.binance.secret'),
            )
        );
    }
}
