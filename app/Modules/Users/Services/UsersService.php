<?php
declare(strict_types=1);

namespace App\Modules\Users\Services;

use App\Modules\Core\Contracts\Services\ServiceContract;
use App\Modules\Users\Contracts\UsersModuleContract;
use App\Modules\Users\Models\User;

/**
 * User service.
 */
final class UsersService implements ServiceContract, UsersModuleContract
{
    /**
     * Create new user.
     *
     * @param string $name
     * @param string $email
     * @param string $password
     * @return User
     */
    public function create(string $name, string $email, string $password): User
    {
        $user = new User();
        $user->name = $name;
        $user->email = $email;
        $user->password = bcrypt($password);

        $user->save();

        return $user;
    }

    /**
     * Get count users.
     *
     * @return int
     */
    public function getCountUsers(): int
    {
        return User::query()
            ->count();
    }
}
