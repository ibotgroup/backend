<?php
declare(strict_types=1);

namespace App\Modules\Accesses\Services;

use App\Modules\Accesses\Contracts\AccessesModuleContract;
use App\Modules\Accesses\Models\Access;
use App\Modules\Accesses\Repositories\AccessesRepository;
use App\Modules\Core\Contracts\Services\ServiceContract;
use App\Modules\Users\Models\User;
use Illuminate\Support\Collection;

/**
 * Accesses service.
 */
final class AccessesService implements ServiceContract, AccessesModuleContract
{
    /**
     * Get list.
     *
     * @param User $user
     * @return Collection
     */
    public function list(User $user): Collection
    {
        return $this->listModels($user)
            ->map(fn(Access $access) => $access->toApi());
    }

    /**
     * Get models list.
     *
     * @param User $user
     * @return Collection
     */
    public function listModels(User $user): Collection
    {
        return $this->getAccessRepository()
            ->getList($user->id);
    }


    /**
     * Add access.
     *
     * @param User $user
     * @param string $title
     * @param int $exchange
     * @param string $key
     * @param string $secret
     * @param int $maxActiveBots
     * @return Access
     */
    public function add(
        User $user,
        string $title,
        int $exchange,
        string $key,
        string $secret,
        int $maxActiveBots,
    ): Access
    {
        $access = new Access();
        $access->user()
            ->associate($user);
        $access->title = $title;
        $access->exchange = $exchange;
        $access->key = $key;
        $access->secret = $secret;
        $access->max_active_bots = $maxActiveBots;

        $access->save();

        return $access;
    }

    /**
     * Delete access.
     *
     * @param Access $access
     */
    public function delete(Access $access): void
    {
        $access->delete();
    }

    /**
     * Check exists repository.
     *
     * @param int $exchange
     * @param string $key
     * @return bool
     */
    public function checkExistsAccess(
        int $exchange,
        string $key
    ): bool
    {
        return $this->getAccessRepository()
            ->checkExistsAccess($exchange, $key);
    }

    /**
     * Find by sid.
     *
     * @param string $sid
     * @return Access|null
     */
    public function findBySid(string $sid): ?Access
    {
        return $this->getAccessRepository()
            ->findBySid($sid);
    }

    /**
     * Get {@see AccessesRepository::class}.
     *
     * @return AccessesRepository
     */
    private function getAccessRepository(): AccessesRepository
    {
        return app(AccessesRepository::class);
    }
}
