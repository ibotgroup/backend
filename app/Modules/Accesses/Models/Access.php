<?php
declare(strict_types=1);

namespace App\Modules\Accesses\Models;

use App\Modules\Accesses\Contracts\AccessesModuleContract;
use App\Modules\Accesses\Dto\Api\Access\AccessDto;
use App\Modules\Accesses\Dto\Api\Access\KeysDto;
use App\Modules\Core\Contracts\Models\Api;
use App\Modules\Core\Contracts\Models\ApiOption;
use App\Modules\Core\Dto\Api\ApiOptionDto;
use App\Modules\Core\Exceptions\Models\ModelNotExistsException;
use App\Modules\Core\Models\Model;
use App\Modules\Users\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use JetBrains\PhpStorm\Pure;

/**
 * Access model.
 *
 * @property-read int $id
 * @property-read int $sid
 * @property-read int $users_id
 * @property string $title
 * @property int $exchange
 * @property string $key
 * @property string $secret
 * @property integer $max_active_bots
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read User $user
 * @property-read string|int|null $exchangeText
 */
final class Access extends Model implements AccessesModuleContract, ApiOption, Api
{
    const STATUS_INACTIVE = 1;
    const STATUS_ACTIVE = 2;
    const STATUS_DELETED = 3;

    const EXCHANGE_BINANCE = 1;

    const EXCHANGES = [
        self::EXCHANGE_BINANCE,
    ];

    const EXCHANGES_TEXT = [
        self::EXCHANGE_BINANCE => 'Binance',
    ];

    const MAX_ACTIVE_BOTS = 20;

    /** {@inheritdoc} */
    protected $table = 'accesses';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'sid' => 'integer',
        'users_id' => 'integer',
        'title' => 'string',
        'exchange' => 'integer',
        'key' => 'string',
        'secret' => 'encrypted',
        'max_active_bots' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    /**
     * {@see User::class}.
     *
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'users_id');
    }

    /**
     * Get "exchangeText" аттрибут.
     *
     * @return string|int|null
     */
    public function getExchangeTextAttribute(): string|int|null
    {
        return self::EXCHANGES_TEXT[$this->exchange] ?? $this->exchange;
    }

    /**
     * To api dto.
     *
     * @return AccessDto
     * @throws ModelNotExistsException
     */
    public function toApi(): AccessDto
    {
        $this->throwIfNotExists();

        return new AccessDto(
            (string)$this->sid,
            $this->title,
            $this->exchange,
            $this->exchangeText,
            $this->key,
            $this->max_active_bots,
            $this->created_at?->format('Y-m-d H:i:s')
        );
    }

    /**
     * To api option.
     *
     * @return ApiOptionDto
     * @throws ModelNotExistsException
     */
    public function toApiOption(): ApiOptionDto
    {
        $this->throwIfNotExists();

        return new ApiOptionDto(
            (string)$this->sid ?? "",
            $this->title ?? "",
        );
    }

    /**
     * To keys dto.
     *
     * @return KeysDto
     * @throws ModelNotExistsException
     */
    public function toKeysDto(): KeysDto
    {
        $this->throwIfNotExists();

        return new KeysDto(
            $this->key,
            $this->secret,
        );
    }

    /**
     * Is owner.
     *
     * @param User $user
     * @return bool
     */
    public function isOwner(User $user): bool
    {
        return $this->users_id === $user->id;
    }

    /**
     * Set some events.
     *
     * @return void
     */
    protected static function booted(): void
    {
        static::creating(function (self $model) {
            $model->sid = rndSid();
        });
    }
}
