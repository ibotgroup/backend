<?php
declare(strict_types=1);

namespace App\Modules\Accesses\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Accesses\Contracts\AccessesModuleContract;
use App\Modules\Accesses\Models\Access;
use App\Modules\Accesses\Requests\AddRequest;
use App\Modules\Accesses\Requests\DeleteRequest;
use App\Modules\Accesses\Requests\ListRequest;
use App\Modules\Accesses\Services\AccessesService;
use App\Modules\Core\Attributes\Controllers\CatchResponse;
use App\Modules\Core\Attributes\Controllers\Transactional;
use App\Modules\Core\Dto\ResponseDto;
use App\Modules\Core\Exceptions\Traits\Lock\TransactionNotStartedException;

/**
 * Accesses controller.
 */
final class AccessesController extends Controller implements AccessesModuleContract
{
    /**
     * List access.
     *
     * @param ListRequest $request
     * @return ResponseDto
     */
    #[CatchResponse]
    public function list(ListRequest $request): ResponseDto
    {
        return new ResponseDto(
            $this->getAccessesService()
                ->list(
                    ...$request->validated()
                )
        );
    }

    /**
     * Add new api access.
     *
     * @param AddRequest $request
     * @return ResponseDto
     */
    #[Transactional]
    #[CatchResponse]
    public function add(AddRequest $request): ResponseDto
    {
        $this->getAccessesService()
            ->add(
                ...$request->validated()
            );

        return new ResponseDto();
    }

    /**
     * Delete api access.
     *
     * @param Access $access
     * @return ResponseDto
     * @throws TransactionNotStartedException
     */
    #[Transactional]
    #[CatchResponse]
    public function delete(Access $access): ResponseDto
    {
        Access::refreshAndLockForUpdate($access);
        app(DeleteRequest::class);

        $this->getAccessesService()
            ->delete($access);

        return new ResponseDto();
    }

    /**
     * Get {@see AccessesService::class}.
     *
     * @return AccessesService
     */
    private function getAccessesService(): AccessesService
    {
        return app(AccessesService::class);
    }
}
