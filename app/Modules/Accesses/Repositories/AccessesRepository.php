<?php
declare(strict_types=1);

namespace App\Modules\Accesses\Repositories;

use App\Modules\Accesses\Contracts\AccessesModuleContract;
use App\Modules\Accesses\Models\Access;
use App\Modules\Core\Contracts\Repositories\RepositoryContract;
use App\Modules\Core\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Collection;

/**
 * Access repository.
 */
final class AccessesRepository extends BaseRepository implements AccessesModuleContract, RepositoryContract
{
    /**
     * Constructor.
     *
     * @param Access $model
     */
    public function __construct(protected Access $model)
    {
    }

    /**
     * Get list access.
     *
     * @param int $userId
     * @return Collection
     */
    public function getList(
        int $userId,
    ): Collection
    {
        return $this->model
            ->newQuery()
            ->where('users_id', $userId)
            ->orderBy('created_at')
            ->get();
    }

    /**
     * Check exists access.
     *
     * @param int $exchange
     * @param string $key
     * @return bool
     */
    public function checkExistsAccess(
        int $exchange,
        string $key
    ): bool
    {
        return $this->model
            ->newQuery()
            ->where('exchange', $exchange)
            ->where('key', $key)
            ->exists();
    }
}
