<?php
declare(strict_types=1);

namespace App\Modules\Accesses\Dto\Api\Access;

use App\Modules\Bots\Contracts\BotsModuleContract;
use App\Modules\Core\Dto\BaseDto;

/**
 * Access dto.
 *
 * @property-read string $key
 * @property-read string $secret
 */
class KeysDto extends BaseDto implements BotsModuleContract
{
    /**
     * Constructor.
     *
     * @param string $key
     * @param string $secret
     */
    public function __construct(
        protected string $key,
        protected string $secret,
    ) {
    }
}
