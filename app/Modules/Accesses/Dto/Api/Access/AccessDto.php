<?php
declare(strict_types=1);

namespace App\Modules\Accesses\Dto\Api\Access;

use App\Modules\Bots\Contracts\BotsModuleContract;
use App\Modules\Core\Dto\BaseDto;

/**
 * Access dto.
 *
 * @property-read int|string $id
 * @property-read string|null $title
 * @property-read int|null $exchange
 * @property-read string|null $exchangeText
 * @property-read string|null $key
 * @property-read int|null $maxActiveBots
 * @property-read string|null $createdAt
 */
class AccessDto extends BaseDto implements BotsModuleContract
{
    /**
     * Constructor.
     *
     * @param string|null $id
     * @param string|null $title
     * @param int|null $exchange
     * @param string|null $exchangeText
     * @param string|null $key
     * @param int|null $maxActiveBots
     * @param string|null $createdAt
     */
    public function __construct(
        protected ?string $id,
        protected ?string $title,
        protected ?int $exchange,
        protected ?string $exchangeText,
        protected ?string $key,
        protected ?int $maxActiveBots,
        protected ?string $createdAt,
    ) {
    }
}
