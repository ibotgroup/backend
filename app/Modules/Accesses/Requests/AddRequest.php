<?php
declare(strict_types=1);

namespace App\Modules\Accesses\Requests;

use App\Modules\Accesses\Contracts\AccessesModuleContract;
use App\Modules\Accesses\Dto\Api\Access\KeysDto;
use App\Modules\Accesses\Models\Access;
use App\Modules\Accesses\Services\AccessesService;
use App\Modules\Core\Exceptions\Services\Network\RequestException;
use App\Modules\Exchanges\Exceptions\Providers\BinanceProvider\BinanceProviderException;
use App\Modules\Exchanges\Providers\BinanceProvider;
use App\Modules\Exchanges\Services\BinanceService;
use App\Modules\Users\Models\User;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use JetBrains\PhpStorm\ArrayShape;
use function sprintf;

/**
 * Add access request.
 */
final class AddRequest extends FormRequest implements AccessesModuleContract
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    #[ArrayShape([
        'title' => "string",
        'exchange' => "array",
        'key' => "string",
        'secret' => "string",
        'maxActiveBots' => "string",
    ])]
    public function rules(): array
    {
        return [
            'title' => 'required|max:255',
            'exchange' => [
                'required',
                Rule::in(Access::EXCHANGES)
            ],
            'key' => 'required|max:255',
            'secret' => 'required|max:255',
            'maxActiveBots' => sprintf('required|integer|min:1|max:%s', Access::MAX_ACTIVE_BOTS),
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function validated(): array
    {
        $data = parent::validated();
        $data['user'] = User::query()->find(1);
        $data['exchange'] = (int)$data['exchange'];
        $data['maxActiveBots'] = (int)$data['maxActiveBots'];

        return $data;
    }

    /**
     * {@inheritDoc}
     */
    protected function getValidatorInstance(): Validator
    {
        return parent::getValidatorInstance()
            ->after(function (Validator $validator) {
                if (count($validator->failed()) === 0) {
                    $this->after($validator);
                }
            });
    }

    /**
     * Post validation method.
     *
     * @param Validator $validator
     * @return void
     * @throws RequestException
     */
    public function after(Validator $validator): void
    {
        if ($this->getAccessesService()
            ->checkExistsAccess($this['exchange'], $this['key'])
        ) {
            $validator->errors()->add('key', 'This key already added.');
        } else {
            $this->isAllowedSpotTrading($validator);
        }
    }

    /**
     * Check api.
     *
     * @param Validator $validator
     * @return void
     * @throws RequestException
     */
    private function isAllowedSpotTrading(Validator $validator): void
    {
        $binance = new BinanceProvider(
            new KeysDto(
                $this['key'],
                $this['secret']
            )
        );

        try {
            if (!$binance->account()
                ->isAllowedSpotTrading()
            ) {
                $validator->errors()
                    ->add('key', 'Required allow trading via API.');
            }
        } catch (BinanceProviderException) {
            $message = 'Incorrect API key or secret.';

            $validator->errors()
                ->add('key', $message);
            $validator->errors()
                ->add('secret', $message);
        }
    }

    /**
     * Get {@see AccessesService::class}.
     *
     * @return AccessesService
     */
    private function getAccessesService(): AccessesService
    {
        return app(AccessesService::class);
    }

    /**
     * Get {@see BinanceService::class}.
     *
     * @return BinanceService
     */
    private function getBinanceService(): BinanceService
    {
        return app(BinanceService::class);
    }
}
