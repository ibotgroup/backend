<?php
declare(strict_types=1);

namespace App\Modules\Accesses\Requests;

use App\Modules\Accesses\Contracts\AccessesModuleContract;
use App\Modules\Accesses\Models\Access;
use App\Modules\Users\Models\User;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Delete access request.
 *
 * @property-read Access|mixed $access
 */
final class DeleteRequest extends FormRequest implements AccessesModuleContract
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return $this->access instanceof Access &&
            $this->access->isOwner(User::query()->find(1));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [];
    }
}
