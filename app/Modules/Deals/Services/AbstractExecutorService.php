<?php
declare(strict_types=1);

namespace App\Modules\Deals\Services;

use App\Modules\Bots\Dto\Services\DealsExecutor\OrderProcessedDto;
use App\Modules\Bots\Dto\Services\DealsExecutor\OrdersProcessedDto;
use App\Modules\Bots\Models\Bot;
use App\Modules\Core\Contracts\Services\ServiceContract;
use App\Modules\Deals\Contracts\DealsModuleContract;
use App\Modules\Deals\Events\OrderNotAcceptedEvent;
use App\Modules\Deals\Events\OrdersProcessedEvent;
use App\Modules\Deals\Models\Deal;
use App\Modules\Deals\Models\DealError;
use App\Modules\Exchanges\Enum\ProviderStatusEnum;
use App\Modules\Exchanges\Exceptions\Providers\BinanceProvider\BaseRequestException;
use App\Modules\Exchanges\Exceptions\Providers\BinanceProvider\FilterFailureRequestException;
use App\Modules\Exchanges\Exceptions\Providers\BinanceProvider\NewOrderRejectedRequestException;
use DateTime;
use stdClass;

/**
 * Abstract executor service.
 */
abstract class AbstractExecutorService implements DealsModuleContract, ServiceContract
{
    /**
     * Binance order process response.
     *
     * @param Deal $deal
     * @param stdClass $response
     * @return OrderProcessedDto
     */
    protected function binanceOrderProcessResponse(Deal $deal, stdClass $response): OrderProcessedDto
    {
        $orderProcessed = null;
        $dt = $this->getDateTimeByResponse($response);

        $deal->refresh();

        if (isset($response->transactTime)) {
            $deal->binance_order_id = $response->orderId;
            $deal->binance_client_order_id = $response->clientOrderId;
        }

        if (in_array($response->status, [ProviderStatusEnum::BINANCE_STATUS_NEW, ProviderStatusEnum::BINANCE_STATUS_PARTIALLY_FILLED])) {
            $orderProcessed = new OrderProcessedDto(deal: $deal, set: true);
            $deal->status = Deal::STATUS_SET;
            $deal->set_at = $dt;
        } else if (in_array($response->status, [ProviderStatusEnum::BINANCE_STATUS_FILLED])) {
            $orderProcessed = new OrderProcessedDto(deal: $deal, triggered: true);
            $deal->status = Deal::STATUS_TRIGGERED;
            $deal->triggered_at = $dt;
        } else if (in_array($response->status, [ProviderStatusEnum::BINANCE_STATUS_CANCELED, ProviderStatusEnum::BINANCE_STATUS_PENDING_CANCEL])) {
            if ($deal->cancel) {
                $orderProcessed = new OrderProcessedDto(deal: $deal, cancel: true);
            } else {
                $orderProcessed = new OrderProcessedDto(deal: $deal, cancelled: true);
            }

            $deal->status = Deal::STATUS_CANCELLED;
            $deal->cancelled_at = $dt;
            $deal->cancel = false;
        } else if (in_array($response->status, [ProviderStatusEnum::BINANCE_STATUS_REJECTED, ProviderStatusEnum::BINANCE_STATUS_EXPIRED])) {
            $orderProcessed = new OrderProcessedDto(deal: $deal, failed: true);
            $deal->status = Deal::STATUS_FAILED;
            $deal->failed_at = $dt;
        }

        $deal->save();

        return $orderProcessed;
    }

    /**
     * Create deal error.
     *
     * @param BaseRequestException $e
     * @param Deal $deal
     * @param bool $open
     * @return void
     */
    protected function createDealError(BaseRequestException $e, Deal $deal, bool $open): void
    {
        $dealError = new DealError();
        $dealError->code = $e->getErrCode();
        $dealError->message = $e->getErrMsg();
        $dealError->deal()
            ->associate($deal);
        $dealError->save();

        $deal->status = Deal::STATUS_ERROR;
        $deal->save();

        if ($e instanceof FilterFailureRequestException ||
            $e instanceof NewOrderRejectedRequestException
        ) {
            event(new OrderNotAcceptedEvent($deal, $dealError));
        }
    }

    /**
     * Send events;
     *
     * @param Bot $bot
     * @param OrdersProcessedDto $ordersProcessed
     * @param bool $afterExecution
     */
    protected function sendEvents(Bot $bot, OrdersProcessedDto $ordersProcessed, bool $afterExecution)
    {
        $triggered = $ordersProcessed->triggered();
        $cancelled = $ordersProcessed->cancelled();
        $failed = $ordersProcessed->failed();

        if ($triggered->list->count() > 0 ||
            $cancelled->list->count() > 0 ||
            $failed->list->count() > 0 ||
            $bot->botIteration->processed_after_monitoring
        ) {
            event(new OrdersProcessedEvent($bot, $ordersProcessed, true));
        } else if ($afterExecution) {
            event(new OrdersProcessedEvent($bot, $ordersProcessed, false));
        }
    }

    /**
     * Get time by response.
     *
     * @param stdClass $response
     * @return DateTime
     */
    private function getDateTimeByResponse(stdClass $response): DateTime
    {
        if (isset($response->transactTime)) {
            $dt = (new DateTime())
                ->setTimestamp((int)($response->transactTime / 1000));
        } else if (isset($response->updateTime)) {
            $dt = (new DateTime())
                ->setTimestamp((int)($response->updateTime / 1000));
        } else {
            $dt = new DateTime();
        }

        return $dt;
    }
}
