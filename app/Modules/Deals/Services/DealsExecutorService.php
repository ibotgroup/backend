<?php
declare(strict_types=1);

namespace App\Modules\Deals\Services;

use App\Modules\Bots\Dto\Services\DealsExecutor\OrderProcessedDto;
use App\Modules\Bots\Dto\Services\DealsExecutor\OrdersProcessedDto;
use App\Modules\Bots\Models\Bot;
use App\Modules\Bots\Services\BotsService;
use App\Modules\Core\Contracts\Jobs\WeightControlled;
use App\Modules\Core\Exceptions\Services\Network\RequestException;
use App\Modules\Core\Exceptions\Traits\Lock\TransactionNotStartedException;
use App\Modules\Core\Facades\DB;
use App\Modules\Core\Traits\Tasks\WeightControl;
use App\Modules\Deals\Events\OrdersExceptionEvent;
use App\Modules\Deals\Models\Deal;
use App\Modules\Deals\Repositories\DealsRepository;
use App\Modules\Deals\Tasks\DealsExecutorJob;
use App\Modules\Exchanges\Exceptions\Providers\BinanceProvider\BaseRequestException;
use App\Modules\Exchanges\Exceptions\Providers\BinanceProvider\CancelledRejectedRequestException;
use App\Modules\Exchanges\Exceptions\Providers\BinanceProvider\ErrorMsgReceivedRequestException;
use App\Modules\Exchanges\Exceptions\Providers\BinanceProvider\FilterFailureRequestException;
use App\Modules\Exchanges\Exceptions\Providers\BinanceProvider\GeneralRequestException;
use App\Modules\Exchanges\Exceptions\Providers\BinanceProvider\NewOrderRejectedRequestException;
use App\Modules\Exchanges\Exceptions\Providers\BinanceProvider\RequestIssueRequestException;
use App\Modules\Exchanges\Exceptions\Providers\BinanceProvider\UnknownRequestException;
use App\Modules\Exchanges\Services\BinanceService;
use App\Modules\Logs\Loggers\Logger;
use Illuminate\Contracts\Cache\LockTimeoutException;
use Illuminate\Support\Collection;
use stdClass;
use Throwable;

/**
 * DealsExecutorService.
 */
final class DealsExecutorService extends AbstractExecutorService implements WeightControlled
{
    use WeightControl;

    /**
     * Execute pending deals.
     *
     * @param bool $toJobs
     * @return int
     * @throws LockTimeoutException
     */
    public function executeDealsByGroups(bool $toJobs = true): int
    {
        $cnt = 0;

        $this->getDealsRepository()
            ->getPendingDeals()
            ->groupBy('bots_id')
            ->each(function (Collection $deals, int $botId) use (&$cnt, $toJobs) {
                $cnt += $deals->count();
                $bot = $this->getBotsService()
                    ->find($botId);

                if ($toJobs) {
                    DealsExecutorJob::dispatch($bot, $deals->count());
                } else {
                    $this->executeDeals($bot);
                }
            });

        return $cnt;
    }

    /**
     * Execute deals.
     *
     * @param Bot $bot
     * @return OrdersProcessedDto
     * @throws LockTimeoutException
     */
    public function executeDeals(Bot $bot): OrdersProcessedDto
    {
        $ordersProcessed = null;

        $bot->lockAwaitOptimalTiming(function () use ($bot, &$ordersProcessed) {
            $binance = $this->getBinanceService()
                ->getBinanceByAccess($bot->access);

            try {
                DB::beginTransaction();
                Bot::refreshAndLockForUpdate($bot);

                if (!$bot->botIteration?->isPending && !$bot->botIteration?->closing_deals) {
                    $ordersProcessed = new OrdersProcessedDto(collect());

                    return;
                }

                $list = collect();

                try {
                    $deals = $this->getDealsRepository()
                        ->getPendingDeals($bot->id);

                    $deals->each(function (Deal $deal) use ($list, $bot, $deals) {
                        $orderProcessed = null;

                        try {
                            $orderProcessed = $this->executeDeal($deal);
                        }
                        catch (Throwable $e) {
                            $this->getLogger()
                                ->registerException($e, [$deal->tryToApi()]);

                            event(new OrdersExceptionEvent($bot, $deal, $deals, $e));

                            throw $e;
                        } finally {
                            if (!$orderProcessed) {
                                $orderProcessed = new OrderProcessedDto(deal: $deal, catch: true);
                            }

                            $list->add($orderProcessed);
                        }
                    });

                    $ordersProcessed = new OrdersProcessedDto($list);

                    $this->sendEvents($bot, $ordersProcessed, true);
                } catch (Throwable) {
                    $ordersProcessed = new OrdersProcessedDto($list);
                }
            } finally {
                DB::commit();

                $this->setLeftWeight(
                    $binance->leftWeight(),
                    $binance->leftWeightAt(),
                );
            }
        });

        return $ordersProcessed;
    }

    /**
     * Open deal.
     *
     * @param Deal $deal
     * @return OrderProcessedDto
     * @throws BaseRequestException
     * @throws CancelledRejectedRequestException
     * @throws ErrorMsgReceivedRequestException
     * @throws FilterFailureRequestException
     * @throws GeneralRequestException
     * @throws NewOrderRejectedRequestException
     * @throws RequestException
     * @throws RequestIssueRequestException
     * @throws UnknownRequestException
     * @throws TransactionNotStartedException
     */
    public function executeDeal(Deal $deal): OrderProcessedDto
    {
        Deal::refreshAndLockForUpdate($deal);
        usleep(500000);

        try {
            if ($deal->cancel) {
                if ($deal->binance_client_order_id) {
                    $order = $this->cancel($deal);
                } else {
                    $deal->status = Deal::STATUS_CANCELLED;
                    $deal->cancel = false;
                    $deal->save();

                    return new OrderProcessedDto(deal: $deal, cancel: true);
                }
            } else {
                $order = $this->newLimitOrder($deal);
            }

            return $this->binanceOrderProcessResponse($deal, $order);
        } catch (BaseRequestException $e) {
            $this->createDealError($e, $deal, true);

            throw $e;
        }
    }

    /**
     * Cancel limit order.
     *
     * @param Deal $deal
     * @return stdClass
     * @throws CancelledRejectedRequestException
     * @throws ErrorMsgReceivedRequestException
     * @throws FilterFailureRequestException
     * @throws NewOrderRejectedRequestException
     * @throws RequestException
     * @throws GeneralRequestException
     * @throws RequestIssueRequestException
     * @throws UnknownRequestException
     */
    private function cancel(Deal $deal): stdClass
    {
        $binance = $this->getBinanceService()
            ->getBinanceByAccess($deal->bot->access);

        return $binance->cancelOrder(
            $deal->bot->pair->pair,
            null,
            $deal->binance_client_order_id
        );
    }

    /**
     * New limit order.
     *
     * @param Deal $deal
     * @return stdClass
     * @throws RequestException
     */
    private function newLimitOrder(Deal $deal): stdClass
    {
        $binance = $this->getBinanceService()
            ->getBinanceByAccess($deal->bot->access);

        return $binance->newLimitOrder(
            $deal->bot->pair->pair,
            $deal->sideText,
            priceCeil($deal->volume / $deal->price, $deal->bot->pair->stepSizeAsFloat),
            $deal->price
        );
    }

    /**
     * Get binance service.
     *
     * @return BinanceService
     */
    private function getBinanceService(): BinanceService
    {
        return app(BinanceService::class);
    }

    /**
     * Get deal repository.
     *
     * @return DealsRepository
     */
    private function getDealsRepository(): DealsRepository
    {
        return app(DealsRepository::class);
    }

    /**
     * Get {@see Logger::class}.
     *
     * @return Logger
     */
    private function getLogger(): Logger
    {
        return app(Logger::class);
    }

    /**
     * Get {@see BotsService::class}.
     *
     * @return BotsService
     */
    private function getBotsService(): BotsService
    {
        return app(BotsService::class);
    }
}
