<?php
declare(strict_types=1);

namespace App\Modules\Deals\Services;

use App\Modules\Bots\Dto\Traders\Deals\DealDto;
use App\Modules\Bots\Models\Bot;
use App\Modules\Core\Contracts\Services\ServiceContract;
use App\Modules\Deals\Contracts\DealsModuleContract;
use App\Modules\Deals\Models\Deal;
use App\Modules\Deals\Repositories\DealsRepository;
use App\Modules\Users\Models\User;
use Illuminate\Support\Collection;
use JetBrains\PhpStorm\ExpectedValues;

/**
 * Deals service.
 */
final class DealsService implements ServiceContract, DealsModuleContract
{
    /**
     * Get list.
     *
     * @param User $user
     * @return Collection
     */
    public function list(User $user): Collection
    {
        return $this->listModels($user)
            ->map(fn(Deal $deal) => $deal->toApi());
    }

    /**
     * Get list by bot.
     *
     * @param Bot $bot
     * @return Collection
     */
    public function listByBot(Bot $bot): Collection
    {
        return $this->listByBotModels($bot)
            ->map(fn(Deal $deal) => $deal->toApi());
    }

    /**
     * Get models list.
     *
     * @param User $user
     * @return Collection
     */
    public function listModels(User $user): Collection
    {
        return $this->getDealsRepository()
            ->getList($user->id);
    }

    /**
     * Get models list by bot.
     *
     * @param Bot $bot
     * @return Collection
     */
    public function listByBotModels(Bot $bot): Collection
    {
        return $this->getDealsRepository()
            ->getListByBot($bot->id);
    }

    /**
     * Add new deal.
     *
     * @param Bot $bot
     * @param DealDto $deal
     * @param int $status
     * @return Deal
     */
    public function add(
        Bot $bot,
        DealDto $deal,
        #[ExpectedValues(Deal::STATUSES)] int $status,
    ): Deal
    {
        $dealModel = new Deal();
        $dealModel->bot()
            ->associate($bot);
        $dealModel->volume = $deal->quantity;
        $dealModel->side = $deal->type;
        $dealModel->type = $deal->orderType;
        $dealModel->price = $deal->price;
        $dealModel->market_price = $deal->marketPrice;
        $dealModel->status = $status;

        $dealModel->save();

        return $dealModel;
    }

    /**
     * Cancel deals by bot.
     *
     * @param Bot $bot
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function cancelNewByBot(Bot $bot): \Illuminate\Database\Eloquent\Collection
    {
        return $this->getDealsRepository()
            ->getNotCancelledDeals($bot->id)
            ->each(fn (Deal $deal) => $deal->closeDeal());
    }

    /**
     * Replace deals. Cancel and open new.
     * {@see DealDto::clas}.
     *
     * @param Bot $bot
     * @param Collection $deals
     * @return int
     */
    public function replaceDeals(Bot $bot, Collection $deals): int
    {
        $countChanges = 0;
        $dealsNew = $bot->dealsPendingOrSet;

        //cancelling
        $dealsNew->each(function (Deal $deal) use ($deals, &$countChanges) {
            if ($deals->filter(fn(DealDto $dealDto) => $dealDto->price === $deal->price)
                    ->count() === 0) {
                $deal->closeDeal();
                $countChanges++;
            }
        });

        //new
        $deals->each(function (DealDto $dealDto) use ($dealsNew, $bot, &$countChanges) {
            if ($dealsNew->filter(fn(Deal $deal) => $deal->price === $dealDto->price)
                    ->count() === 0) {
                $this->add(
                    $bot,
                    $dealDto,
                    Deal::STATUS_PENDING
                );
                $countChanges++;
            }
        });

        return $countChanges;
    }

    /**
     * Get {@see DealsService::class}.
     *
     * @return DealsRepository
     */
    private function getDealsRepository(): DealsRepository
    {
        return app(DealsRepository::class);
    }
}
