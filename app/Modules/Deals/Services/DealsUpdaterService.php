<?php
declare(strict_types=1);

namespace App\Modules\Deals\Services;

use App\Modules\Bots\Dto\Services\DealsExecutor\OrderProcessedDto;
use App\Modules\Bots\Dto\Services\DealsExecutor\OrdersProcessedDto;
use App\Modules\Bots\Exceptions\Services\Bots\BotNotLockedException;
use App\Modules\Bots\Models\Bot;
use App\Modules\Bots\Services\BotsService;
use App\Modules\Core\Contracts\Jobs\WeightControlled;
use App\Modules\Core\Exceptions\Services\Network\RequestException;
use App\Modules\Core\Exceptions\Traits\Lock\TransactionNotStartedException;
use App\Modules\Core\Facades\DB;
use App\Modules\Core\Traits\Tasks\WeightControl;
use App\Modules\Deals\Models\Deal;
use App\Modules\Deals\Repositories\DealsRepository;
use App\Modules\Deals\Tasks\DealsUpdaterJob;
use App\Modules\Exchanges\Exceptions\Providers\BinanceProvider\BaseRequestException;
use App\Modules\Exchanges\Exceptions\Providers\BinanceProvider\CancelledRejectedRequestException;
use App\Modules\Exchanges\Exceptions\Providers\BinanceProvider\ErrorMsgReceivedRequestException;
use App\Modules\Exchanges\Exceptions\Providers\BinanceProvider\FilterFailureRequestException;
use App\Modules\Exchanges\Exceptions\Providers\BinanceProvider\GeneralRequestException;
use App\Modules\Exchanges\Exceptions\Providers\BinanceProvider\NewOrderRejectedRequestException;
use App\Modules\Exchanges\Exceptions\Providers\BinanceProvider\RequestIssueRequestException;
use App\Modules\Exchanges\Exceptions\Providers\BinanceProvider\UnknownRequestException;
use App\Modules\Exchanges\Services\BinanceService;
use App\Modules\Logs\Loggers\Logger;
use DateTime;
use Illuminate\Support\Collection;
use stdClass;
use Throwable;

/**
 * Deals updater service.
 */
final class DealsUpdaterService extends AbstractExecutorService implements WeightControlled
{
    use WeightControl;

    /**
     * Execute pending deals
     *
     * @param bool $toJobs
     * @return int
     * @throws BotNotLockedException
     * @throws ErrorMsgReceivedRequestException
     * @throws GeneralRequestException
     * @throws RequestException
     * @throws RequestIssueRequestException
     * @throws Throwable
     * @throws TransactionNotStartedException
     * @throws UnknownRequestException
     */
    public function updateDealsByGroups(bool $toJobs = true): int
    {
        $cnt = 0;

        $this->getDealsRepository()
            ->getObservedDeals()
            ->groupBy('bots_id')
            ->each(function (Collection $deals, $botId) use (&$cnt, $toJobs) {
                $cnt += $deals->count();
                $bot = $this->getBotsService()
                    ->find($botId);

                if ($toJobs) {
                    DealsUpdaterJob::dispatch($bot, 1 + $deals->count() * 2);
                } else {
                    $this->updateDealsByBot($bot);
                }
            });

        return $cnt;
    }

    /**
     * Update deals by bot.
     *
     * @param Bot $bot
     * @return OrdersProcessedDto
     * @throws ErrorMsgReceivedRequestException
     * @throws GeneralRequestException
     * @throws RequestException
     * @throws RequestIssueRequestException
     * @throws Throwable
     * @throws UnknownRequestException
     * @throws TransactionNotStartedException
     */
    public function updateDealsByBot(Bot $bot): OrdersProcessedDto
    {
        $ordersProcessed = null;

        $bot->lockAwaitOptimalTiming( function () use ($bot, &$ordersProcessed) {
            $binance = $this->getBinanceService()
                ->getBinanceByAccess($bot->access);

            try {
                DB::beginTransaction();
                Bot::refreshAndLockForUpdate($bot);

                if (!$bot->botIteration?->isMonitoring) {
                    $ordersProcessed = new OrdersProcessedDto(collect());

                    return;
                }

                $deals = $this->getDealsRepository()
                    ->getObservedDeals($bot->id);

                $list = collect();

                if ($deals->count() > 0) {
                    $openOrders = $binance
                        ->openOrders($bot->pair->pair);

                    $deals->each(function (Deal $deal) use ($openOrders, $list) {
                        $orderProcessed = null;

                        try {
                            $orderProcessed = $this->updateDeal($deal, $openOrders);
                        } catch (BaseRequestException) {
                        } catch (Throwable $e) {
                            $this->getLogger()
                                ->registerException($e, [$deal->tryToApi()]);

                            throw $e;
                        } finally {
                            if (!$orderProcessed) {
                                $orderProcessed = new OrderProcessedDto(deal: $deal, catch: true);
                            }
                        }

                        $list->add($orderProcessed);
                    });
                }

                $ordersProcessed = new OrdersProcessedDto($list);

                $this->sendEvents($bot, $ordersProcessed, false);
            } finally {
                DB::commit();

                $this->setLeftWeight(
                    $binance->leftWeight(),
                    $binance->leftWeightAt(),
                );
            }
        });

        return $ordersProcessed;
    }

    /**
     * Update deal status.
     *
     * @param Deal $deal
     * @param Collection $openOrders
     * @return OrderProcessedDto
     * @throws BaseRequestException
     * @throws CancelledRejectedRequestException
     * @throws ErrorMsgReceivedRequestException
     * @throws FilterFailureRequestException
     * @throws GeneralRequestException
     * @throws NewOrderRejectedRequestException
     * @throws RequestException
     * @throws RequestIssueRequestException
     * @throws UnknownRequestException
     * @throws TransactionNotStartedException
     */
    public function updateDeal(Deal $deal, Collection $openOrders): OrderProcessedDto
    {
        Deal::refreshAndLockForUpdate($deal);
        $binance = $this->getBinanceService()
            ->getBinanceByAccess($deal->bot->access);

        try {
            if ($openOrders
                    ->filter(fn(stdClass $order) => $order->clientOrderId === $deal->binance_client_order_id)
                    ->count() === 0) {
                $order = $binance->queryOrder($deal->bot->pair->pair, null, $deal->binance_client_order_id);

                $deal->cancel = false;
                $deal->save();

                return $this->binanceOrderProcessResponse($deal, $order);
            } else {
                $deal->updated_at = new DateTime();

                $deal->save();

                return new OrderProcessedDto(deal: $deal);
            }
        } catch (BaseRequestException $e) {
            $this->createDealError($e, $deal, false);

            throw $e;
        }
    }

    /**
     * Get {@see BinanceService::class}.
     *
     * @return BinanceService
     */
    private function getBinanceService(): BinanceService
    {
        return app(BinanceService::class);
    }

    /**
     * Get {@see BotsService::class}.
     *
     * @return BotsService
     */
    private function getBotsService(): BotsService
    {
        return app(BotsService::class);
    }

    /**
     * Get {@see DealsRepository::class}.
     *
     * @return DealsRepository
     */
    private function getDealsRepository(): DealsRepository
    {
        return app(DealsRepository::class);
    }

    /**
     * Get {@see Logger::class}.
     *
     * @return Logger
     */
    private function getLogger(): Logger
    {
        return app(Logger::class);
    }
}
