<?php
declare(strict_types=1);

namespace App\Modules\Deals\Tasks;

use App\Modules\Bots\Models\Bot;
use App\Modules\Core\Contracts\Services\DataUpdaterContract;
use App\Modules\Core\Exceptions\Services\Network\RequestException;
use App\Modules\Core\Exceptions\Traits\Lock\TransactionNotStartedException;
use App\Modules\Core\Jobs\AbstractJob;
use App\Modules\Deals\Services\DealsUpdaterService;
use App\Modules\Exchanges\Exceptions\Providers\BinanceProvider\ErrorMsgReceivedRequestException;
use App\Modules\Exchanges\Exceptions\Providers\BinanceProvider\GeneralRequestException;
use App\Modules\Exchanges\Exceptions\Providers\BinanceProvider\RequestIssueRequestException;
use App\Modules\Exchanges\Exceptions\Providers\BinanceProvider\UnknownRequestException;
use App\Modules\Exchanges\Jobs\BinanceWeightLimited;
use App\Modules\Market\Contracts\MarketModuleContract;
use DateTime;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\Middleware\WithoutOverlapping;
use Illuminate\Queue\SerializesModels;
use Throwable;
use function app;

/**
 * DealUpdaterJob.
 */
final class DealsUpdaterJob extends AbstractJob implements MarketModuleContract, DataUpdaterContract, ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Constructor.
     *
     * @param Bot $bot
     * @param int $weight
     */
    public function __construct(
        private Bot $bot,
        protected int $weight
    )
    {
    }

    /**
     * Update prices in database from system binance api.
     *
     * @throws RequestException
     * @throws TransactionNotStartedException
     * @throws ErrorMsgReceivedRequestException
     * @throws GeneralRequestException
     * @throws RequestIssueRequestException
     * @throws UnknownRequestException
     * @throws Throwable
     */
    public function handle(): void
    {
        $this->info();

        $this->getDealsUpdaterService()
            ->updateDealsByBot($this->bot);
    }

    /**
     * {@inheritDoc}
     */
    public function getId(): string
    {
        return 'deals:updater';
    }

    /**
     * {@inheritDoc}
     */
    public function getWeight(): int
    {
        return $this->weight;
    }

    /**
     * Determine the time at which the job should timeout.
     *
     * @return DateTime
     */
    public function retryUntil(): DateTime
    {
        return now()->addMinutes(10);
    }

    /**
     * Get the middleware the job should pass through.
     *
     * @return array
     */
    public function middleware(): array
    {
        return [
            (new WithoutOverlapping(sprintf('trading_bot_%s', $this->bot->id)))
                ->expireAfter(1000)
                ->dontRelease(),
            new BinanceWeightLimited($this->getWeight())
        ];
    }

    /**
     * Get {@see DealsUpdaterService::class}.
     *
     * @return DealsUpdaterService
     */
    private function getDealsUpdaterService(): DealsUpdaterService
    {
        return app(DealsUpdaterService::class);
    }
}
