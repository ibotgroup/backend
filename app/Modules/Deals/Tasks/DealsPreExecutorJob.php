<?php
declare(strict_types=1);

namespace App\Modules\Deals\Tasks;

use App\Modules\Core\Jobs\AbstractJob;
use App\Modules\Core\Jobs\RateLimited;
use App\Modules\Deals\Services\DealsExecutorService;
use App\Modules\Market\Contracts\MarketModuleContract;
use DateTime;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\Middleware\WithoutOverlapping;
use Illuminate\Queue\SerializesModels;
use Throwable;
use function app;

/**
 * DealsPreExecutorJob.
 */
final class DealsPreExecutorJob extends AbstractJob implements MarketModuleContract, ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Update prices in database from system binance api.
     *
     * @throws Throwable
     */
    public function handle(): void
    {
        $this->info();

        $this->getDealsExecutorService()
            ->executeDealsByGroups();
    }

    /**
     * {@inheritDoc}
     */
    public function getId(): string
    {
        return 'deals:pre-executor';
    }

    /**
     * {@inheritDoc}
     */
    public function getWeight(): int
    {
        return 0;
    }

    /**
     * Determine the time at which the job should timeout.
     *
     * @return DateTime
     */
    public function retryUntil(): DateTime
    {
        return now()->addMinutes(5);
    }

    /**
     * Get the middleware the job should pass through.
     *
     * @return array
     */
    public function middleware(): array
    {
        return [
            (new WithoutOverlapping($this->getId()))
                ->expireAfter(300)
                ->dontRelease(),
            new RateLimited($this->getId(), 1, true)
        ];
    }

    /**
     * Get {@see DealsExecutorService::class}.
     *
     * @return DealsExecutorService
     */
    private function getDealsExecutorService(): DealsExecutorService
    {
        return app(DealsExecutorService::class);
    }
}
