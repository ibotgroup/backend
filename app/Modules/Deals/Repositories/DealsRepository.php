<?php
declare(strict_types=1);

namespace App\Modules\Deals\Repositories;

use App\Modules\Bots\Models\Bot;
use App\Modules\Bots\Models\BotIteration;
use App\Modules\Core\Contracts\Repositories\RepositoryContract;
use App\Modules\Core\Repositories\BaseRepository;
use App\Modules\Deals\Contracts\DealsModuleContract;
use App\Modules\Deals\Models\Deal;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

/**
 * Deals repository.
 */
final class DealsRepository extends BaseRepository implements DealsModuleContract, RepositoryContract
{
    /**
     * Constructor.
     *
     * @param Deal $model
     */
    public function __construct(protected Deal $model)
    {
    }

    /**
     * Get list deals.
     *
     * @param int $userId
     * @return Collection
     */
    public function getList(
        int $userId,
    ): Collection
    {
        return $this->getBaseQuery()
            ->select('deals.*')
            ->where('deals.status', '!=', Deal::STATUS_CANCELLED)
            ->join('bots', 'deals.bots_id', '=', 'bots.id')
            ->where('bots.users_id', $userId)
            ->orderBy('deals.created_at', 'DESC')
            ->get();
    }

    /**
     * Get list deals.
     *
     * @param int $botId
     * @return Collection
     */
    public function getListByBot(
        int $botId,
    ): Collection
    {
        return $this->getBaseQuery()
            ->where('status', '!=', Deal::STATUS_CANCELLED)
            ->where('bots_id', $botId)
            ->orderBy('created_at', 'DESC')
            ->get();
    }

    /**
     * Get pending deals.
     *
     * @param int|null $botId
     * @return Collection
     */
    public function getPendingDeals(int $botId = null): Collection
    {
        $query = $this->getBaseQuery()
            ->select('deals.*')
            ->where(static function (Builder $builder) {
                $builder->whereHas('bot', static function (Builder $builder) {
                    $builder->where(static function (Builder $builder) {
                        $builder->doesntHave('botIteration')
                            ->orWhereHas('botIteration', static function (Builder $builder) {
                                $builder->where('status', BotIteration::STATUS_PENDING);
                            });
                    })
                        ->where('status', Bot::STATUS_ACTIVE);
                })
                    ->where('deals.status', Deal::STATUS_PENDING);
            })
            ->orWhere(static function (Builder $builder) {
                $builder->whereHas('bot', static function (Builder $builder) {
                    $builder->whereHas('botIteration', static function (Builder $builder) {
                        $builder->where('closing_deals', true)
                            ->where('status', BotIteration::STATUS_PROCESSED);
                    });
                })
                    ->where('deals.cancel', true);
            })
            ->orderBy('deals.cancel', 'DESC')
            ->orderBy('deals.updated_at', 'ASC');

        if ($botId !== null) {
            $query->where('deals.bots_id', $botId);
        }

        return $query->get();
    }

    /**
     * Get observed deals.
     *
     * @param int|null $botId
     * @return Collection
     */
    public function getObservedDeals(int $botId = null): Collection
    {
        $query = $this->getBaseQuery()
            ->select('deals.*')
            ->where(static function (Builder $builder) {
                $builder->whereHas('bot', static function (Builder $builder) {
                    $builder->where(static function (Builder $builder) {
                        $builder->doesntHave('botIteration')
                            ->orWhereHas('botIteration', static function (Builder $builder) {
                                $builder->where('status', BotIteration::STATUS_MONITORING);
                            });
                    })
                        ->where('status', Bot::STATUS_ACTIVE);
                })
                    ->where('deals.status', Deal::STATUS_SET);
            })
            ->orWhere(static function (Builder $builder) {
                $builder->whereHas('bot', static function (Builder $builder) {
                    $builder->whereHas('botIteration', static function (Builder $builder) {
                        $builder->where('closing_deals', true)
                            ->where('status', BotIteration::STATUS_MONITORING);
                    });
                })
                    ->where('deals.cancel', true);
            })
            ->orderBy('deals.updated_at', 'ASC');

        if ($botId !== null) {
            $query->where('deals.bots_id', $botId);
        }

        return $query->get();
    }

    /**
     * Get not cancelled deals.
     *
     * @param int $botId
     * @return Collection
     */
    public function getNotCancelledDeals(int $botId): Collection
    {
        return $this->getBaseQuery()
            ->select('deals.*')
            ->whereIn('deals.status', [Deal::STATUS_PENDING, Deal::STATUS_SET])
            ->orderBy('deals.updated_at', 'ASC')
            ->where('deals.bots_id', $botId)
            ->get();
    }

    /**
     * Get base query.
     *
     * @return Builder
     */
    private function getBaseQuery(): Builder
    {
        return $this->model
            ->newQuery()
            ->with('bot')
            ->with('bot.access')
            ->with('bot.algorithm')
            ->with('bot.pair');
    }
}
