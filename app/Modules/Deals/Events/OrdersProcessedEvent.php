<?php
declare(strict_types=1);

namespace App\Modules\Deals\Events;

use App\Modules\Bots\Dto\Services\DealsExecutor\OrdersProcessedDto;
use App\Modules\Bots\Models\Bot;
use App\Modules\Core\Contracts\EventContract;
use App\Modules\Core\Traits\ProtectedProperties;
use App\Modules\Deals\Contracts\DealsModuleContract;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Order filled.
 *
 * @property-read OrdersProcessedDto $deals
 * @property-read Bot $bot
 * @property-read bool $iterationCompleted
 */
final class OrdersProcessedEvent implements EventContract, DealsModuleContract
{
    use Dispatchable, InteractsWithSockets, SerializesModels, ProtectedProperties;

    /**
     * Constructor.
     *
     * @param Bot $bot
     * @param OrdersProcessedDto $deals
     * @param bool $iterationCompleted
     */
    public function __construct(private Bot $bot, private OrdersProcessedDto $deals, private bool $iterationCompleted)
    {
    }
}
