<?php
declare(strict_types=1);

namespace App\Modules\Deals\Events;

use App\Modules\Bots\Models\Bot;
use App\Modules\Core\Contracts\EventContract;
use App\Modules\Core\Traits\ProtectedProperties;
use App\Modules\Deals\Contracts\DealsModuleContract;
use App\Modules\Deals\Models\Deal;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Collection;
use Throwable;

/**
 * Orders throws event. If critical error when deals opening.
 *
 * @property-read Bot $bot
 * @property-read Collection $deal
 * @property-read Collection $deals
 * @property-read Throwable $e
 */
final class OrdersExceptionEvent implements EventContract, DealsModuleContract
{
    use Dispatchable, InteractsWithSockets, SerializesModels, ProtectedProperties;

    /**
     * Constructor.
     *
     * @param Bot $bot
     * @param Deal $deal
     * @param Collection $deals
     * @param Throwable $e
     */
    public function __construct(
        private Bot $bot,
        private Deal $deal,
        private Collection $deals,
        private Throwable $e
    )
    {
    }
}
