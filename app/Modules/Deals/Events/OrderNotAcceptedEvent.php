<?php
declare(strict_types=1);

namespace App\Modules\Deals\Events;

use App\Modules\Core\Contracts\EventContract;
use App\Modules\Core\Traits\ProtectedProperties;
use App\Modules\Deals\Contracts\DealsModuleContract;
use App\Modules\Deals\Models\Deal;
use App\Modules\Deals\Models\DealError;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Order not accepted.
 *
 * @property-read Deal $deal
 * @property-read DealError $dealError
 */
final class OrderNotAcceptedEvent implements EventContract, DealsModuleContract
{
    use Dispatchable, InteractsWithSockets, SerializesModels, ProtectedProperties;

    /**
     * Constructor.
     *
     * @param Deal $deal
     * @param DealError $dealError
     */
    public function __construct(private Deal $deal, private DealError $dealError)
    {
    }
}
