<?php
declare(strict_types=1);

namespace App\Modules\Deals\Exceptions\Services;

use App\Modules\Deals\Exceptions\DealsException;

/**
 * ServicesException.
 */
interface ServicesException extends DealsException
{
}
