<?php
declare(strict_types=1);

namespace App\Modules\Deals\Exceptions\Services\Deals;

use App\Modules\Deals\Exceptions\Services\ServicesException;

/**
 * DealsException.
 */
interface DealsException extends ServicesException
{
}
