<?php
declare(strict_types=1);

namespace App\Modules\Deals\Exceptions\Services\Deals;

use Exception;

/**
 * OrderStatusIsNotSetException.
 */
class OrderStatusIsNotSetException extends Exception implements DealsException
{
}
