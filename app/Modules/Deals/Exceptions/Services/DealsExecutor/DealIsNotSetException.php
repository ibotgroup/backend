<?php
declare(strict_types=1);

namespace App\Modules\Deals\Exceptions\Services\DealsExecutor;

use App\Modules\Deals\Exceptions\DealsException;
use Exception;

/**
 * DealIsNotSetException.
 */
final class DealIsNotSetException extends Exception implements DealsException
{
}
