<?php
declare(strict_types=1);

namespace App\Modules\Deals\Exceptions\Services\DealsExecutor;

use App\Modules\Deals\Exceptions\DealsException;
use Exception;
use Throwable;

/**
 * ExecuteDealsErrorsException.
 */
final class ExecuteDealsErrorsException extends Exception implements DealsException
{
    /**
     * Constructor.
     *
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     * @param int $count
     * @param int $countErrors
     */
    public function __construct($message = "", $code = 0, Throwable $previous = null, protected int $count = 0, protected int $countErrors = 0)
    {
        parent::__construct($message, $code, $previous);
    }

    /**
     * Get count errros.
     *
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }

    /**
     * Get count errors.
     *
     * @return int
     */
    public function getCountErrors(): int
    {
        return $this->countErrors;
    }
}
