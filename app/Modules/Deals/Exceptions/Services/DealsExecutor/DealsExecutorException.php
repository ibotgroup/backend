<?php
declare(strict_types=1);

namespace App\Modules\Deals\Exceptions\Services\DealsExecutor;

use App\Modules\Deals\Exceptions\Services\ServicesException;

/**
 * DealsExecutorException.
 */
interface DealsExecutorException extends ServicesException
{
}
