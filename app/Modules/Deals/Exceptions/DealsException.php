<?php
declare(strict_types=1);

namespace App\Modules\Deals\Exceptions;

use App\Modules\Deals\Contracts\DealsModuleContract;
use Throwable;

/**
 * DealsException.
 */
interface DealsException extends Throwable, DealsModuleContract
{
}
