<?php
declare(strict_types=1);

namespace App\Modules\Deals\Models;

use App\Modules\Bots\Enum\Trades\OrderTypeEnum;
use App\Modules\Bots\Enum\Trades\TypeEnum;
use App\Modules\Bots\Models\Bot;
use App\Modules\Core\Contracts\Models\Api;
use App\Modules\Core\Exceptions\Models\ModelNotExistsException;
use App\Modules\Deals\Dto\Api\DealDto;
use App\Modules\Deals\Contracts\DealsModuleContract;
use App\Modules\Core\Models\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Throwable;

/**
 * Bot model.
 *
 * @property-read int $id
 * @property-read int $sid
 * @property int $binance_order_id
 * @property string $binance_client_order_id
 * @property-read int $bots_id
 * @property float $volume
 * @property int $status
 * @property int $side
 * @property int $type
 * @property float $price
 * @property float $market_price
 * @property Carbon|null $set_at
 * @property Carbon|null $triggered_at
 * @property Carbon|null $cancelled_at
 * @property Carbon|null $failed_at
 * @property boolean $cancel
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Bot $bot
 * @property-read string $statusText
 * @property-read string $sideText
 * @property-read string $typeText
 * @property-read bool $isPending
 * @property-read bool $isSet
 * @property-read bool $isTriggered
 * @property-read bool $isCancelled
 * @property-read bool $isFailed
 * @property-read bool $isError
 */
final class Deal extends Model implements DealsModuleContract, Api
{
    public const STATUS_PENDING = 1;
    public const STATUS_SET = 2;
    public const STATUS_TRIGGERED = 3;
    public const STATUS_CANCELLED = 4;
    public const STATUS_FAILED = 5;
    public const STATUS_ERROR = 6;
    public const STATUS_CANCEL = 7;

    public const STATUSES = [
        self::STATUS_PENDING,
        self::STATUS_SET,
        self::STATUS_TRIGGERED,
        self::STATUS_CANCELLED,
        self::STATUS_FAILED,
        self::STATUS_ERROR,
        self::STATUS_CANCEL,
    ];

    public const STATUSES_TEXT = [
        self::STATUS_PENDING => 'Pending',
        self::STATUS_SET => 'New',
        self::STATUS_TRIGGERED => 'Filled',
        self::STATUS_CANCELLED => 'Cancelled',
        self::STATUS_FAILED => 'Failed',
        self::STATUS_ERROR => 'Error',
        self::STATUS_CANCEL => 'Cancelled manually',
    ];

    /** {@inheritdoc} */
    protected $table = 'deals';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'sid' => 'integer',
        'binance_order_id' => 'integer',
        'binance_client_order_id' => 'string',
        'bots_id' => 'integer',
        'volume' => 'float',
        'status' => 'integer',
        'side' => 'integer',
        'type' => 'integer',
        'price' => 'float',
        'market_price' => 'float',
        'set_at' => 'datetime',
        'triggered_at' => 'datetime',
        'cancelled_at' => 'datetime',
        'failed_at' => 'datetime',
        'cancel' => 'boolean',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    /**
     * Get "statusText" аттрибут.
     *
     * @return string
     */
    public function getStatusTextAttribute(): string
    {
        return (string)(self::STATUSES_TEXT[$this->status] ?? $this->status);
    }

    /**
     * Get "sideText" аттрибут.
     *
     * @return string
     */
    public function getSideTextAttribute(): string
    {
        return (string)(TypeEnum::TYPES_TEXT[$this->side] ?? $this->side);
    }

    /**
     * Get "typeText" аттрибут.
     *
     * @return string
     */
    public function getTypeTextAttribute(): string
    {
        return (string)(OrderTypeEnum::TYPES_TEXT[$this->type] ?? $this->type);
    }

    /**
     * {@see Bot::class}.
     *
     * @return BelongsTo
     */
    public function bot(): BelongsTo
    {
        return $this->belongsTo(Bot::class, 'bots_id');
    }

    /**
     * Get attribute "isPending".
     *
     * @return bool
     */
    public function getIsPendingAttribute(): bool
    {
        return $this->status === self::STATUS_PENDING;
    }

    /**
     * Get attribute "isSet".
     *
     * @return bool
     */
    public function getIsSetAttribute(): bool
    {
        return $this->status === self::STATUS_SET;
    }

    /**
     * Get attribute "isTriggered".
     *
     * @return bool
     */
    public function getIsTriggeredAttribute(): bool
    {
        return $this->status === self::STATUS_TRIGGERED;
    }

    /**
     * Get attribute "isCancelled".
     *
     * @return bool
     */
    public function getIsCancelledAttribute(): bool
    {
        return $this->status === self::STATUS_CANCELLED;
    }

    /**
     * Get attribute "isFailed".
     *
     * @return bool
     */
    public function getIsFailedAttribute(): bool
    {
        return $this->status === self::STATUS_FAILED;
    }

    /**
     * Get attribute "isError".
     *
     * @return bool
     */
    public function getIsErrorAttribute(): bool
    {
        return $this->status === self::STATUS_ERROR;
    }

    /**
     * Get attribute "isCancel".
     *
     * @return bool
     */
    public function getIsCancelAttribute(): bool
    {
        return $this->status === self::STATUS_CANCEL;
    }

    /**
     * To api dto.
     *
     * @return DealDto
     * @throws ModelNotExistsException
     */
    public function toApi(): DealDto
    {
        $this->throwIfNotExists();

        return new DealDto(
            $this->bot->toApi(),
            $this->volume,
            $this->status,
            $this->statusText,
            $this->side,
            $this->sideText,
            $this->type,
            $this->typeText,
            $this->price,
            $this->market_price,
            $this->set_at?->format('Y-m-d H:i:s'),
            $this->triggered_at?->format('Y-m-d H:i:s'),
            $this->cancelled_at?->format('Y-m-d H:i:s'),
            $this->failed_at?->format('Y-m-d H:i:s'),
            $this->created_at?->format('Y-m-d H:i:s'),
            $this->updated_at?->format('Y-m-d H:i:s'),
        );
    }

    /**
     * Try to dto api.
     *
     * @return DealDto|null
     */
    public function tryToApi(): ?DealDto
    {
        try {
            return $this->toApi();
        } catch (Throwable) {
        }

        return null;
    }

    /**
     * Close deal.
     */
    public function closeDeal(): void
    {
        if ($this->status === Deal::STATUS_PENDING) {
            $this->status = Deal::STATUS_CANCELLED;
        } else {
            $this->cancel = true;
        }

        $this->save();
    }

    /**
     * Set some events.
     *
     * @return void
     */
    protected static function booted(): void
    {
        static::creating(function (self $model) {
            $model->sid = rndSid();
        });
    }
}
