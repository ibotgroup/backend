<?php
declare(strict_types=1);

namespace App\Modules\Deals\Models;

use App\Modules\Bots\Models\Bot;
use App\Modules\Core\Models\Model;
use App\Modules\Deals\Contracts\DealsModuleContract;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Deal error code.
 *
 * @property-read int $id
 * @property-read int $deals_id
 * @property string $code
 * @property string $message
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Deal $deal
 */
final class DealError extends Model implements DealsModuleContract
{
    /** {@inheritdoc} */
    protected $table = 'deals_errors';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'deals_id' => 'integer',
        'code' => 'string',
        'message' => 'string',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    /**
     * {@see Deal::class}.
     *
     * @return BelongsTo
     */
    public function deal(): BelongsTo
    {
        return $this->belongsTo(Deal::class, 'deals_id');
    }
}
