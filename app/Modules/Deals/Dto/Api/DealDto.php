<?php
declare(strict_types=1);

namespace App\Modules\Deals\Dto\Api;

use App\Modules\Bots\Dto\Api\Bots\BotDto;
use App\Modules\Core\Dto\BaseDto;
use App\Modules\Deals\Contracts\DealsModuleContract;

/**
 * Deal dto,
 *
 * @property-read BotDto $bot
 * @property-read float $volume
 * @property-read int $status
 * @property-read string $statusText
 * @property-read int $side
 * @property-read string $sideText
 * @property-read int $type
 * @property-read string $typeText
 * @property-read float $price
 * @property-read float $marketPrice
 * @property-read string|null $setAt
 * @property-read string|null $triggeredAt
 * @property-read string|null $cancelledAt
 * @property-read string|null $failedAt
 * @property-read string|null $createdAt
 * @property-read string|null $updatedAt
 */
final class DealDto extends BaseDto implements DealsModuleContract
{
    /**
     * Constructor.
     *
     * @param BotDto $bot
     * @param float $volume
     * @param int $status
     * @param string $statusText
     * @param int $side
     * @param string $sideText
     * @param int $type
     * @param string $typeText
     * @param float $price
     * @param float $marketPrice
     * @param string|null $setAt
     * @param string|null $triggeredAt
     * @param string|null $cancelledAt
     * @param string|null $failedAt
     * @param string|null $createdAt
     * @param string|null $updatedAt
     */
    public function __construct(
        protected BotDto $bot,
        protected float $volume,
        protected int $status,
        protected string $statusText,
        protected int $side,
        protected string $sideText,
        protected int $type,
        protected string $typeText,
        protected float $price,
        protected float $marketPrice,
        protected ?string $setAt = null,
        protected ?string $triggeredAt = null,
        protected ?string $cancelledAt = null,
        protected ?string $failedAt = null,
        protected ?string $createdAt = null,
        protected ?string $updatedAt = null,
    )
    {
    }
}
