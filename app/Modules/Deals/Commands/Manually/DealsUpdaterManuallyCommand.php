<?php
declare(strict_types=1);

namespace App\Modules\Deals\Commands\Manually;

use App\Modules\Core\Exceptions\Traits\Lock\TransactionNotStartedException;
use App\Modules\Deals\Contracts\DealsModuleContract;
use App\Modules\Deals\Services\DealsUpdaterService;
use Illuminate\Console\Command;
use Throwable;

/**
 * Deals updater command.
 */
final class DealsUpdaterManuallyCommand extends Command implements DealsModuleContract
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'deals:updater-manually';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Deals updater manually';

    /**
     * Handle.
     *
     * @return int
     * @throws TransactionNotStartedException
     * @throws Throwable
     */
    public function handle(): int
    {
        $cnt = $this->getDealsUpdaterService()
            ->updateDealsByGroups(false);

        $this->info(sprintf("Was updated %s orders manually.", $cnt));

        return 0;
    }

    /**
     * Get {@see DealsUpdaterService::class}.
     *
     * @return DealsUpdaterService
     */
    private function getDealsUpdaterService(): DealsUpdaterService
    {
        return app(DealsUpdaterService::class);
    }
}
