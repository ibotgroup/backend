<?php
declare(strict_types=1);

namespace App\Modules\Deals\Commands\Manually;

use App\Modules\Core\Exceptions\Traits\Lock\TransactionNotStartedException;
use App\Modules\Deals\Contracts\DealsModuleContract;
use App\Modules\Deals\Services\DealsExecutorService;
use Illuminate\Console\Command;
use Throwable;

/**
 * Deals executor command.
 */
final class DealsExecutorManuallyCommand extends Command implements DealsModuleContract
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'deals:executor-manually';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Deals executor manually';

    /**
     * Handle.
     *
     * @return int
     * @throws TransactionNotStartedException
     * @throws Throwable
     */
    public function handle(): int
    {
        $cnt = $this->getDealsExecutorService()
            ->executeDealsByGroups(false);

        $this->info(sprintf("Was executed %s orders manually.", $cnt));

        return 0;
    }

    /**
     * Get {@see DealsExecutorService::class}.
     *
     * @return DealsExecutorService
     */
    private function getDealsExecutorService(): DealsExecutorService
    {
        return app(DealsExecutorService::class);
    }
}
