<?php
declare(strict_types=1);

namespace App\Modules\Deals\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Deals\Requests\ListByBotRequest;
use App\Modules\Deals\Requests\ListRequest;
use App\Modules\Core\Attributes\Controllers\CatchResponse;
use App\Modules\Core\Dto\ResponseDto;
use App\Modules\Deals\Contracts\DealsModuleContract;
use App\Modules\Deals\Services\DealsService;

/**
 * Deals controller.
 */
final class DealsController extends Controller implements DealsModuleContract
{
    /**
     * List access.
     *
     * @param ListRequest $request
     * @return ResponseDto
     */
    #[CatchResponse]
    public function list(ListRequest $request): ResponseDto
    {
        return new ResponseDto(
            $this->getDealsService()
                ->list(
                    ...$request->validated()
                )
        );
    }

    /**
     * List by bot.
     *
     * @param ListByBotRequest $request
     * @return ResponseDto
     */
    #[CatchResponse]
    public function listByBot(ListByBotRequest $request): ResponseDto
    {
        return new ResponseDto(
            $this->getDealsService()
                ->listByBot(
                    ...$request->validated()
                )
        );
    }

    /**
     * Get {@see DealsService::class}.
     *
     * @return DealsService
     */
    private function getDealsService(): DealsService
    {
        return app(DealsService::class);
    }
}
