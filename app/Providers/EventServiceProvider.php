<?php
declare(strict_types=1);

namespace App\Providers;

use App\Modules\Bots\Listeners\OrderNotAcceptedListener;
use App\Modules\Bots\Listeners\OrdersProcessedListener;
use App\Modules\Bots\Listeners\OrderExceptionListener;
use App\Modules\Deals\Events\OrderNotAcceptedEvent;
use App\Modules\Deals\Events\OrdersProcessedEvent;
use App\Modules\Deals\Events\OrdersExceptionEvent;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

/**
 * Event service provider.
 */
class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        OrderNotAcceptedEvent::class => [
            OrderNotAcceptedListener::class,
        ],
        OrdersProcessedEvent::class => [
            OrdersProcessedListener::class,
        ],
        OrdersExceptionEvent::class => [
            OrderExceptionListener::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
    }
}
