<?php
declare(strict_types=1);

namespace App\Providers;

use App\Modules\Core\Validation\Numeric;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;
use App\Modules\Core\Validation\Scalar;
use function sprintf;

/**
 * App service provider.
 */
final class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register(): void
    {}

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(): void
    {
        $this->extendValidators();
    }

    /**
     * Extend validators.
     */
    private function extendValidators(): void
    {
        Validator::extend(Scalar::NAME, sprintf('%s@validate', Scalar::class));
        Validator::extend(Numeric::NAME, sprintf('%s@validate', Numeric::class));
    }
}
