<?php
declare(strict_types=1);

namespace App\Console;

use App\Modules\Bots\Commands\GridTesterCommand;
use App\Modules\Bots\Commands\TraderCommand;
use App\Modules\Core\Commands\CreateUserCommand;
use App\Modules\Core\Commands\DemoCommand;
use App\Modules\Core\Commands\SchedulerCommand;
use App\Modules\Deals\Commands\Manually\DealsExecutorManuallyCommand;
use App\Modules\Deals\Commands\Manually\DealsUpdaterManuallyCommand;
use App\Modules\Exchanges\Commands\GetUsedBinanceWeighCommand;
use App\Modules\Exchanges\Commands\SetNullBinanceWeighCommand;
use App\Modules\Market\Commands\GetPricesCommand;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

/**
 * Kernel.
 */
final class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        TraderCommand::class,
        CreateUserCommand::class,
        DemoCommand::class,
        SchedulerCommand::class,
        DealsUpdaterManuallyCommand::class,
        DealsExecutorManuallyCommand::class,
        GetPricesCommand::class,
        GetUsedBinanceWeighCommand::class,
        SetNullBinanceWeighCommand::class,
        GridTesterCommand::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('horizon:snapshot')
            ->everyMinute()
            ->runInBackground()
            ->withoutOverlapping();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
