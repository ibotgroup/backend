<?php

return [
    'system' => [
        'binance' => [
            'key' => env('BINANCE_API_KEY', ''),
            'secret' => env('BINANCE_API_SECRET', ''),
        ]
    ]
];
