<?php
declare(strict_types=1);

use App\Modules\Accesses\Controllers\AccessesController;
use App\Modules\Bots\Controllers\BotsController;
use App\Modules\Deals\Controllers\DealsController;
use App\Modules\Stocks\Controllers\StocksController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::name('api.')
    ->prefix('/api')
    ->group(function () {
        Route::name('accesses.')
            ->prefix('/accesses')
            ->group(function () {
                Route::get('/list', [AccessesController::class, 'list'])->name('list');
                Route::post('/add', [AccessesController::class, 'add'])->name('add');
                Route::delete('/delete/{access}', [AccessesController::class, 'delete'])->name('delete');
            });

        Route::name('bots.')
            ->prefix('/bots')
            ->group(function () {
                Route::get('/list', [BotsController::class, 'list'])->name('list');
                Route::post('/add', [BotsController::class, 'add'])->name('add');
                Route::get('/add/options', [BotsController::class, 'addOptions'])->name('add.options');
                Route::delete('/delete/{bot}', [BotsController::class, 'delete'])->name('delete');
            });

        Route::name('deals.')
            ->prefix('/deals')
            ->group(function () {
                Route::get('/list', [DealsController::class, 'list'])->name('list');
                Route::get('/list/{bot}', [DealsController::class, 'listByBot'])->name('list.by_bot');
            });

        Route::name('stocks.')
            ->prefix('/stocks')
            ->group(function () {
                Route::get('/get/last-thousand/{interval}/{pair:pair}', [StocksController::class, 'getLastThousand'])->name('get.last.thousand');
            });
    });

